/** *************************************************************************** */
//                       Clase Program Versión 20.
//Objetivo:        Inicializar el aplicativo según licencias configuradas
//Fecha Creación:  2019-03-02
//Responsable:     Sistemas
//Empresa:         Hiper S.A
/** *************************************************************************** */
package soft.gui;

import com.jtattoo.plaf.aero.AeroLookAndFeel;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import javax.swing.JOptionPane;

import java.util.Properties;
import javax.swing.JFrame;
import javax.swing.UIManager;
import soft.deployment.*;
import soft.gui.oracle.FrmBaseOracle;
import soft.gui.sqlserver.FrmBaseSqlServer;

public class Program {

    private static deploymentLic molicencia;

    public static void setUIFont(javax.swing.plaf.FontUIResource f) {
        java.util.Enumeration keys = UIManager.getDefaults().keys();

        while (keys.hasMoreElements()) {
            Object key = keys.nextElement();
            Object value = UIManager.get(key);

            if (value instanceof javax.swing.plaf.FontUIResource) {
                System.out.println(key.toString());
                //if (key.toString().equalsIgnoreCase("FormattedTextField.font")==false)
                UIManager.put(key, f);
            }
        }
    }

    private static void iniciarskin() {

        try {
            Properties props = new Properties();

            props.put("logoString", "Hiper S.A");
            props.put("centerWindowTitle", "off");
            props.put("windowDecoration", "on");
            props.put("windowTitleFont", "Arial BOLD 12");
            props.put("windowInactiveTitleForegroundColor", "158 162 177");

            UIManager.put("OptionPane.messageFont", new Font("Arial", Font.PLAIN, 11));
            UIManager.put("OptionPane.buttonFont", new Font("Arial", Font.PLAIN, 11));
            UIManager.put("Label.font", new Font("Arial", Font.PLAIN, 11));
            UIManager.put("Button.font", new Font("Arial", Font.PLAIN, 11));
            UIManager.put("TextField.font", new Font("Arial", Font.PLAIN, 11));

            UIManager.put("RadioButton.font", new Font("Arial", Font.PLAIN, 11));
            UIManager.put("ProgressBar.font", new Font("Arial", Font.PLAIN, 11));
            UIManager.put("ComboBox.font", new Font("Arial", Font.PLAIN, 11));

            JFrame.setDefaultLookAndFeelDecorated(true);
            AeroLookAndFeel.setTheme(props);

            UIManager.setLookAndFeel("com.jtattoo.plaf.aero.AeroLookAndFeel");

        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmBaseOracle.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

    }//Fin de método iniciarskin

    private static boolean cargarArchivo() {
        boolean flagprocessar = true;

        molicencia = new deploymentLic();

        if (molicencia.MensajeError() != null) {
            flagprocessar = false;
            JOptionPane.showMessageDialog(null, molicencia.MensajeError(), "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
        }

        return flagprocessar;
    }

    public static void main(String... args) {

        iniciarskin();

        if (cargarArchivo()) {
            java.awt.EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    /*Modificar  Base*/
                    try {
                        Properties propiedades = new Properties();
                        propiedades.load(new FileInputStream("config_bd.properties"));

                        String conexion = propiedades.getProperty("conexion");
                        String servidor = propiedades.getProperty("servidor");
                        String usuario = propiedades.getProperty("usuario");;
                        String clave = propiedades.getProperty("clave");;
                        String puerto = propiedades.getProperty("puerto");;
                        String serviceName = propiedades.getProperty("serviceName");;
                        String rolUsuario = propiedades.getProperty("rolUsuario");;
                        String rutaWallet = propiedades.getProperty("rutaWallet");;
                        String nombreTableSpace = propiedades.getProperty("nombreTableSpace");;
                        String esquema = propiedades.getProperty("esquema");;
                        String claveEsquema = propiedades.getProperty("claveEsquema");;

                        String conexionLlaves = propiedades.getProperty("conexionLlaves");;
                        String servidorLlaves = propiedades.getProperty("servidorLlaves");;
                        String usuarioLlaves = propiedades.getProperty("usuarioLlaves");;
                        String claveLlaves = propiedades.getProperty("claveLlaves");;
                        String puertoLlaves = propiedades.getProperty("puertoLlaves");;
                        String serviceNameLlaves = propiedades.getProperty("serviceNameLlaves");;
                        String rolUsuarioLlaves = propiedades.getProperty("rolUsuarioLlaves");;
                        String rutaWalletLlaves = propiedades.getProperty("rutaWalletLlaves");;
                        String nombreTableSpaceLlaves = propiedades.getProperty("nombreTableSpaceLlaves");;
                        String esquemaLlaves = propiedades.getProperty("esquemaLlaves");;
                        String claveEsquemaLlaves = propiedades.getProperty("claveEsquemaLlaves");;

                        String esquemaCNB = propiedades.getProperty("esquemaCNB");;
                        String contrasenaCNB = propiedades.getProperty("contrasenaCNB");;
                        String tableSpaceCNB = propiedades.getProperty("tableSpaceCNB");;
                        String rolCNB = propiedades.getProperty("rolCNB");;

                    } catch (Exception ex) {
                        System.out.println("Error:" + ex);
                    }

                    /*FIN*/
                    if (molicencia.getMotorBase().equalsIgnoreCase("ORACLE")) {
                        new FrmBaseOracle(molicencia).setVisible(true);
                    } else if (molicencia.getMotorBase().equalsIgnoreCase("SQLSERVER")) {
                        new FrmBaseSqlServer(molicencia).setVisible(true);
                    }
                }
            });
        }
    }//Fin de método main 

}//Fin de clase Program

