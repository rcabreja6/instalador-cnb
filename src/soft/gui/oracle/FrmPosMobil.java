
package soft.gui.oracle;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;


public class FrmPosMobil extends javax.swing.JFrame {
    private final JDialog dialogo;
    
    public FrmPosMobil(JDialog odialogo, String EsqemaCliente, String TableSpaceCliente, 
    String claveEsquema, String nombreRolCliente, String modulo) {    
        initComponents();
        dialogo=odialogo;
        dialogo.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        
        if(modulo.equalsIgnoreCase("CNB")){
            lblesquema.setText("Nombre Esquema " + modulo + ":");
            txtesquema_cliente.setText("ADMSWITCH_CNBPRG");
            lblclave.setText("Contraseña Esquema " + modulo + ":");
            lbltablespace.setText("Nombre de TableSpace " + modulo + ":");
            txttablespace_cliente.setText("SWITCH_CUENTA_CLIENTE");
            txtRolCliente.setText("CONTROL_CUENTA_ADM");
            lblRolCliente.setText("Nombre de Rol Asignado:");
        }
        
        if(EsqemaCliente!=null)
              this.txtesquema_cliente.setText(EsqemaCliente);
        
        if(TableSpaceCliente!=null)
            this.txttablespace_cliente.setText(TableSpaceCliente);
        
        if(claveEsquema!=null)
            txtclave_cliente.setText(claveEsquema);
        
        if(nombreRolCliente!=null)
            this.txtRolCliente.setText(nombreRolCliente);
    }
    
    
    public String getRolCliente(){
        return this.txtRolCliente.getText();
    }
    
    public String getEsquemaCliente(){
        return txtesquema_cliente.getText();
    }
    
    public String getClaveEsquemaCliente(){
        return new String(this.txtclave_cliente.getPassword());
    }
    
    public String getTableSpaceCliente(){
        return txttablespace_cliente.getText();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlgeneral = new javax.swing.JPanel();
        pnlcontendor = new javax.swing.JPanel();
        lblesquema = new javax.swing.JLabel();
        txtesquema_cliente = new javax.swing.JTextField();
        lblclave = new javax.swing.JLabel();
        txtclave_cliente = new javax.swing.JPasswordField();
        lbltablespace = new javax.swing.JLabel();
        txttablespace_cliente = new javax.swing.JTextField();
        lblRolCliente = new javax.swing.JLabel();
        txtRolCliente = new javax.swing.JTextField();
        pnlcomando = new javax.swing.JPanel();
        btnaceptar = new javax.swing.JButton();

        setTitle("Configuración de Base de Datos POS Mobil");
        setName("frmposmobil"); // NOI18N
        setPreferredSize(new java.awt.Dimension(430, 149));
        setResizable(false);

        pnlgeneral.setPreferredSize(new java.awt.Dimension(428, 146));

        pnlcontendor.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEtchedBorder(java.awt.Color.white, null), javax.swing.BorderFactory.createEmptyBorder(3, 5, 5, 5)));
        pnlcontendor.setPreferredSize(new java.awt.Dimension(295, 130));
        pnlcontendor.setLayout(new java.awt.GridLayout(4, 2, 2, 2));

        lblesquema.setText("Nombre Esquema Cliente:");
        pnlcontendor.add(lblesquema);

        txtesquema_cliente.setDocument(getUpcaseFilter(20));
        txtesquema_cliente.setText("ADMIN_CUENTA");
        pnlcontendor.add(txtesquema_cliente);

        lblclave.setText("Contraseña Esquema Cliente:");
        pnlcontendor.add(lblclave);

        txtclave_cliente.setText("hiper");
        pnlcontendor.add(txtclave_cliente);

        lbltablespace.setText("Nombre de TableSpace Cliente:");
        pnlcontendor.add(lbltablespace);

        txttablespace_cliente.setDocument(getUpcaseFilter(30));
        txttablespace_cliente.setText("SWITCH_CUENTA_CLIENTE");
        pnlcontendor.add(txttablespace_cliente);

        lblRolCliente.setText("Nombre Rol Cliente:");
        pnlcontendor.add(lblRolCliente);

        txtRolCliente.setDocument(getUpcaseFilter(20));
        txtRolCliente.setText("CONTROL_CUENTA");
        pnlcontendor.add(txtRolCliente);

        javax.swing.GroupLayout pnlgeneralLayout = new javax.swing.GroupLayout(pnlgeneral);
        pnlgeneral.setLayout(pnlgeneralLayout);
        pnlgeneralLayout.setHorizontalGroup(
            pnlgeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlgeneralLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(pnlcontendor, javax.swing.GroupLayout.PREFERRED_SIZE, 460, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6))
        );
        pnlgeneralLayout.setVerticalGroup(
            pnlgeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlgeneralLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(pnlcontendor, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnaceptar.setText("Aceptar");
        btnaceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaceptarActionPerformed(evt);
            }
        });
        pnlcomando.add(btnaceptar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(pnlgeneral, javax.swing.GroupLayout.PREFERRED_SIZE, 467, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlcomando, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(126, 126, 126))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(pnlgeneral, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(pnlcomando, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private boolean validar(){
        boolean resultado=true;
        
        if( txtesquema_cliente.getText().trim().length()==0){
            resultado=false;
            JOptionPane.showMessageDialog(this,"Ingrese el nombre del Esquema Cliente"  , "Mensaje del Sisterma", JOptionPane.ERROR_MESSAGE);
        }
        
        if(resultado==true){
            if(txttablespace_cliente.getText().trim().length()==0){
                resultado=false;
                JOptionPane.showMessageDialog(this,"Ingrese el nombre del TableSpace Esquema Cliente"  , "Mensaje del Sisterma", JOptionPane.ERROR_MESSAGE);
            }
        }
        
        if(resultado==true){
            if( txtclave_cliente.getPassword().length==0){
                 resultado=false;
                JOptionPane.showMessageDialog(this,"Ingrese la contraseña del Esquuema Cliente"  , "Mensaje del Sisterma", JOptionPane.ERROR_MESSAGE);
            }
        }
        
         if(resultado==true){
            if(txtRolCliente.getText().trim().length()==0){
                resultado=false;
                JOptionPane.showMessageDialog(this,"Ingrese el nombre del rol del Esquema Cliente"  , "Mensaje del Sisterma", JOptionPane.ERROR_MESSAGE);
            }
        }
        
        return resultado;
    }
    
    private void btnaceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaceptarActionPerformed
        if(validar())
            dialogo.dispose();
    }//GEN-LAST:event_btnaceptarActionPerformed

    

     private PlainDocument getUpcaseFilter(int limite){
        PlainDocument ofiltrocase=null;
     
        ofiltrocase= new PlainDocument(){
                        
                  
            //public void insertString(DocumentFilter.FilterBypass fb, int offset,String text, AttributeSet attr) throws BadLocationException {
             public void insertString(int offset, String text, AttributeSet attr)  throws BadLocationException {

                if (text == null)
                    return;


                 if ((getLength() + text.trim().length()) <= limite){

                    super.insertString(offset, text.replaceAll("\\W", "").toUpperCase(), attr);  // remove non-digits

                 }
            }
        };
        
        return ofiltrocase;
    }
    
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnaceptar;
    private javax.swing.JLabel lblRolCliente;
    private javax.swing.JLabel lblclave;
    private javax.swing.JLabel lblesquema;
    private javax.swing.JLabel lbltablespace;
    private javax.swing.JPanel pnlcomando;
    private javax.swing.JPanel pnlcontendor;
    private javax.swing.JPanel pnlgeneral;
    private javax.swing.JTextField txtRolCliente;
    private javax.swing.JPasswordField txtclave_cliente;
    private javax.swing.JTextField txtesquema_cliente;
    private javax.swing.JTextField txttablespace_cliente;
    // End of variables declaration//GEN-END:variables
}
