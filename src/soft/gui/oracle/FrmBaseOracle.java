
/******************************************************************************/
//                       Clase FrmBaseOracle Versión 20.
//Objetivo:        Interfaz GUI para capturar los datos de creación de Base de Datos Oracle
//Fecha Creación:  2019-03-02
//Responsable:     Sistemas
//Empresa:         Hiper S.A
/******************************************************************************/

package soft.gui.oracle;

import soft.gui.FrminfoProceso;
import javax.swing.JDialog;
import java.awt.Component;
import java.awt.Container;
import java.awt.Image;
import java.awt.event.ItemEvent;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.MonthDay;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;

import javax.swing.JOptionPane;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import javax.swing.text.PlainDocument;
import seg.*;
import soft.deployment.*;
/*
class clase {
    public int icodigo;
    private String vnombre;
    
    public clase(int codigo, String valor){
          icodigo=codigo;
          vnombre=valor;
    }
    
    public String toString(){
        return vnombre;
    }
    
}
*/


public class FrmBaseOracle extends javax.swing.JFrame {
    
    private parametrosConexion conexionBaseEsquemaSwt,  conexionBaseAdminSwt;
    private parametrosConexion conexionBaseEsquemaLlaves, conexionBaseAdminLlaves;
    private parametrosConexion conexionBaseAdminClientes, conexionBaseEsquemaClientes;
    private parametrosConexion conexionBaseAdminDatos, conexionBaseEsquemaDatos;
  
    private String EsquemaCliente,ClaveEsquemaCliente;
    private String TableSpaceEsquemaCliente;
    
    private String EsquemaDatos, ClaveEsquemaDatos;
    private String TableSpaceDatos;
    
    private String nombreRolDatos, nombreRolCliente;
    
    private deploymentLic licencia;
    
    private void enableComponents(Container container, boolean enable) {
        Component[] components = container.getComponents();
        
        for (Component component : components) {
            component.setEnabled(enable);
            
            if (component instanceof Container) {
                enableComponents((Container)component, enable);
            }
        }
    }
      
    private DocumentFilter getDigitosFilter(int limite){
        DocumentFilter ofilnumeros;
        
        ofilnumeros= new DocumentFilter(){
      
            @Override
            public void insertString(DocumentFilter.FilterBypass fb, int offset,String text, AttributeSet attr) throws BadLocationException {

                if ( text.trim().length() <= limite)
                    fb.insertString(offset, text.replaceAll("\\D++", ""), attr);  // remove non-digits
            }

            @Override
            public void replace(DocumentFilter.FilterBypass fb, int offset, int length,String text, AttributeSet attr) throws BadLocationException {
                if ( text.trim().length() <= limite) 
                    fb.replace(offset, length, text.replaceAll("\\D++", ""), attr);  // remove non-digits
                 //fb.replace(offset, length, text.replaceAll("\\W", "").toUpperCase(), attr);  // remove non-digits
            }

        };
        
        return ofilnumeros;
    }
    
    private PlainDocument getUpcaseFilter(int limite){
        PlainDocument ofiltrocase;
        //PlainDocument o=null;
      
        ofiltrocase= new PlainDocument(){
                             
            //public void insertString(DocumentFilter.FilterBypass fb, int offset,String text, AttributeSet attr) throws BadLocationException {
            @Override
            public void insertString(int offset, String text, AttributeSet attr)  throws BadLocationException {

                if (text == null)
                    return;

                 if ((getLength() + text.trim().length()) <= limite){

                    super.insertString(offset, text.replaceAll("\\W", "").toUpperCase(), attr);  // remove non-digits

                 }
            }

             /*
            public void replace(DocumentFilter.FilterBypass fb, int offset, int length,String text, AttributeSet attr) throws BadLocationException {
                fb.replace(offset, length, text.replaceAll("\\W", "").toUpperCase(), attr);  // remove non-digits
            }
            */

        };
        
        return ofiltrocase;
    }
    
    public FrmBaseOracle(deploymentLic infolicencia) {
        initComponents();
        
        EsquemaCliente=null;
        ClaveEsquemaCliente=null;
        TableSpaceEsquemaCliente=null;

        EsquemaDatos=null;
        ClaveEsquemaDatos=null;
        TableSpaceDatos=null;
        
        nombreRolCliente=null;
        nombreRolDatos=null;
        
        InputStream imgStream;
        Image imagen1;
        licencia=infolicencia;
 
        /*
        
        clase oclase= null;
        clase [] colclase= new clase[3];
        
        oclase= new clase(10,"Sistemas");
        
        colclase[0]=oclase;
        
        oclase= new clase(23,"RRHH");
        colclase[1]=oclase;
        
        oclase= new clase(26,"Generencia");
        colclase[2]=oclase;
        
        jcombo.setModel(new javax.swing.DefaultComboBoxModel<>(  colclase) ) ;
        */

        try{
            imgStream = FrmBaseOracle.class.getResourceAsStream("/imagenes/base.jpg");
            imagen1 = ImageIO.read(imgStream);
            setIconImage(imagen1);
            
            setLocationRelativeTo(null); 
           
           
            if (licencia.getModulo().equalsIgnoreCase("POSMOVIL")){
                txtestadoposmobil.setText("NO CONFIGURADO");
                lblproducto.setText("Módulo Pos Mobil:");
                btnposmobil.setEnabled(true);
            }
            else if(licencia.getModulo().equalsIgnoreCase("WALLET")){
                txtestadoposmobil.setText("NO CONFIGURADO");
                lblproducto.setText("Módulo Wallet:");
                btnposmobil.setEnabled(true);
            }
            else if(licencia.getModulo().equalsIgnoreCase("CNB")){
                txtestadoposmobil.setText("NO CONFIGURADO");
                lblproducto.setText("Módulo CNB:");
                btnposmobil.setEnabled(true); 
            }
            else{
                 txtestadoposmobil.setText("NO DISPONIBLE");
                 btnposmobil.setEnabled(false);
            }          
        }
        catch(IOException e){
            ;// e.printStackTrace();
        }

    }
    
 
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlcontenedor = new javax.swing.JPanel();
        pnlformaengine = new javax.swing.JPanel();
        lblTipoConEngine = new javax.swing.JLabel();
        cbotipoconengine = new javax.swing.JComboBox<>();
        lblServidorEsquemaSwt = new javax.swing.JLabel();
        txtServidorSwt = new javax.swing.JTextField();
        lblClaveEsquemaSwt = new javax.swing.JLabel();
        lblUsuarioSwt = new javax.swing.JLabel();
        txtUsuarioSwt = new javax.swing.JTextField();
        lblClaveUsuarioSwt = new javax.swing.JLabel();
        txtClaveSwt = new javax.swing.JPasswordField();
        lblPuertoSwt = new javax.swing.JLabel();
        txtPuertoSwt = new javax.swing.JTextField();
        txtClaveEsquemaSeg = new javax.swing.JPasswordField();
        lblServicioSwt = new javax.swing.JLabel();
        txtServicioSwt = new javax.swing.JTextField();
        lblEsquemaSwt = new javax.swing.JLabel();
        txtEsquemaSwt = new javax.swing.JTextField();
        txtClaveEsquemaSwt = new javax.swing.JPasswordField();
        txtTablespaceSwt = new javax.swing.JTextField();
        txtUsuarioSeg = new javax.swing.JTextField();
        lblTablespaceSwt = new javax.swing.JLabel();
        lblServidorSeg = new javax.swing.JLabel();
        txtServidorEsquemaSeg = new javax.swing.JTextField();
        lblPuertoEsquemaSeg = new javax.swing.JLabel();
        txtPuertoEsquemaSeg = new javax.swing.JTextField();
        lblServicioSeg = new javax.swing.JLabel();
        txtServicioSeg = new javax.swing.JTextField();
        lblBaseSeg = new javax.swing.JLabel();
        txtEsquemaSeg = new javax.swing.JTextField();
        lblUsuarioBaseSeg = new javax.swing.JLabel();
        lblClaveBaseSeg = new javax.swing.JLabel();
        txtClaveUsuarioSeg = new javax.swing.JPasswordField();
        txtTableSpaceSeg = new javax.swing.JTextField();
        lblClaveEsquemaSeg = new javax.swing.JLabel();
        lblTablespaceSeg = new javax.swing.JLabel();
        lblTipoConSeg = new javax.swing.JLabel();
        cbotipoconseg = new javax.swing.JComboBox<>();
        lblrutaengine = new javax.swing.JLabel();
        txtrutaengine = new javax.swing.JTextField();
        btnrutaengine = new javax.swing.JButton();
        lblrutaseg = new javax.swing.JLabel();
        txtrutaseg = new javax.swing.JTextField();
        btnrutaseg = new javax.swing.JButton();
        lblrolswt = new javax.swing.JLabel();
        txtrolswt = new javax.swing.JTextField();
        lblrolseg = new javax.swing.JLabel();
        txtrolseg = new javax.swing.JTextField();
        btnposmobil = new javax.swing.JButton();
        txtestadoposmobil = new javax.swing.JTextField();
        lblproducto = new javax.swing.JLabel();
        btnaceptar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Módulo de Instalación de  Base Datos UC-Engine PCI 3.0");
        setFont(new java.awt.Font("Arial", 0, 10)); // NOI18N
        setPreferredSize(new java.awt.Dimension(965, 415));
        setResizable(false);

        pnlcontenedor.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 0, 5));
        pnlcontenedor.setPreferredSize(new java.awt.Dimension(950, 400));

        pnlformaengine.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEtchedBorder(java.awt.Color.white, null), javax.swing.BorderFactory.createEmptyBorder(3, 5, 5, 5)));
        pnlformaengine.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        pnlformaengine.setPreferredSize(new java.awt.Dimension(440, 450));

        lblTipoConEngine.setText("Tipo Conexión Base Engine:");

        cbotipoconengine.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Aut. Thin SSL ", "Aut. Thin Estandar" }));
        cbotipoconengine.setMinimumSize(new java.awt.Dimension(166, 23));
        cbotipoconengine.setPreferredSize(new java.awt.Dimension(166, 23));
        cbotipoconengine.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbotipoconengineItemStateChanged(evt);
            }
        });

        lblServidorEsquemaSwt.setText("Servidor Base Engine:");

        txtServidorSwt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                FrmBaseOracle.this.keyTyped(evt);
            }
        });

        lblClaveEsquemaSwt.setText("Clave Esquema Engine:");

        lblUsuarioSwt.setText("Usuario Base  Engine:");

        txtUsuarioSwt.setText("system");

        lblClaveUsuarioSwt.setText("Clave Usuario Base Engine:");

        lblPuertoSwt.setText("Puerto Base Engine:");

        ((AbstractDocument) txtPuertoSwt.getDocument()).setDocumentFilter(getDigitosFilter(5));
        txtPuertoSwt.setText("1521");
        txtPuertoSwt.setMinimumSize(new java.awt.Dimension(6, 23));
        txtPuertoSwt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                FrmBaseOracle.this.keyTyped(evt);
            }
        });

        txtClaveEsquemaSeg.setText("hiper");
        txtClaveEsquemaSeg.setPreferredSize(new java.awt.Dimension(41, 23));

        lblServicioSwt.setText("Service Name Base Engine:");

        txtServicioSwt.setText("orcl");
        txtServicioSwt.setMinimumSize(new java.awt.Dimension(6, 23));

        lblEsquemaSwt.setText("Esquema Engine:");

        txtEsquemaSwt.setDocument(getUpcaseFilter(20));
        txtEsquemaSwt.setText("ADMSWITCH_CNBDAT");
        txtEsquemaSwt.setPreferredSize(new java.awt.Dimension(79, 23));

        txtClaveEsquemaSwt.setText("hiper");
        txtClaveEsquemaSwt.setPreferredSize(new java.awt.Dimension(41, 23));

        txtTablespaceSwt.setDocument(getUpcaseFilter(30));
        txtTablespaceSwt.setText("SWITCH_TRANSACCIONAL");
        txtTablespaceSwt.setPreferredSize(new java.awt.Dimension(137, 23));

        txtUsuarioSeg.setText("system");
        txtUsuarioSeg.setPreferredSize(new java.awt.Dimension(40, 23));

        lblTablespaceSwt.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTablespaceSwt.setText("Nombre TableSpace Engine:");

        lblServidorSeg.setText("Servidor Esquema Seguridad:");

        txtServidorEsquemaSeg.setPreferredSize(new java.awt.Dimension(6, 23));

        lblPuertoEsquemaSeg.setText("Puerto Esquema Seguidad:");

        txtPuertoEsquemaSeg.setText("1521");
        txtPuertoEsquemaSeg.setPreferredSize(new java.awt.Dimension(30, 23));
        txtPuertoEsquemaSeg.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                FrmBaseOracle.this.keyTyped(evt);
            }
        });

        lblServicioSeg.setText("Service Name Base Seguridad:");

        txtServicioSeg.setText("orcl");
        txtServicioSeg.setPreferredSize(new java.awt.Dimension(6, 23));

        lblBaseSeg.setText("Esquema Seguridad:");

        txtEsquemaSeg.setDocument(getUpcaseFilter(20));
        txtEsquemaSeg.setText("ADMINLLAVES");
        txtEsquemaSeg.setPreferredSize(new java.awt.Dimension(74, 23));

        lblUsuarioBaseSeg.setText("Usuario Base Seguridad:");

        lblClaveBaseSeg.setText("Clave Base Seguridad:");

        txtClaveUsuarioSeg.setPreferredSize(new java.awt.Dimension(6, 23));

        txtTableSpaceSeg.setDocument(getUpcaseFilter(30));
        txtTableSpaceSeg.setText("SWITCH_LLAVES");
        txtTableSpaceSeg.setPreferredSize(new java.awt.Dimension(87, 23));

        lblClaveEsquemaSeg.setText("Clave Esquema Seguridad:");

        lblTablespaceSeg.setText("Nombre TableSpace Seguridad:");

        lblTipoConSeg.setText("Tipo Conexión Base Seguridad:");

        cbotipoconseg.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Aut. Thin SSL", "Aut. Thin Estandar" }));
        cbotipoconseg.setMinimumSize(new java.awt.Dimension(166, 23));
        cbotipoconseg.setPreferredSize(new java.awt.Dimension(166, 23));
        cbotipoconseg.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbotipoconsegItemStateChanged(evt);
            }
        });

        lblrutaengine.setText("Ruta Wallet Base Engine:");

        txtrutaengine.setEditable(false);
        txtrutaengine.setPreferredSize(new java.awt.Dimension(6, 23));

        btnrutaengine.setText("....");
        btnrutaengine.setPreferredSize(new java.awt.Dimension(49, 20));
        btnrutaengine.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnrutaengineActionPerformed(evt);
            }
        });

        lblrutaseg.setText("Ruta Certificado Base Seguridad:");

        txtrutaseg.setEditable(false);
        txtrutaseg.setPreferredSize(new java.awt.Dimension(6, 23));

        btnrutaseg.setText("...");
        btnrutaseg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnrutasegActionPerformed(evt);
            }
        });

        lblrolswt.setText("Nombre de Rol de Usuario Engine:");

        txtrolswt.setDocument(getUpcaseFilter(20));
        txtrolswt.setText("CONTROL_SWITCH_ADM");
        txtrolswt.setPreferredSize(new java.awt.Dimension(6, 23));

        lblrolseg.setText("Nombre Rol Uusuario Seguridad:");

        txtrolseg.setDocument(getUpcaseFilter(20));
        txtrolseg.setText("CONTROL_LLAVES_ADM");
        txtrolseg.setPreferredSize(new java.awt.Dimension(6, 23));

        btnposmobil.setText("...");
        btnposmobil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnposmobilActionPerformed(evt);
            }
        });

        txtestadoposmobil.setEditable(false);
        txtestadoposmobil.setPreferredSize(new java.awt.Dimension(97, 23));
        txtestadoposmobil.setRequestFocusEnabled(false);

        lblproducto.setText("Módulo:");

        javax.swing.GroupLayout pnlformaengineLayout = new javax.swing.GroupLayout(pnlformaengine);
        pnlformaengine.setLayout(pnlformaengineLayout);
        pnlformaengineLayout.setHorizontalGroup(
            pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlformaengineLayout.createSequentialGroup()
                .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblUsuarioSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblClaveUsuarioSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblPuertoSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblServicioSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblServidorEsquemaSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTipoConEngine, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblServicioSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTipoConSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblServidorSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblPuertoEsquemaSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblClaveBaseSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblproducto, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblUsuarioBaseSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(31, 31, 31)
                .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlformaengineLayout.createSequentialGroup()
                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(txtServicioSeg, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txtPuertoEsquemaSeg, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txtUsuarioSwt, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txtClaveSwt, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txtPuertoSwt, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txtServicioSwt, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cbotipoconseg, 0, 183, Short.MAX_VALUE))
                            .addComponent(txtServidorEsquemaSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtUsuarioSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtClaveUsuarioSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(48, 48, 48)
                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlformaengineLayout.createSequentialGroup()
                                .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblTablespaceSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblEsquemaSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblBaseSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblClaveEsquemaSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblClaveEsquemaSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblrolseg))
                                .addGap(0, 6, Short.MAX_VALUE))
                            .addGroup(pnlformaengineLayout.createSequentialGroup()
                                .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(lblTablespaceSeg, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lblrutaseg, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txtrolseg, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtClaveEsquemaSeg, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtClaveEsquemaSwt, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtEsquemaSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtTablespaceSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtrutaengine, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtrolswt, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(txtTableSpaceSeg, javax.swing.GroupLayout.DEFAULT_SIZE, 183, Short.MAX_VALUE)
                                .addComponent(txtrutaseg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(txtEsquemaSeg, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(pnlformaengineLayout.createSequentialGroup()
                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlformaengineLayout.createSequentialGroup()
                                .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtServidorSwt)
                                    .addComponent(cbotipoconengine, 0, 183, Short.MAX_VALUE))
                                .addGap(48, 48, 48)
                                .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblrolswt)
                                    .addComponent(lblrutaengine, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(pnlformaengineLayout.createSequentialGroup()
                                .addComponent(txtestadoposmobil, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnposmobil, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlformaengineLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(btnrutaengine, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlformaengineLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnrutaseg, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        pnlformaengineLayout.setVerticalGroup(
            pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlformaengineLayout.createSequentialGroup()
                .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbotipoconengine, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTipoConEngine, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtrolswt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblrolswt, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(1, 1, 1)
                .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblServidorEsquemaSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtServidorSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblrutaengine)
                    .addComponent(txtrutaengine, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnrutaengine, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(1, 1, 1)
                .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblUsuarioSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtUsuarioSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtTablespaceSwt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTablespaceSwt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(1, 1, 1)
                .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblClaveUsuarioSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtClaveSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtEsquemaSwt, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblEsquemaSwt, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(1, 1, 1)
                .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlformaengineLayout.createSequentialGroup()
                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblPuertoSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtPuertoSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblClaveEsquemaSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(1, 1, 1)
                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblServicioSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtServicioSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(txtClaveEsquemaSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlformaengineLayout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblTipoConSeg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(cbotipoconseg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtEsquemaSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlformaengineLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblBaseSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlformaengineLayout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtServidorEsquemaSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtClaveEsquemaSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblClaveEsquemaSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(1, 1, 1)
                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtUsuarioSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblUsuarioBaseSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtrutaseg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblrutaseg)
                            .addComponent(btnrutaseg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(1, 1, 1)
                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblTablespaceSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtClaveUsuarioSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblClaveBaseSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtTableSpaceSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(pnlformaengineLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(lblServidorSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(1, 1, 1)
                .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtrolseg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pnlformaengineLayout.createSequentialGroup()
                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtPuertoEsquemaSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblPuertoEsquemaSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(pnlformaengineLayout.createSequentialGroup()
                                .addGap(5, 5, 5)
                                .addComponent(lblrolseg)))
                        .addGap(1, 1, 1)
                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtServicioSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblServicioSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(8, 8, 8)
                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnposmobil, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblproducto)
                            .addComponent(txtestadoposmobil, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(6, 6, 6))
        );

        btnaceptar.setText("Aceptar");
        btnaceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaceptarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlcontenedorLayout = new javax.swing.GroupLayout(pnlcontenedor);
        pnlcontenedor.setLayout(pnlcontenedorLayout);
        pnlcontenedorLayout.setHorizontalGroup(
            pnlcontenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlcontenedorLayout.createSequentialGroup()
                .addComponent(pnlformaengine, javax.swing.GroupLayout.DEFAULT_SIZE, 916, Short.MAX_VALUE)
                .addGap(1, 1, 1))
            .addGroup(pnlcontenedorLayout.createSequentialGroup()
                .addGap(412, 412, 412)
                .addComponent(btnaceptar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlcontenedorLayout.setVerticalGroup(
            pnlcontenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlcontenedorLayout.createSequentialGroup()
                .addComponent(pnlformaengine, javax.swing.GroupLayout.PREFERRED_SIZE, 342, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnaceptar)
                .addContainerGap())
        );

        getContentPane().add(pnlcontenedor, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnaceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaceptarActionPerformed
        MonthDay[] fiestas; 
        fiestas=new MonthDay[8]; 
        fiestas[0]=MonthDay.of(1,1); 
        fiestas[1]=MonthDay.of(5,1); 
        fiestas[2]=MonthDay.of(8,15); 
        fiestas[3]=MonthDay.of(10,12); 
        fiestas[4]=MonthDay.of(11,1); 
        fiestas[5]=MonthDay.of(12,6); 
        fiestas[6]=MonthDay.of(12,8); 
        fiestas[7]=MonthDay.of(12,25);
        
        procesar();
    }//GEN-LAST:event_btnaceptarActionPerformed

    private void btnposmobilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnposmobilActionPerformed
        llamarForma();
    }//GEN-LAST:event_btnposmobilActionPerformed

    private void btnrutasegActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnrutasegActionPerformed

        txtrutaseg.setText("");

        JFileChooser jfiledialog=new JFileChooser();
        jfiledialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        jfiledialog.setDialogTitle("Selecciòn de Ruta Wallet de Base de Datos de Seguridad");
        int seleccion=jfiledialog.showOpenDialog(this);

        if(seleccion==JFileChooser.APPROVE_OPTION){
            File archivoScript=jfiledialog.getSelectedFile();
            txtrutaseg.setText(archivoScript.getAbsolutePath());
        }

    }//GEN-LAST:event_btnrutasegActionPerformed

    private void btnrutaengineActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnrutaengineActionPerformed

        txtrutaengine.setText("");

        JFileChooser jfiledialog=new JFileChooser();
        jfiledialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        jfiledialog.setDialogTitle("Selecciòn de Ruta Wallet de Base Datos Engine");
        int seleccion=jfiledialog.showOpenDialog(this);

        if(seleccion==JFileChooser.APPROVE_OPTION){
            File archivoScript=jfiledialog.getSelectedFile();
            txtrutaengine.setText(archivoScript.getAbsolutePath());
        }
    }//GEN-LAST:event_btnrutaengineActionPerformed

    private void cbotipoconsegItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbotipoconsegItemStateChanged
       

        if (evt.getStateChange() == ItemEvent.SELECTED) {

            if(cbotipoconseg.getSelectedIndex()==0){
                txtrutaseg.setText("");
                btnrutaseg.setEnabled(true);
                lblServicioSeg.setText("Service Name Base Seguridad:");
            }
            else if(cbotipoconseg.getSelectedIndex()==1){
                txtrutaseg.setText("");
                btnrutaseg.setEnabled(false);
                 lblServicioSeg.setText("Instancia Base Seguridad (SID):");
            }
        }

    }//GEN-LAST:event_cbotipoconsegItemStateChanged

    private void keyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_keyTyped

        if (evt.getSource()==txtServidorSwt){
            if(txtServidorSwt.getText().length()==80)
            evt.consume();
        }

        if(evt.getSource()==txtPuertoSwt){
            if(txtPuertoSwt.getText().length()==6)
            evt.consume();
            else if(txtPuertoSwt.getText().length()>6){
                txtPuertoSwt.setText("");
                evt.consume();
            }
        }

        if(evt.getSource()==txtPuertoEsquemaSeg){
            if(txtPuertoEsquemaSeg.getText().length()==6)
            evt.consume();
            else if(txtPuertoEsquemaSeg.getText().length()>6){
                txtPuertoEsquemaSeg.setText("");
                evt.consume();
            }
        }
    }//GEN-LAST:event_keyTyped

    private void cbotipoconengineItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbotipoconengineItemStateChanged
      
        if (evt.getStateChange() == ItemEvent.SELECTED) {

            if(cbotipoconengine.getSelectedIndex()==0){
                txtrutaengine.setText("");
                btnrutaengine.setEnabled(true);
                
                lblServicioSwt.setText("Service Name Base Engine:");
            }
            else if(cbotipoconengine.getSelectedIndex()==1){
                txtrutaengine.setText("");
                btnrutaengine.setEnabled(false);
                lblServicioSwt.setText("Instancia Base Engine (SID):");
            }
        }
    }//GEN-LAST:event_cbotipoconengineItemStateChanged

    
    private void llamarForma(){
        JDialog odialogo= new JDialog(this,true);
        
        FrmPosMobil ofrmposmobil=null;
        FrmWallet ofrmWallet=null;
        

        if (licencia.getModulo().equalsIgnoreCase("POSMOVIL") || licencia.getModulo().equalsIgnoreCase("CNB")  )
            ofrmposmobil= new FrmPosMobil(odialogo,EsquemaCliente,TableSpaceEsquemaCliente,
            ClaveEsquemaCliente,nombreRolCliente,licencia.getModulo());
        
           
        
        else if(licencia.getModulo().equalsIgnoreCase("WALLET") ){
            ofrmWallet= new FrmWallet(odialogo,EsquemaCliente,TableSpaceEsquemaCliente,ClaveEsquemaCliente,
            EsquemaDatos,this.TableSpaceDatos,ClaveEsquemaDatos, nombreRolCliente, nombreRolDatos,licencia.getMotorBase());
        }
       
     
        if(ofrmposmobil!=null ){
            
            if (licencia.getModulo().equalsIgnoreCase("CNB"))
                odialogo.setTitle("Configuración Base de Datos Módulo " + licencia.getModulo());
            else
                odialogo.setTitle(ofrmposmobil.getTitle());
            
            odialogo.add(ofrmposmobil.getContentPane());
            odialogo.setSize(ofrmposmobil.getSize());
        }
        
        if (ofrmWallet!=null && ofrmposmobil==null){
              odialogo.setTitle(ofrmWallet.getTitle());
              odialogo.add(ofrmWallet.getContentPane());
              odialogo.setSize(ofrmWallet.getSize());
        }
        
        if(ofrmposmobil!=null || ofrmWallet!=null){
            odialogo.setResizable(false);
            odialogo.pack();
            odialogo.setLocationRelativeTo(this);
            odialogo.setVisible(true);
        }
        
        
        if(ofrmposmobil!=null){
            EsquemaCliente=ofrmposmobil.getEsquemaCliente();
            TableSpaceEsquemaCliente=ofrmposmobil.getTableSpaceCliente();
            ClaveEsquemaCliente=ofrmposmobil.getClaveEsquemaCliente();
            nombreRolCliente=ofrmposmobil.getRolCliente();
        }
        
        if(ofrmWallet!=null){
            EsquemaCliente=ofrmWallet.getEsquemaCliente();
            TableSpaceEsquemaCliente=ofrmWallet.getTableSpaceCliente();
            ClaveEsquemaCliente=ofrmWallet.getClaveEsquemaCliente();
            
            EsquemaDatos=ofrmWallet.getEsquemaDatos();
            ClaveEsquemaDatos=ofrmWallet.getClaveEsquemaDatos();
            TableSpaceDatos=ofrmWallet.getTableSpaceDatos();
            nombreRolCliente=ofrmWallet.getRolCliente();
            nombreRolDatos=ofrmWallet.getRolDatos();
        }
        
        if (ofrmposmobil!=null && ofrmWallet==null){
            if(EsquemaCliente!=null  && TableSpaceEsquemaCliente!=null && ClaveEsquemaCliente!=null ){
                txtestadoposmobil.setText("CONFIGURADO");
            }
            else
                txtestadoposmobil.setText("NO CONFIGURADO");
        }
        
        if (ofrmposmobil==null && ofrmWallet!=null){
            if(EsquemaCliente!=null  && TableSpaceEsquemaCliente!=null && 
               ClaveEsquemaCliente!=null && TableSpaceDatos!=null && 
               EsquemaDatos!=null && ClaveEsquemaDatos!=null){
                 txtestadoposmobil.setText("CONFIGURADO");
            }
            else
                txtestadoposmobil.setText("NO CONFIGURADO"); 
        }      
    }
    
    
    private boolean validarentrada(){
        boolean flagresultado=true;
        
        if(txtServidorSwt.getText()==null || txtServidorSwt.getText().trim().length()==0){
            flagresultado=false;
            JOptionPane.showMessageDialog(this, "Ingrese la información del Servidor de Base de Datos del Engine", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
        }
        
        if(flagresultado==true)
            if ( txtPuertoSwt.getText().trim().length()==0){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese la información del puerto de  Base de Datos del Engine", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        
        if(flagresultado==true)
            if (txtServicioSwt.getText().trim().length()==0){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese el nombre de instancia de la Base de Datos del Engine", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        
        if(flagresultado==true)
            if (txtEsquemaSwt.getText().trim().length()==0){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese el nombre de esquema del Engine", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        
        if(flagresultado==true)
            if (txtClaveEsquemaSwt.getPassword().length==0){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese la clave de esquema del Engine", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        
        if(flagresultado==true)
            if ( txtUsuarioSwt.getText().trim().length()==0){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese el usuario de Base de Datos del Engine", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        
        
      
        if(flagresultado==true)
            if ( txtClaveSwt.getPassword()==null ||  txtClaveSwt.getPassword().length==0) {
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese la clave de Base de Datos del Engine", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        
        
        if(flagresultado==true)
            if (txtTablespaceSwt.getText()==null || txtTablespaceSwt.getText().trim().length()==0){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese nombre del TableSpace del Engine", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        
        
        if(flagresultado==true)
            if (txtServidorEsquemaSeg.getText()==null || txtServidorEsquemaSeg.getText().trim().length()==0){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese la información del servidor de la Base de Datos de Seguridad", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        
        if(flagresultado==true)
            if (txtPuertoEsquemaSeg.getText()==null || txtPuertoEsquemaSeg.getText().trim().length()==0){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese el puerto de la Base de Datos de Seguridad", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        
        if(flagresultado==true)
            if (txtServicioSeg.getText()==null || txtServicioSeg.getText().trim().length()==0){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese el nombre de instancia de la Base de Datos de Seguridad", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        
        if(flagresultado==true)
            if (txtEsquemaSeg.getText()==null || txtEsquemaSeg.getText().trim().length()==0){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese el nombre del Esquema de la Base de Datos de Seguridad", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        
        if(flagresultado==true)
            if (txtClaveEsquemaSeg.getPassword()==null || txtClaveEsquemaSeg.getPassword().length==0){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese la clave de Esquema de la Base de Datos de Seguridad", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        
        
        if(flagresultado==true)
            if (txtUsuarioSeg.getText()==null || txtUsuarioSeg.getText().trim().length()==0){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese el nombre de Usuario de la Base de Datos de Seguridad", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        
        if(flagresultado==true)
            if (txtClaveUsuarioSeg.getPassword()==null || txtClaveUsuarioSeg.getPassword().length==0 ){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese la clave de Usuario de la Base de Datos de Seguridad", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        
        if(flagresultado==true)
            if (txtTableSpaceSeg.getText()==null || txtTableSpaceSeg.getText().trim().length()==0){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese el nombre del TableSpace de la Base de Datos de Seguridad", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        
        if(flagresultado==true){
            
            if(licencia.getModulo().equalsIgnoreCase("POSMOVIL")==true || licencia.getModulo().equalsIgnoreCase("WALLET")==true){
                if( ClaveEsquemaCliente==null  ||EsquemaCliente==null ||
                    TableSpaceEsquemaCliente==null ||  nombreRolCliente==null){
                        flagresultado=false;
                        JOptionPane.showMessageDialog(this, "Ingrese la información de configuración de la Base de Datos de Cliente", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
        
        if(flagresultado==true){
            if(licencia.getModulo().equalsIgnoreCase("WALLET")==true){
                if(EsquemaDatos==null || TableSpaceDatos==null ||ClaveEsquemaDatos==null || nombreRolDatos==null ){
                    flagresultado=false;
                        JOptionPane.showMessageDialog(this, "Ingrese la información de configuración de la Base de Datos Vault", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
        
        if(flagresultado==true){
            if(cbotipoconengine.getSelectedIndex()==0 && txtrutaengine.getText().trim().equals("")==true){
                 flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese la ruta wallet de la Base de Datos Engine", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        }
        
        if(flagresultado==true){
            
            if(cbotipoconseg.getSelectedIndex()==0 && txtrutaseg.getText().trim().equals("")==true){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese la ruta wallet de la Base de Datos Seguridad", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        }
        
        return flagresultado;
    }
    
    private boolean llenaarDatosBase(){
        boolean flagresultado;
        
        conexionBaseAdminSwt=null;
        conexionBaseEsquemaSwt=null;
        conexionBaseAdminLlaves=null;
        conexionBaseEsquemaLlaves=null;
        
        conexionBaseAdminClientes=null;
        conexionBaseEsquemaClientes=null;
        
        conexionBaseAdminDatos=null;
        conexionBaseEsquemaDatos=null;
        
        flagresultado= validarentrada();
        
        if (flagresultado){
        
            licencia.getListaFiles().forEach((final ArchivoComandos ob) -> {
                parametrosConexion conexionBase;

                if (ob.getEsquema().equals("ADMINBASESWITCH")==true ||ob.getEsquema().equals("ADMINBASECUENTA")==true ||
                    ob.getEsquema().equals("ADMINBASEDATOS")==true){
                  
                    conexionBase= new parametrosConexion();
                    conexionBase.setMotorBaseDatos(licencia.getMotorBase());
                    conexionBase.setEsquema(txtUsuarioSwt.getText());
                    conexionBase.setServidorBaseDatos(txtServidorSwt.getText());
                    conexionBase.setClaveBaseDatos( new String( this.txtClaveSwt.getPassword()));
                    conexionBase.setPuertoBase(Integer.parseInt(txtPuertoSwt.getText()));
                    conexionBase.setNombreTableSpace(txtTablespaceSwt.getText());
                    conexionBase.setServiceName(txtServicioSwt.getText());
                    
                    conexionBase.setTipoConexion(cbotipoconengine.getSelectedIndex());
                    
                    if(cbotipoconengine.getSelectedIndex()==0)
                        conexionBase.setRutaWallet(txtrutaengine.getText());

                    if (conexionBaseAdminSwt==null)
                        conexionBaseAdminSwt=conexionBase;
                    
                    if(ob.getEsquema().equals("ADMINBASECUENTA")==true){
                        
                        conexionBase.setNombreRol(nombreRolCliente);
                        
                        if(conexionBaseAdminClientes==null)
                            conexionBaseAdminClientes=conexionBase;
                        
                        ob.setContenidoArchivo(recuperarContenidoArchivo(ob.getfilename(),ob.getEsquema(),
                        txtEsquemaSwt.getText(),this.TableSpaceEsquemaCliente)); 
                    }
                    else if (ob.getEsquema().equals("ADMINBASEDATOS")==true){
                        
                        conexionBase.setNombreRol(nombreRolDatos);
                        
                        if(conexionBaseAdminDatos==null)
                            conexionBaseAdminDatos=conexionBase;
                        
                        ob.setContenidoArchivo(recuperarContenidoArchivo(ob.getfilename(),ob.getEsquema(),
                        txtEsquemaSwt.getText(),this.TableSpaceDatos)); 
                    }
                    
                    else{
                        conexionBase.setNombreRol(txtrolswt.getText());
                        ob.setContenidoArchivo(recuperarContenidoArchivo(ob.getfilename(),ob.getEsquema(),
                        txtEsquemaSwt.getText(),txtTablespaceSwt.getText())); 
                    }
                    
                    ob.setParametrosConexion(conexionBase);
                    
                }
                
                if (ob.getEsquema().equals("ESQUEMABASESWITCH")==true){

                    conexionBase= new parametrosConexion();
                    conexionBase.setMotorBaseDatos(licencia.getMotorBase());
                    conexionBase.setEsquema(txtEsquemaSwt.getText());

                    conexionBase.setServidorBaseDatos(txtServidorSwt.getText());
                    conexionBase.setClaveBaseDatos( new String( txtClaveEsquemaSwt.getPassword()));
                    conexionBase.setPuertoBase(Integer.parseInt(txtPuertoSwt.getText()));
                    conexionBase.setNombreTableSpace(txtTablespaceSwt.getText());
                    conexionBase.setServiceName(txtServicioSwt.getText());
                    
                    conexionBase.setTipoConexion(cbotipoconengine.getSelectedIndex());
                    
                    if(cbotipoconengine.getSelectedIndex()==0)
                        conexionBase.setRutaWallet(txtrutaengine.getText());
                    
                    conexionBase.setNombreRol(txtrolswt.getText());
                    
                    ob.setParametrosConexion(conexionBase);

                    if (conexionBaseEsquemaSwt==null)
                        conexionBaseEsquemaSwt=conexionBase;

                    ob.setContenidoArchivo(recuperarContenidoArchivo(ob.getfilename(),
                    ob.getEsquema(),conexionBase.getEsquema(),conexionBase.getNombreTableSpace())); 
                }
                
                if (ob.getEsquema().equals("ADMINBASELLAVES")==true){
                    conexionBase= new parametrosConexion();
                    conexionBase.setMotorBaseDatos(licencia.getMotorBase());
                    conexionBase.setEsquema(txtUsuarioSeg.getText());
                    conexionBase.setServidorBaseDatos(txtServidorEsquemaSeg.getText());
                    conexionBase.setClaveBaseDatos(new String( txtClaveUsuarioSeg.getPassword()));
                    conexionBase.setPuertoBase(Integer.parseInt(txtPuertoEsquemaSeg.getText()));
                    conexionBase.setNombreTableSpace(txtTableSpaceSeg.getText());
                    conexionBase.setServiceName(txtServicioSeg.getText());
                    
                    conexionBase.setTipoConexion(cbotipoconseg.getSelectedIndex());
                    
                    if(cbotipoconseg.getSelectedIndex()==0)
                        conexionBase.setRutaWallet(txtrutaseg.getText());
                    
                    conexionBase.setNombreRol(txtrolseg.getText());
                    
                    ob.setParametrosConexion(conexionBase);

                    if(conexionBaseAdminLlaves==null)
                        conexionBaseAdminLlaves=conexionBase;

                    ob.setContenidoArchivo(recuperarContenidoArchivo(ob.getfilename(),ob.getEsquema(),
                    txtEsquemaSeg.getText(),conexionBase.getNombreTableSpace())); 
                }
                
                if (ob.getEsquema().equals("ESQUEMABASELLAVES")==true) {
                    conexionBase= new parametrosConexion();
                    conexionBase.setMotorBaseDatos(licencia.getMotorBase());
                    conexionBase.setEsquema(txtEsquemaSeg.getText());
                    conexionBase.setServidorBaseDatos(txtServidorEsquemaSeg.getText());
                    conexionBase.setClaveBaseDatos(new String( txtClaveEsquemaSeg.getPassword()));
                    conexionBase.setPuertoBase(Integer.parseInt(txtPuertoEsquemaSeg.getText()));
                    conexionBase.setNombreTableSpace(txtTableSpaceSeg.getText());
                    conexionBase.setServiceName(txtServicioSeg.getText());
                    
                    conexionBase.setTipoConexion(cbotipoconseg.getSelectedIndex());
                    
                    if(cbotipoconseg.getSelectedIndex()==0)
                        conexionBase.setRutaWallet(txtrutaseg.getText());
                    
                    conexionBase.setNombreRol(txtrolseg.getText());
                    
                    ob.setParametrosConexion(conexionBase);

                    if (conexionBaseEsquemaLlaves==null)
                        conexionBaseEsquemaLlaves=conexionBase;

                    ob.setContenidoArchivo(recuperarContenidoArchivo(ob.getfilename(),ob.getEsquema(),
                    conexionBase.getEsquema(),conexionBase.getNombreTableSpace())); 
                }
                
                if(ob.getEsquema().equals("ESQUEMABASECUENTA")==true){
                    conexionBase= new parametrosConexion();
                    conexionBase.setMotorBaseDatos(licencia.getMotorBase());
                    conexionBase.setEsquema(EsquemaCliente);
                    conexionBase.setClaveBaseDatos( this.ClaveEsquemaCliente);
                    conexionBase.setServidorBaseDatos(txtServidorSwt.getText());
                    conexionBase.setPuertoBase(Integer.parseInt(txtPuertoSwt.getText()));
                    conexionBase.setNombreTableSpace(this.TableSpaceEsquemaCliente);
                    conexionBase.setServiceName(txtServicioSwt.getText());
                    
                    conexionBase.setTipoConexion(cbotipoconengine.getSelectedIndex());
                     
                    if(cbotipoconengine.getSelectedIndex()==0)
                        conexionBase.setRutaWallet(txtrutaengine.getText());
                    
                    conexionBase.setNombreRol(this.nombreRolCliente);
                    
                    ob.setParametrosConexion(conexionBase);
                    
                     if (conexionBaseEsquemaClientes==null)
                        conexionBaseEsquemaClientes=conexionBase;
                    
                    ob.setContenidoArchivo(recuperarContenidoArchivo(ob.getfilename(),ob.getEsquema(),
                    ClaveEsquemaCliente,TableSpaceEsquemaCliente));    
                }
                
                if(ob.getEsquema().equalsIgnoreCase("ESQUEMABASEDATOS")==true){
                    conexionBase= new parametrosConexion();
                    conexionBase.setMotorBaseDatos(licencia.getMotorBase());
                    conexionBase.setEsquema(this.EsquemaDatos);
                    conexionBase.setClaveBaseDatos( this.ClaveEsquemaDatos);
                    conexionBase.setServidorBaseDatos(txtServidorSwt.getText());
                    conexionBase.setPuertoBase(Integer.parseInt(txtPuertoSwt.getText()));
                    conexionBase.setNombreTableSpace(this.TableSpaceDatos);
                    conexionBase.setServiceName(txtServicioSwt.getText());
                    conexionBase.setTipoConexion(cbotipoconengine.getSelectedIndex());
                    
                    if(cbotipoconengine.getSelectedIndex()==0)
                        conexionBase.setRutaWallet(txtrutaengine.getText());
                    
                    conexionBase.setNombreRol(nombreRolDatos);
                    
                    ob.setParametrosConexion(conexionBase);
                    
                    if(conexionBaseEsquemaDatos==null)
                        conexionBaseEsquemaDatos=conexionBase;
                    
                    ob.setContenidoArchivo(recuperarContenidoArchivo(ob.getfilename(),ob.getEsquema(),
                    this.ClaveEsquemaDatos,TableSpaceDatos));      
                }
                
            }); //Fin de for(ArchivoComandos ob : listafiles){
        }
        
       return flagresultado;
       
    }//Fin de metodo llenaarDatosBase
    
    private void procesar(){
        
        if(llenaarDatosBase() ){   
            JDialog odialogo= new JDialog(this,true);
            
            FrminfoProceso ofrmproceso=new FrminfoProceso(odialogo,licencia, 
            conexionBaseEsquemaSwt,conexionBaseAdminSwt,
            conexionBaseEsquemaLlaves,conexionBaseAdminLlaves,
            conexionBaseAdminClientes,conexionBaseEsquemaClientes,
            conexionBaseAdminDatos,conexionBaseEsquemaDatos);

            odialogo.setTitle(ofrmproceso.getTitle());
            odialogo.add(ofrmproceso.getContentPane());
            odialogo.setSize(ofrmproceso.getSize());
            odialogo.setResizable(false);

            odialogo.pack();
            odialogo.setLocationRelativeTo(this);
            
            enableComponents(pnlformaengine,false);
            btnposmobil.setEnabled(false);
            btnaceptar.setEnabled(false);
            
            odialogo.setVisible(true);
           
            btnaceptar.setEnabled(true);
            enableComponents(pnlformaengine,true);
            
            
        }//Fin de if(llenaarDatosBase())
    }
    
    //Función para leer los archivos de scripts de Base de Datos
    private String recuperarContenidoArchivo(String vnombreArchivo, String Esquema, String EsquemaBase, String nombreTableSpace){
        String contenidoArchivo="";
        File archivo, archivo_sql;
       
        archivo= new File(System.getProperties().getProperty("user.dir") + File.separator + 
        "File" + File.separator + vnombreArchivo);
       
        try{
            StringBuffer buffer;
            String contenidoTemporal;
            RandomAccessFile fichero ;
           
            if (archivo.exists()){
                 fichero= new RandomAccessFile(archivo,"r");
                 buffer=new StringBuffer();
                 
                 while((contenidoTemporal=fichero.readLine())!=null){
                     
                    buffer.append(contenidoTemporal).append(System.getProperty("line.separator"));
                 }
                 
                
                fichero.close();
                
                contenidoTemporal=buffer.toString();  
                
                //contenidoArchivo= Encripcion.desencriptar(contenidoTemporal);
                contenidoArchivo=contenidoTemporal;
                
                if (Esquema.equals("ADMINBASESWITCH")==true || Esquema.equals("ESQUEMABASESWITCH")==true){
                    contenidoArchivo=contenidoArchivo.replace("v_esquema_swt varchar2(40):='ADMINSWITCH_ADQ';", "v_esquema_swt varchar2(40):='" + EsquemaBase + "';" );
                    contenidoArchivo=contenidoArchivo.replace("v_esquema_swt varchar2(40):='ADMINSWITCH';", "v_esquema_swt varchar2(40):='" + EsquemaBase + "';" );
                    contenidoArchivo=contenidoArchivo.replace("v_esquema varchar2(40):='ADMINSWITCH';", "v_esquema varchar2(40):='" + EsquemaBase + "';" );
                    
                    contenidoArchivo=contenidoArchivo.replace("lv_esquema_switch varchar2(40):= 'ADMINSWITCH_ADQ';", "lv_esquema_switch varchar2(40):= '" + EsquemaBase + "';" );
                    contenidoArchivo=contenidoArchivo.replace("lv_esquema_switch varchar2(40):= 'ADMINSWITCH';", "lv_esquema_switch varchar2(40):= '" + EsquemaBase + "';" );
                    
                    contenidoArchivo=contenidoArchivo.replace("v_table_space varchar2(40):='SWITCH_TRANSACCIONAL_ADQ';", "v_table_space varchar2(40):='" + nombreTableSpace + "';" ); 
                    contenidoArchivo=contenidoArchivo.replace("v_table_space varchar2(40):='SWITCH_TRANSACCIONAL';", "v_table_space varchar2(40):='" + nombreTableSpace + "';" ); 
                    
                    contenidoArchivo= contenidoArchivo.replace("&TRANSACCIONAL&", nombreTableSpace);
                    
                    
                    contenidoArchivo=contenidoArchivo.replace("v_clave_defecto varchar2(100):='hiper';", "v_clave_defecto varchar2(100):='" + new String( txtClaveEsquemaSwt.getPassword()) + "';" );
                    
                    contenidoArchivo=contenidoArchivo.replace("v_esquema_cuenta varchar2(40):='ADMIN_CUENTA_ADQ';", "v_esquema_cuenta varchar2(40):='" + EsquemaCliente + "';" );
                    contenidoArchivo=contenidoArchivo.replace("v_esquema_cuenta varchar2(40):='ADMIN_CUENTA';", "v_esquema_cuenta varchar2(40):='" + EsquemaCliente + "';" );
                    
                    contenidoArchivo=contenidoArchivo.replace("v_nombre_rol varchar(50):='CONTROL_SWITCH';", "v_nombre_rol varchar(50):='" + txtrolswt.getText() + "';");
                    
                    
                    //Por los Triguers
                    contenidoArchivo=contenidoArchivo.replace("admin_cuenta.",  EsquemaCliente + "."  );
                    contenidoArchivo=contenidoArchivo.replace("admin_cuenta_adq.",  EsquemaCliente + "."  );
                }
                
                if (Esquema.equals("ESQUEMABASELLAVES")==true || Esquema.equals("ADMINBASELLAVES")==true){
                    contenidoArchivo=contenidoArchivo.replace("v_esquema_swt varchar2(40):= 'ADMINLLAVES_ADQ';", "v_esquema_swt varchar2(40) := '" + EsquemaBase + "';" ); 
                    contenidoArchivo=contenidoArchivo.replace("v_table_space varchar2(40):= 'SWITCH_LLAVES_ADQ';", "v_table_space varchar2(40) := '" + nombreTableSpace + "';" ); 
                    
                    contenidoArchivo=contenidoArchivo.replace("v_esquema_swt varchar2(40):='ADMINLLAVES_ADQ';", "v_esquema_swt varchar2(40) :='" + EsquemaBase + "';" ); 
                    contenidoArchivo=contenidoArchivo.replace("v_table_space varchar2(40):='SWITCH_LLAVES_ADQ';", "v_table_space varchar2(40):='" + nombreTableSpace + "';" ); 
                    
                    contenidoArchivo=contenidoArchivo.replace("v_esquema_swt varchar2(40):= 'ADMINLLAVES';", "v_esquema_swt varchar2(40) := '" + EsquemaBase + "';" ); 
                    contenidoArchivo=contenidoArchivo.replace("v_table_space varchar2(40):= 'SWITCH_LLAVES';", "v_table_space varchar2(40) := '" + nombreTableSpace + "';" ); 
                    
                    contenidoArchivo=contenidoArchivo.replace("v_esquema_swt varchar2(40):='ADMINLLAVES';", "v_esquema_swt varchar2(40):='" + EsquemaBase + "';" ); 
                    contenidoArchivo=contenidoArchivo.replace("v_table_space varchar2(40):='SWITCH_LLAVES';","v_table_space varchar2(40):='" + nombreTableSpace + "';" ); 
                    
                    contenidoArchivo=contenidoArchivo.replace("v_esquema_engine varchar2(40):='ADMINSWITCH';", "v_esquema_engine varchar2(40):='" + txtEsquemaSwt.getText() + "';"  );

                    contenidoArchivo=contenidoArchivo.replace("v_clave_defecto varchar2(100):='hiper';", "v_clave_defecto varchar2(100):='" + new String( txtClaveEsquemaSeg.getPassword()) + "';" );
                    contenidoArchivo= contenidoArchivo.replace("v_nombre_rol varchar(50):='CONTROL_LLAVES';", "v_nombre_rol varchar(50):='" + txtrolseg.getText() + "';");
                }
                 
                if (Esquema.equals("ADMINBASECUENTA")==true || Esquema.equals("ESQUEMABASECUENTA")==true){
                    contenidoArchivo=contenidoArchivo.replace("v_esquema_swt varchar2(40):='ADMIN_CUENTA';", "v_esquema_swt varchar2(40):='" + this.EsquemaCliente + "';" ); 
                    contenidoArchivo=contenidoArchivo.replace("v_esquema_engine varchar2(40):='ADMINSWITCH';", "v_esquema_engine varchar2(40):='" + txtEsquemaSwt.getText() + "';" ); 
                    contenidoArchivo=contenidoArchivo.replace("v_table_space varchar2(40):='SWITCH_CUENTA_CLIENTE';", "v_table_space varchar2(40):='" + nombreTableSpace + "';" ); 
                    contenidoArchivo=contenidoArchivo.replace("v_clave_defecto varchar2(100):='hiper';", "v_clave_defecto varchar2(100):='" + this.ClaveEsquemaCliente + "';" );
                    
                    contenidoArchivo=contenidoArchivo.replace("v_nombre_rol varchar(50):='CONTROL_CUENTA';", "v_nombre_rol varchar(50):='" + this.nombreRolCliente + "';");
                    
                    contenidoArchivo=contenidoArchivo.replace("adminswitch_adq.",  txtEsquemaSwt.getText() + "."  );
                    contenidoArchivo=contenidoArchivo.replace("adminswitch.",  txtEsquemaSwt.getText() + "."  );
                      
                    contenidoArchivo=contenidoArchivo.replace("ADMINSWITCH_ADQ.",  txtEsquemaSwt.getText() + "."  );
                    contenidoArchivo=contenidoArchivo.replace("ADMINSWITCH.",  txtEsquemaSwt.getText() + "."  );
                    
                }
                  
                if(Esquema.equalsIgnoreCase("ADMINBASEDATOS")==true || Esquema.equalsIgnoreCase("ESQUEMABASEDATOS")==true){
                    contenidoArchivo=contenidoArchivo.replace("v_esquema_swt varchar2(40):='ADMIN_DATOS';", "v_esquema_swt varchar2(40):='" + this.EsquemaDatos + "';");
                    contenidoArchivo=contenidoArchivo.replace("v_table_space varchar2(40):='SWITCH_DATOS_VAULT';", "v_table_space varchar2(40):='" + nombreTableSpace + "';");
                    contenidoArchivo=contenidoArchivo.replace("v_esquema_engine varchar2(40):='ADMINSWITCH';", "v_esquema_engine varchar2(40):='" + txtEsquemaSwt.getText() + "';"  );
                    contenidoArchivo=contenidoArchivo.replace("v_esquema_cuenta varchar2(40):='ADMIN_CUENTA';", "v_esquema_cuenta varchar2(40):='" + EsquemaCliente + "';"  );
                    contenidoArchivo=contenidoArchivo.replace("v_clave_defecto varchar2(100):='hiper';", "v_clave_defecto varchar2(100):='" + this.ClaveEsquemaDatos + "';" );
                    
                    contenidoArchivo=contenidoArchivo.replace("v_nombre_rol varchar(50):='CONTROL_DATOS';", "v_nombre_rol varchar(50):='" + this.nombreRolDatos + "';");
                    
                    contenidoArchivo=contenidoArchivo.replace("adminswitch.",  txtEsquemaSwt.getText() + "."  );
                    contenidoArchivo=contenidoArchivo.replace("admin_cuenta.",  EsquemaCliente + "."  );
                }
                
                if (vnombreArchivo.equalsIgnoreCase("script_transaccional_01")==true || vnombreArchivo.equalsIgnoreCase("script_sinonimos")==true || vnombreArchivo.equalsIgnoreCase("script_cuenta_01")==true ){
                    archivo_sql= new File(System.getProperties().getProperty("user.dir") + File.separator + 
                    "File" + File.separator + vnombreArchivo + ".sql");
                    
                    if (archivo_sql.exists())
                        archivo_sql.delete();
                    
                    fichero= new RandomAccessFile(System.getProperties().getProperty("user.dir") + File.separator + "File" + File.separator + vnombreArchivo + ".sql","rw");
                    fichero.seek(0);
                    
                    fichero.writeBytes(contenidoArchivo);
                    fichero.close(); 
                }
                
                 //CODIGO QUE DESENCRIPTA LO ENCRIPTADFO
                 /*
                fichero= new RandomAccessFile(System.getProperties().getProperty("user.dir") + File.separator + "File" + File.separator + vnombreArchivo + "_DESCENCRIPTADO.txt","rw");
                fichero.seek(0);
           
                fichero.writeBytes(contenidoArchivo);
                fichero.close(); 
                */
                 
                 
            } //Fin de if (archivo.exists())
        }
        
        catch(IOException e){
            ;
        }
       
        return contenidoArchivo;
    }
    
  
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnaceptar;
    private javax.swing.JButton btnposmobil;
    private javax.swing.JButton btnrutaengine;
    private javax.swing.JButton btnrutaseg;
    private javax.swing.JComboBox<String> cbotipoconengine;
    private javax.swing.JComboBox<String> cbotipoconseg;
    private javax.swing.JLabel lblBaseSeg;
    private javax.swing.JLabel lblClaveBaseSeg;
    private javax.swing.JLabel lblClaveEsquemaSeg;
    private javax.swing.JLabel lblClaveEsquemaSwt;
    private javax.swing.JLabel lblClaveUsuarioSwt;
    private javax.swing.JLabel lblEsquemaSwt;
    private javax.swing.JLabel lblPuertoEsquemaSeg;
    private javax.swing.JLabel lblPuertoSwt;
    private javax.swing.JLabel lblServicioSeg;
    private javax.swing.JLabel lblServicioSwt;
    private javax.swing.JLabel lblServidorEsquemaSwt;
    private javax.swing.JLabel lblServidorSeg;
    private javax.swing.JLabel lblTablespaceSeg;
    private javax.swing.JLabel lblTablespaceSwt;
    private javax.swing.JLabel lblTipoConEngine;
    private javax.swing.JLabel lblTipoConSeg;
    private javax.swing.JLabel lblUsuarioBaseSeg;
    private javax.swing.JLabel lblUsuarioSwt;
    private javax.swing.JLabel lblproducto;
    private javax.swing.JLabel lblrolseg;
    private javax.swing.JLabel lblrolswt;
    private javax.swing.JLabel lblrutaengine;
    private javax.swing.JLabel lblrutaseg;
    private javax.swing.JPanel pnlcontenedor;
    private javax.swing.JPanel pnlformaengine;
    private javax.swing.JPasswordField txtClaveEsquemaSeg;
    private javax.swing.JPasswordField txtClaveEsquemaSwt;
    private javax.swing.JPasswordField txtClaveSwt;
    private javax.swing.JPasswordField txtClaveUsuarioSeg;
    private javax.swing.JTextField txtEsquemaSeg;
    private javax.swing.JTextField txtEsquemaSwt;
    private javax.swing.JTextField txtPuertoEsquemaSeg;
    private javax.swing.JTextField txtPuertoSwt;
    private javax.swing.JTextField txtServicioSeg;
    private javax.swing.JTextField txtServicioSwt;
    private javax.swing.JTextField txtServidorEsquemaSeg;
    private javax.swing.JTextField txtServidorSwt;
    private javax.swing.JTextField txtTableSpaceSeg;
    private javax.swing.JTextField txtTablespaceSwt;
    private javax.swing.JTextField txtUsuarioSeg;
    private javax.swing.JTextField txtUsuarioSwt;
    private javax.swing.JTextField txtestadoposmobil;
    private javax.swing.JTextField txtrolseg;
    private javax.swing.JTextField txtrolswt;
    private javax.swing.JTextField txtrutaengine;
    private javax.swing.JTextField txtrutaseg;
    // End of variables declaration//GEN-END:variables
}
