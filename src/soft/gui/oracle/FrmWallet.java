
/******************************************************************************/
//                      Clase FrmWallet Versión 2.0
//Objetivo:        Interfaz GUI de ingreso de informacion de Configracion BD Wallet
//Fecha Creación:  2019-03-02
//Responsable:     Sistemas
//Empresa:         Hiper S.A
/******************************************************************************/

package soft.gui.oracle;

import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;


public class FrmWallet extends javax.swing.JFrame {
    private final JDialog dialogo;
    private final String vmotorBaseDatos;
    
    public String getRolCliente(){
        
        if (vmotorBaseDatos.equalsIgnoreCase("ORACLE"))
            return this.txtrolcliente.getText();
        else 
            return null;
    }
    
    public String getRolDatos(){
        
        if (vmotorBaseDatos.equalsIgnoreCase("ORACLE"))
            return this.txtRolDatos.getText();
        else
            return null;
    }
    
    public String getEsquemaCliente(){
        return txtesquema_cliente.getText();
    }
    
    public String getClaveEsquemaCliente(){
        return new String(this.txtclave_cliente.getPassword());
    }
    
    public String getTableSpaceCliente(){
        return txttablespace_cliente.getText();
    }
    
    public String getTableSpaceDatos(){
        return this.txtTableSpaceDatos.getText();
    }
    
    public String getEsquemaDatos(){
        return this.txtEsquemaDatos.getText();
    }
    
    public String getClaveEsquemaDatos() {
        return new String(this.txtClaveDatos.getPassword());
    }
    
    private PlainDocument getUpcaseFilter(int limite){
        PlainDocument ofiltrocase=null;
     
        ofiltrocase= new PlainDocument(){
                        
            //public void insertString(DocumentFilter.FilterBypass fb, int offset,String text, AttributeSet attr) throws BadLocationException {
             public void insertString(int offset, String text, AttributeSet attr)  throws BadLocationException {

                if (text == null)
                    return;

                 if ((getLength() + text.trim().length()) <= limite){

                    if (vmotorBaseDatos.equalsIgnoreCase("ORACLE")==true)
                      super.insertString(offset, text.replaceAll("\\W", "").toUpperCase(), attr);  // remove non-digits
                    else 
                       super.insertString(offset, text.replaceAll("\\W", ""), attr); 
                    
                    
                 }
            }
        };
        
        return ofiltrocase;
    }
    
    public FrmWallet(JDialog odialogo, String EsqemaCliente, 
    String TableSpaceCliente, String claveEsquemaCliente, 
    String EsquemaWallet, String TableSpaceWallet, String claveWallet, 
    String nombreRolCliente, String nombreRolDatos, String motorBase) {
        GridLayout olayaout=null;
        
        vmotorBaseDatos=motorBase;
        
        initComponents();
        
        if (vmotorBaseDatos.equalsIgnoreCase("SQLSERVER")){
          
            lblTableSpaceCliente.setText("Base de Datos Cliente:");
            lblTablSpaceDatos.setText("Base de Datos Vault:");
            
            pnlcontendor.remove(lblrolcliente);
            pnlcontendor.remove(txtRolDatos);
            
            pnlcontendor.remove(lblroldatos);
            pnlcontendor.remove(txtrolcliente);
            
            
            olayaout= (GridLayout) this.pnlcontendor.getLayout();
            olayaout.setRows(7);
         
            pnlprincipal.setPreferredSize(new Dimension(433,189));
        }
        
         dialogo=odialogo;
         dialogo.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
         
        if(nombreRolCliente!=null)
            this.txtrolcliente.setText(nombreRolCliente);
      
         
        if(nombreRolDatos!=null)
            this.txtRolDatos.setText(nombreRolDatos);
        
        if(EsqemaCliente!=null)
              this.txtesquema_cliente.setText(EsqemaCliente);
        else if(vmotorBaseDatos.equalsIgnoreCase("SQLSERVER"))
              this.txtesquema_cliente.setText("admin_cuenta");   
        
        if(TableSpaceCliente!=null)
            this.txttablespace_cliente.setText(TableSpaceCliente);
         else if(vmotorBaseDatos.equalsIgnoreCase("SQLSERVER")==true)
              this.txttablespace_cliente.setText("DBCUENTA");
        
        if(claveEsquemaCliente!=null)
            txtclave_cliente.setText(claveEsquemaCliente);
        
        if(EsquemaWallet!=null)
            this.txtEsquemaDatos.setText(EsquemaWallet);
        else if(vmotorBaseDatos.equalsIgnoreCase("SQLSERVER")==true)
              this.txtEsquemaDatos.setText("admin_datos");
        
        if(TableSpaceWallet!=null)
            this.txtTableSpaceDatos.setText(TableSpaceWallet);
        else if(vmotorBaseDatos.equalsIgnoreCase("SQLSERVER")==true)
             this.txtTableSpaceDatos.setText("DBDATOS");
        
        if(claveWallet!=null)
            this.txtClaveDatos.setText(claveWallet);
           
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlprincipal = new javax.swing.JPanel();
        pnlcontendor = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtesquema_cliente = new javax.swing.JTextField();
        lblClaveCliente = new javax.swing.JLabel();
        txtclave_cliente = new javax.swing.JPasswordField();
        lblTableSpaceCliente = new javax.swing.JLabel();
        txttablespace_cliente = new javax.swing.JTextField();
        lblrolcliente = new javax.swing.JLabel();
        txtrolcliente = new javax.swing.JTextField();
        lblSep1 = new javax.swing.JLabel();
        lblSep2 = new javax.swing.JLabel();
        lblEsquemaDatos = new javax.swing.JLabel();
        txtEsquemaDatos = new javax.swing.JTextField();
        lblClaveDatos = new javax.swing.JLabel();
        txtClaveDatos = new javax.swing.JPasswordField();
        lblTablSpaceDatos = new javax.swing.JLabel();
        txtTableSpaceDatos = new javax.swing.JTextField();
        lblroldatos = new javax.swing.JLabel();
        txtRolDatos = new javax.swing.JTextField();
        pnlcomando = new javax.swing.JPanel();
        btnaceptar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Configuraciòn Wallet");
        setName("frmwallet"); // NOI18N
        setUndecorated(true);
        setPreferredSize(new java.awt.Dimension(426, 250));
        setResizable(false);
        setType(java.awt.Window.Type.POPUP);

        pnlprincipal.setPreferredSize(new java.awt.Dimension(426, 230));
        pnlprincipal.setVerifyInputWhenFocusTarget(false);

        pnlcontendor.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEtchedBorder(java.awt.Color.white, null), javax.swing.BorderFactory.createEmptyBorder(3, 5, 5, 5)));
        pnlcontendor.setPreferredSize(new java.awt.Dimension(423, 228));
        pnlcontendor.setLayout(new java.awt.GridLayout(9, 2, 2, 2));

        jLabel1.setText("Nombre Esquema Cliente:");
        pnlcontendor.add(jLabel1);

        txtesquema_cliente.setDocument(getUpcaseFilter(20));
        txtesquema_cliente.setText("ADMIN_CUENTA");
        pnlcontendor.add(txtesquema_cliente);

        lblClaveCliente.setText("Contraseña Esquema Cliente:");
        pnlcontendor.add(lblClaveCliente);

        txtclave_cliente.setText("hiper");
        pnlcontendor.add(txtclave_cliente);

        lblTableSpaceCliente.setText("Nombre de TableSpace Cliente:");
        pnlcontendor.add(lblTableSpaceCliente);

        txttablespace_cliente.setDocument(getUpcaseFilter(30));
        txttablespace_cliente.setText("SWITCH_CUENTA_CLIENTE");
        pnlcontendor.add(txttablespace_cliente);

        lblrolcliente.setText("Nombre de Rol Cliente:");
        pnlcontendor.add(lblrolcliente);

        txtrolcliente.setDocument(getUpcaseFilter(20));
        txtrolcliente.setText("CONTROL_CUENTA");
        pnlcontendor.add(txtrolcliente);
        pnlcontendor.add(lblSep1);
        pnlcontendor.add(lblSep2);

        lblEsquemaDatos.setText("Nombre Esquema Datos:");
        pnlcontendor.add(lblEsquemaDatos);

        txtEsquemaDatos.setDocument(getUpcaseFilter(20));
        txtEsquemaDatos.setText("ADMIN_DATOS");
        pnlcontendor.add(txtEsquemaDatos);

        lblClaveDatos.setText("Contraseña Esquema Datos:");
        pnlcontendor.add(lblClaveDatos);

        txtClaveDatos.setText("hiper");
        pnlcontendor.add(txtClaveDatos);

        lblTablSpaceDatos.setText("Nombre de TableSpace Datos:");
        pnlcontendor.add(lblTablSpaceDatos);

        txtTableSpaceDatos.setDocument(getUpcaseFilter(30));
        txtTableSpaceDatos.setText("SWITCH_DATOS_VAULT");
        pnlcontendor.add(txtTableSpaceDatos);

        lblroldatos.setText("Nombre Rol Datos:");
        pnlcontendor.add(lblroldatos);

        txtRolDatos.setDocument(getUpcaseFilter(20));
        txtRolDatos.setText("CONTROL_DATOS");
        pnlcontendor.add(txtRolDatos);

        javax.swing.GroupLayout pnlprincipalLayout = new javax.swing.GroupLayout(pnlprincipal);
        pnlprincipal.setLayout(pnlprincipalLayout);
        pnlprincipalLayout.setHorizontalGroup(
            pnlprincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlprincipalLayout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(pnlcontendor, javax.swing.GroupLayout.DEFAULT_SIZE, 410, Short.MAX_VALUE)
                .addGap(8, 8, 8))
        );
        pnlprincipalLayout.setVerticalGroup(
            pnlprincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlprincipalLayout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(pnlcontendor, javax.swing.GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE)
                .addGap(2, 2, 2))
        );

        getContentPane().add(pnlprincipal, java.awt.BorderLayout.PAGE_START);

        btnaceptar.setText("Aceptar");
        btnaceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaceptarActionPerformed(evt);
            }
        });
        pnlcomando.add(btnaceptar);

        getContentPane().add(pnlcomando, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnaceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaceptarActionPerformed
       if(validar())
            dialogo.dispose();
    }//GEN-LAST:event_btnaceptarActionPerformed

    
    private boolean validar(){
        boolean resultado=true;
        
        if( txtesquema_cliente.getText().trim().length()==0){
            resultado=false;
            if(vmotorBaseDatos.equalsIgnoreCase("ORACLE"))
                JOptionPane.showMessageDialog(this,"Ingrese el nombre del Esquema Cliente"  , "Mensaje del Sisterma", JOptionPane.ERROR_MESSAGE);
            else if (vmotorBaseDatos.equalsIgnoreCase("SQLSERVER"))
                 JOptionPane.showMessageDialog(this,"Ingrese el nombre de usuario de Base de Datos Cliente"  , "Mensaje del Sisterma", JOptionPane.ERROR_MESSAGE);
        }
        
        if(resultado==true){
            if(txttablespace_cliente.getText().trim().length()==0){
                resultado=false;
                
                if(vmotorBaseDatos.equalsIgnoreCase("ORACLE"))
                    JOptionPane.showMessageDialog(this,"Ingrese el nombre del TableSpace Esquema Cliente"  , "Mensaje del Sisterma", JOptionPane.ERROR_MESSAGE);
                else if (vmotorBaseDatos.equalsIgnoreCase("SQLSERVER"))
                    JOptionPane.showMessageDialog(this,"Ingrese el nombre de Base de Datos Cliente"  , "Mensaje del Sisterma", JOptionPane.ERROR_MESSAGE);
            }
        }
        
        if(resultado==true){
             if(txtrolcliente.getText().length()==0 && vmotorBaseDatos.equalsIgnoreCase("ORACLE")){
                resultado=false;
                JOptionPane.showMessageDialog(this,"Ingrese el nombre del rol del Esquema Cliente"  , "Mensaje del Sisterma", JOptionPane.ERROR_MESSAGE);
            }
        }
        
        if(resultado==true){
            if( txtclave_cliente.getPassword().length==0){
                resultado=false;
                if(vmotorBaseDatos.equalsIgnoreCase("ORACLE"))
                    JOptionPane.showMessageDialog(this,"Ingrese la contraseña del Esquuema Cliente"  , "Mensaje del Sisterma", JOptionPane.ERROR_MESSAGE);
                else if (vmotorBaseDatos.equalsIgnoreCase("SQLSERVER"))
                    JOptionPane.showMessageDialog(this,"Ingrese la contaseña del usuario de Base de Datos Cliente"  , "Mensaje del Sisterma", JOptionPane.ERROR_MESSAGE);
            }
        }
        
        if(resultado==true){
            if(txtEsquemaDatos.getText().length()==0){
            
                resultado=false;
                
                if(vmotorBaseDatos.equalsIgnoreCase("ORACLE"))
                    JOptionPane.showMessageDialog(this,"Ingrese nombre del Esquuema de Datos"  , "Mensaje del Sisterma", JOptionPane.ERROR_MESSAGE);
                else if (vmotorBaseDatos.equalsIgnoreCase("SQLSERVER"))
                    JOptionPane.showMessageDialog(this,"Ingrese el nombre de usuario de Base de Datos Vault"  , "Mensaje del Sisterma", JOptionPane.ERROR_MESSAGE);
            }
        }
        
        if(resultado==true){
            if(txtClaveDatos.getPassword().length==0){
                resultado=false;
                
                if(vmotorBaseDatos.equalsIgnoreCase("ORACLE"))
                    JOptionPane.showMessageDialog(this,"Ingrese la contraseña del Esquuema Datos"  , "Mensaje del Sisterma", JOptionPane.ERROR_MESSAGE);
                else if (vmotorBaseDatos.equalsIgnoreCase("SQLSERVER"))
                    JOptionPane.showMessageDialog(this,"Ingrese la contraseña de Base de Datos Vault"  , "Mensaje del Sisterma", JOptionPane.ERROR_MESSAGE);
            }
        }
        
        if(resultado==true){
            if(txtTableSpaceDatos.getText().length()==0){
                resultado=false;
                
                if(vmotorBaseDatos.equalsIgnoreCase("ORACLE"))
                    JOptionPane.showMessageDialog(this,"Ingrese el nombre del TableSpace del Esquema Datos"  , "Mensaje del Sisterma", JOptionPane.ERROR_MESSAGE);
                 else if (vmotorBaseDatos.equalsIgnoreCase("SQLSERVER"))
                     JOptionPane.showMessageDialog(this,"Ingrese el nombre de Base de Datos Vault"  , "Mensaje del Sisterma", JOptionPane.ERROR_MESSAGE);
            }
        }
        
        if(resultado==true){
             if(txtRolDatos.getText().length()==0 && vmotorBaseDatos.equalsIgnoreCase("ORACLE")){
                resultado=false;
                JOptionPane.showMessageDialog(this,"Ingrese el nombre del rol de usuario del Esquema  Datos"  , "Mensaje del Sisterma", JOptionPane.ERROR_MESSAGE);
            }
        }
        
        return resultado;
    }
  

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnaceptar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel lblClaveCliente;
    private javax.swing.JLabel lblClaveDatos;
    private javax.swing.JLabel lblEsquemaDatos;
    private javax.swing.JLabel lblSep1;
    private javax.swing.JLabel lblSep2;
    private javax.swing.JLabel lblTablSpaceDatos;
    private javax.swing.JLabel lblTableSpaceCliente;
    private javax.swing.JLabel lblrolcliente;
    private javax.swing.JLabel lblroldatos;
    private javax.swing.JPanel pnlcomando;
    private javax.swing.JPanel pnlcontendor;
    private javax.swing.JPanel pnlprincipal;
    private javax.swing.JPasswordField txtClaveDatos;
    private javax.swing.JTextField txtEsquemaDatos;
    private javax.swing.JTextField txtRolDatos;
    private javax.swing.JTextField txtTableSpaceDatos;
    private javax.swing.JPasswordField txtclave_cliente;
    private javax.swing.JTextField txtesquema_cliente;
    private javax.swing.JTextField txtrolcliente;
    private javax.swing.JTextField txttablespace_cliente;
    // End of variables declaration//GEN-END:variables

}
