
/******************************************************************************/
//                       Clase FrmBaseSqlServer Versión 20.
//Objetivo:        Interfaz GUI para capturar los datos de creación de Base de Datos SqlServer
//Fecha Creación:  2019-03-02
//Responsable:     Sistemas
//Empresa:         Hiper S.A
/******************************************************************************/

package soft.gui.sqlserver;

import java.awt.Component;
import java.awt.Container;
import java.awt.Image;
import java.awt.event.ItemEvent;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import javax.imageio.ImageIO;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import javax.swing.text.PlainDocument;
import seg.Encripcion;
import soft.deployment.ArchivoComandos;
import soft.deployment.deploymentLic;
import soft.deployment.parametrosConexion;
import soft.gui.FrminfoProceso;
import soft.gui.oracle.FrmBaseOracle;
import soft.gui.oracle.FrmPosMobil;
import soft.gui.oracle.FrmWallet;


public class FrmBaseSqlServer extends javax.swing.JFrame {
    private parametrosConexion conexionBaseEsquemaSwt,  conexionBaseAdminSwt;
    private parametrosConexion conexionBaseEsquemaLlaves, conexionBaseAdminLlaves;
    private parametrosConexion conexionBaseAdminClientes, conexionBaseEsquemaClientes;
    private parametrosConexion conexionBaseAdminDatos, conexionBaseEsquemaDatos;
  
    private String EsquemaCliente,ClaveEsquemaCliente;
    private String TableSpaceEsquemaCliente;
    
    private String EsquemaDatos, ClaveEsquemaDatos;
    private String TableSpaceDatos;
    
    private String nombreRolDatos, nombreRolCliente;
    
    private deploymentLic licencia;
    
     private void enableComponents(Container container, boolean enable) {
        Component[] components = container.getComponents();
        
        for (Component component : components) {
            component.setEnabled(enable);
            
            if (component instanceof Container) {
                
                enableComponents((Container)component, enable);  
            }
        }
    }
    
    private PlainDocument getUpcaseFilter(int limite, boolean flagupcase){
        PlainDocument ofiltrocase;
        //PlainDocument o=null;
      
        ofiltrocase= new PlainDocument(){
                             
            //public void insertString(DocumentFilter.FilterBypass fb, int offset,String text, AttributeSet attr) throws BadLocationException {
            @Override
            public void insertString(int offset, String text, AttributeSet attr)  throws BadLocationException {

                if (text == null)
                    return;


                 if ((getLength() + text.trim().length()) <= limite){
                    if(flagupcase)
                        super.insertString(offset, text.replaceAll("\\W", "").toUpperCase(), attr);  // remove non-digits
                    else 
                        super.insertString(offset, text.replaceAll("\\W", ""), attr);
                 }
            }
        };
        
        return ofiltrocase;
    }
    
    private DocumentFilter getDigitosFilter(int limite){
        DocumentFilter ofilnumeros;
        
        ofilnumeros= new DocumentFilter(){
      
            @Override
            public void insertString(DocumentFilter.FilterBypass fb, int offset,String text, AttributeSet attr) throws BadLocationException {

                if ( text.trim().length() <= limite)
                    fb.insertString(offset, text.replaceAll("\\D++", ""), attr);  // remove non-digits
            }

            @Override
            public void replace(DocumentFilter.FilterBypass fb, int offset, int length,String text, AttributeSet attr) throws BadLocationException {
                if ( text.trim().length() <= limite) 
                    fb.replace(offset, length, text.replaceAll("\\D++", ""), attr);  // remove non-digits
                 //fb.replace(offset, length, text.replaceAll("\\W", "").toUpperCase(), attr);  // remove non-digits
            }

        };
        
        return ofilnumeros;
    }
    
    public FrmBaseSqlServer(deploymentLic infolicencia) {
        initComponents();
        
        EsquemaCliente=null;
        ClaveEsquemaCliente=null;
        TableSpaceEsquemaCliente=null;

        EsquemaDatos=null;
        ClaveEsquemaDatos=null;
        TableSpaceDatos=null;
        
        nombreRolCliente=null;
        nombreRolDatos=null;
        
        InputStream imgStream;
        Image imagen1;
        licencia=infolicencia;
        
        try{
            imgStream = FrmBaseOracle.class.getResourceAsStream("/imagenes/base.jpg");
            imagen1 = ImageIO.read(imgStream);
            
            if (imagen1!=null)
                setIconImage(imagen1);
            
            setLocationRelativeTo(null); 

            if (licencia.getModulo().equalsIgnoreCase("POSMOVIL")){
                txtestadoposmobil.setText("NO CONFIGURADO");
                lblproducto.setText("Módulo Pos Mobil:");
                btnposmobil.setEnabled(true);
            }
            else if(licencia.getModulo().equalsIgnoreCase("WALLET")){
                txtestadoposmobil.setText("NO CONFIGURADO");
                lblproducto.setText("Módulo Wallet:");
                btnposmobil.setEnabled(true);
            }
            else{
                 txtestadoposmobil.setText("NO DISPONIBLE");
                 btnposmobil.setEnabled(false);     
            }
            
            
                
        }
        catch(IOException e){
            ;// e.printStackTrace();
        }
        
    }
    
    private void procesar(){
        
        if(llenaarDatosBase() ){   
        
            JDialog odialogo= new JDialog(this,true);

            FrminfoProceso ofrmproceso=new FrminfoProceso(odialogo,licencia, 
            conexionBaseEsquemaSwt,conexionBaseAdminSwt,
            conexionBaseEsquemaLlaves,conexionBaseAdminLlaves,
            conexionBaseAdminClientes,conexionBaseEsquemaClientes,
            conexionBaseAdminDatos,conexionBaseEsquemaDatos);

            odialogo.setTitle(ofrmproceso.getTitle());
            odialogo.add(ofrmproceso.getContentPane());
            odialogo.setSize(ofrmproceso.getSize());
            odialogo.setResizable(false);

            odialogo.pack();
            odialogo.setLocationRelativeTo(this);

            odialogo.setVisible(true);
            
            enableComponents(pnlformaengine,false);
            btnposmobil.setEnabled(false);
            btnaceptar.setEnabled(false);

            btnaceptar.setEnabled(true);
            enableComponents(pnlformaengine,true);   
            
            if(cbotipoconengine.getSelectedIndex()==1)
                btnrutaengine.setEnabled(false);
            
            if (cbotipoconseg.getSelectedIndex()==1)
                btnrutaseg.setEnabled(false);
            
            if (licencia.getModulo().equalsIgnoreCase("NA"))
                btnposmobil.setEnabled(false);   
        }
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlcontenedor = new javax.swing.JPanel();
        pnlformaengine = new javax.swing.JPanel();
        lbltipoconexionengine = new javax.swing.JLabel();
        cbotipoconengine = new javax.swing.JComboBox<>();
        lblServidorEsquemaSwt = new javax.swing.JLabel();
        txtServidorSwt = new javax.swing.JTextField();
        lblUsuarioSwt = new javax.swing.JLabel();
        txtUsuarioSwt = new javax.swing.JTextField();
        lblClaveUsuarioSwt = new javax.swing.JLabel();
        txtClaveSwt = new javax.swing.JPasswordField();
        lblPuertoSwt = new javax.swing.JLabel();
        txtPuertoSwt = new javax.swing.JTextField();
        lblServicioSwt = new javax.swing.JLabel();
        txtBaseEngine = new javax.swing.JTextField();
        lblEsquemaSwt = new javax.swing.JLabel();
        txtUsuarioEngine = new javax.swing.JTextField();
        blbclavengine = new javax.swing.JLabel();
        txtclavenegine = new javax.swing.JPasswordField();
        lblrutaengine = new javax.swing.JLabel();
        txtrutaengine = new javax.swing.JTextField();
        btnrutaengine = new javax.swing.JButton();
        lblrutaengine1 = new javax.swing.JLabel();
        lblhostiname = new javax.swing.JLabel();
        txtHostNameCertEngine = new javax.swing.JTextField();
        lblTipoConSeg = new javax.swing.JLabel();
        cbotipoconseg = new javax.swing.JComboBox<>();
        lblServidorSeg = new javax.swing.JLabel();
        txtServidorEsquemaSeg = new javax.swing.JTextField();
        lblUsuarioBaseSeg = new javax.swing.JLabel();
        txtUsuarioSeg = new javax.swing.JTextField();
        lblClaveBaseSeg = new javax.swing.JLabel();
        txtClaveUsuarioSeg = new javax.swing.JPasswordField();
        lblPuertoEsquemaSeg = new javax.swing.JLabel();
        txtPuertoEsquemaSeg = new javax.swing.JTextField();
        lblServicioSwt1 = new javax.swing.JLabel();
        txtBaseSeg = new javax.swing.JTextField();
        lblBaseSeg = new javax.swing.JLabel();
        txtUsuarioLllave = new javax.swing.JTextField();
        lblClaveEsquemaSeg = new javax.swing.JLabel();
        txtClaveEsquemaSeg = new javax.swing.JPasswordField();
        lblRutaCertSeg = new javax.swing.JLabel();
        txtrutaseg = new javax.swing.JTextField();
        btnrutaseg = new javax.swing.JButton();
        lblhostinameSeg = new javax.swing.JLabel();
        txtNomHostCertSeg = new javax.swing.JTextField();
        lblproducto = new javax.swing.JLabel();
        txtestadoposmobil = new javax.swing.JTextField();
        btnposmobil = new javax.swing.JButton();
        lblClaveCertSeg = new javax.swing.JLabel();
        txtClaveCertEngine = new javax.swing.JPasswordField();
        txtClaveCertSeg = new javax.swing.JPasswordField();
        pnlcomando = new javax.swing.JPanel();
        btnaceptar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Módulo de Instalación de  Base Datos UC-Engine PCI 3.0");
        setResizable(false);

        pnlformaengine.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEtchedBorder(java.awt.Color.white, null), javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5)));

        lbltipoconexionengine.setText("Tipo Conexión Base Engine:");

        cbotipoconengine.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Aut. SQL SSL ", "Aut. SQL Estandar" }));
        cbotipoconengine.setMinimumSize(new java.awt.Dimension(166, 23));
        cbotipoconengine.setPreferredSize(new java.awt.Dimension(166, 23));
        cbotipoconengine.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbotipoconengineItemStateChanged(evt);
            }
        });

        lblServidorEsquemaSwt.setText("Servidor Base Engine:");

        txtServidorSwt.setPreferredSize(new java.awt.Dimension(6, 23));
        txtServidorSwt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtServidorSwtkeyTyped(evt);
            }
        });

        lblUsuarioSwt.setText("Usuario Motor  Base Datos  Engine:");

        txtUsuarioSwt.setText("sa");
        txtUsuarioSwt.setPreferredSize(new java.awt.Dimension(17, 23));

        lblClaveUsuarioSwt.setText("Clave usuario Motor  Base Engine:");

        txtClaveSwt.setMinimumSize(new java.awt.Dimension(6, 23));
        txtClaveSwt.setPreferredSize(new java.awt.Dimension(6, 23));

        lblPuertoSwt.setText("Puerto Motor Base Engine:");

        ((AbstractDocument) txtPuertoSwt.getDocument()).setDocumentFilter(getDigitosFilter(5));
        txtPuertoSwt.setText("1305");
        txtPuertoSwt.setMinimumSize(new java.awt.Dimension(6, 23));
        txtPuertoSwt.setPreferredSize(new java.awt.Dimension(30, 23));
        txtPuertoSwt.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPuertoSwtkeyTyped(evt);
            }
        });

        lblServicioSwt.setText("Nombre Base de Datos Engine:");

        txtBaseEngine.setDocument(getUpcaseFilter(20,true));
        txtBaseEngine.setText("DBSWITCH");
        txtBaseEngine.setMinimumSize(new java.awt.Dimension(6, 23));

        lblEsquemaSwt.setText("Usuario Base Engine:");

        txtUsuarioEngine.setDocument(getUpcaseFilter(20,false));
        txtUsuarioEngine.setText("adminswitch");
        txtUsuarioEngine.setPreferredSize(new java.awt.Dimension(17, 23));

        blbclavengine.setText("Clave Usuario Base Engine:");

        txtclavenegine.setText("hiper");
        txtclavenegine.setMinimumSize(new java.awt.Dimension(6, 23));
        txtclavenegine.setPreferredSize(new java.awt.Dimension(6, 23));

        lblrutaengine.setText("Ruta Certificado Base Engine:");

        txtrutaengine.setEditable(false);
        txtrutaengine.setPreferredSize(new java.awt.Dimension(6, 23));

        btnrutaengine.setText("....");
        btnrutaengine.setPreferredSize(new java.awt.Dimension(49, 20));
        btnrutaengine.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnrutaengineActionPerformed(evt);
            }
        });

        lblrutaengine1.setText("Clave Certificado Base Engine:");

        lblhostiname.setText("Host Engine (HostNameInCertifcate):");

        txtHostNameCertEngine.setPreferredSize(new java.awt.Dimension(17, 23));

        lblTipoConSeg.setText("Tipo Conexión Base Seguridad:");

        cbotipoconseg.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Aut. SQL SSL ", "Aut. SQL Estandar" }));
        cbotipoconseg.setMinimumSize(new java.awt.Dimension(166, 23));
        cbotipoconseg.setPreferredSize(new java.awt.Dimension(166, 23));
        cbotipoconseg.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbotipoconsegItemStateChanged(evt);
            }
        });

        lblServidorSeg.setText("Servidor Base Seguridad:");

        txtServidorEsquemaSeg.setPreferredSize(new java.awt.Dimension(6, 23));

        lblUsuarioBaseSeg.setText("Usuario Motor Base  Datos Seguridad:");

        txtUsuarioSeg.setText("sa");
        txtUsuarioSeg.setPreferredSize(new java.awt.Dimension(40, 23));

        lblClaveBaseSeg.setText("Clave usuario Motor  Base Seguridad:");

        txtClaveUsuarioSeg.setPreferredSize(new java.awt.Dimension(6, 23));

        lblPuertoEsquemaSeg.setText("Puerto Motor Base Seguidad:");

        ((AbstractDocument) txtPuertoEsquemaSeg.getDocument()).setDocumentFilter(getDigitosFilter(5));
        txtPuertoEsquemaSeg.setText("1305");
        txtPuertoEsquemaSeg.setPreferredSize(new java.awt.Dimension(30, 23));
        txtPuertoEsquemaSeg.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPuertoEsquemaSegkeyTyped(evt);
            }
        });

        lblServicioSwt1.setText("Nombre Base Datos Seguridad:");

        txtBaseSeg.setDocument(getUpcaseFilter(20,true));
        txtBaseSeg.setText("DBLLAVES");
        txtBaseSeg.setMinimumSize(new java.awt.Dimension(6, 23));

        lblBaseSeg.setText("Usuario Base Seguridad:");

        txtUsuarioLllave.setDocument(getUpcaseFilter(20,false));
        txtUsuarioLllave.setText("adminllaves");
        txtUsuarioLllave.setPreferredSize(new java.awt.Dimension(17, 23));

        lblClaveEsquemaSeg.setText("Clave usuario Base Seguridad:");

        txtClaveEsquemaSeg.setText("hiper");
        txtClaveEsquemaSeg.setPreferredSize(new java.awt.Dimension(41, 23));

        lblRutaCertSeg.setText("Ruta Certificado Base Seguridad:");

        txtrutaseg.setEditable(false);
        txtrutaseg.setPreferredSize(new java.awt.Dimension(6, 23));

        btnrutaseg.setText("....");
        btnrutaseg.setPreferredSize(new java.awt.Dimension(49, 20));
        btnrutaseg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnrutasegActionPerformed(evt);
            }
        });

        lblhostinameSeg.setText("Host Seguridad (HostNameInCertifcate):");

        txtNomHostCertSeg.setPreferredSize(new java.awt.Dimension(17, 23));

        lblproducto.setText("Módulo:");

        txtestadoposmobil.setEditable(false);
        txtestadoposmobil.setPreferredSize(new java.awt.Dimension(97, 23));
        txtestadoposmobil.setRequestFocusEnabled(false);

        btnposmobil.setText("...");
        btnposmobil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnposmobilActionPerformed(evt);
            }
        });

        lblClaveCertSeg.setText("Clave Certificado Base Seguridad:");

        txtClaveCertEngine.setMinimumSize(new java.awt.Dimension(6, 23));
        txtClaveCertEngine.setPreferredSize(new java.awt.Dimension(6, 23));

        txtClaveCertSeg.setMinimumSize(new java.awt.Dimension(6, 23));
        txtClaveCertSeg.setPreferredSize(new java.awt.Dimension(6, 23));

        javax.swing.GroupLayout pnlformaengineLayout = new javax.swing.GroupLayout(pnlformaengine);
        pnlformaengine.setLayout(pnlformaengineLayout);
        pnlformaengineLayout.setHorizontalGroup(
            pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlformaengineLayout.createSequentialGroup()
                .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pnlformaengineLayout.createSequentialGroup()
                        .addComponent(lblPuertoSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtPuertoSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlformaengineLayout.createSequentialGroup()
                        .addComponent(lblClaveUsuarioSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)
                        .addComponent(txtClaveSwt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(pnlformaengineLayout.createSequentialGroup()
                        .addComponent(lblUsuarioSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(40, 40, 40)
                        .addComponent(txtUsuarioSwt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(pnlformaengineLayout.createSequentialGroup()
                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblServidorEsquemaSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbltipoconexionengine, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(79, 79, 79)
                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtServidorSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbotipoconengine, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(pnlformaengineLayout.createSequentialGroup()
                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblServicioSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblTipoConSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(26, 26, 26)
                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtBaseEngine, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cbotipoconseg, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(pnlformaengineLayout.createSequentialGroup()
                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblproducto, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblServicioSwt1, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(57, 57, 57)
                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtBaseSeg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtestadoposmobil, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(pnlformaengineLayout.createSequentialGroup()
                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(pnlformaengineLayout.createSequentialGroup()
                                    .addComponent(lblPuertoEsquemaSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(65, 65, 65))
                                .addGroup(pnlformaengineLayout.createSequentialGroup()
                                    .addComponent(lblClaveBaseSeg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGap(1, 1, 1)))
                            .addGroup(pnlformaengineLayout.createSequentialGroup()
                                .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblUsuarioBaseSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblServidorSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)))
                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtServidorEsquemaSeg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtUsuarioSeg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtPuertoEsquemaSeg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtClaveUsuarioSeg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlformaengineLayout.createSequentialGroup()
                        .addGap(70, 70, 70)
                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlformaengineLayout.createSequentialGroup()
                                .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblBaseSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblrutaengine1, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblrutaengine, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblEsquemaSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(blbclavengine, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(pnlformaengineLayout.createSequentialGroup()
                                        .addComponent(lblRutaCertSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(59, 59, 59)
                                        .addComponent(txtrutaseg, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(lblhostiname, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(2, 2, 2)
                                .addComponent(btnrutaseg, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlformaengineLayout.createSequentialGroup()
                                .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txtUsuarioLllave, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(pnlformaengineLayout.createSequentialGroup()
                                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(lblhostinameSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                .addComponent(lblClaveCertSeg, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                                                .addComponent(lblClaveEsquemaSeg, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlformaengineLayout.createSequentialGroup()
                                                .addGap(18, 18, 18)
                                                .addComponent(txtClaveCertSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addGroup(pnlformaengineLayout.createSequentialGroup()
                                                    .addGap(18, 18, 18)
                                                    .addComponent(txtNomHostCertSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlformaengineLayout.createSequentialGroup()
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                    .addComponent(txtClaveEsquemaSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                        .addGap(1, 1, 1))
                                    .addComponent(txtHostNameCertEngine, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtrutaengine, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtclavenegine, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtUsuarioEngine, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtClaveCertEngine, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(2, 2, 2)
                                .addComponent(btnrutaengine, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(pnlformaengineLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(btnposmobil, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        pnlformaengineLayout.setVerticalGroup(
            pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlformaengineLayout.createSequentialGroup()
                .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtUsuarioEngine, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblEsquemaSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbltipoconexionengine, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbotipoconengine, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(1, 1, 1)
                .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblServidorEsquemaSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtServidorSwt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(blbclavengine, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtclavenegine, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(1, 1, 1)
                .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblUsuarioSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtUsuarioSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblrutaengine)
                    .addComponent(txtrutaengine, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnrutaengine, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(1, 1, 1)
                .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblClaveUsuarioSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtClaveSwt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblrutaengine1)
                    .addComponent(txtClaveCertEngine, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(1, 1, 1)
                .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlformaengineLayout.createSequentialGroup()
                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblPuertoSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtPuertoSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblhostiname))
                        .addGap(1, 1, 1)
                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblServicioSwt, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtBaseEngine, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblTipoConSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbotipoconseg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtUsuarioLllave, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblBaseSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(1, 1, 1)
                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblServidorSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtServidorEsquemaSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblClaveEsquemaSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtClaveEsquemaSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(1, 1, 1)
                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblUsuarioBaseSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtUsuarioSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblRutaCertSeg)
                            .addComponent(txtrutaseg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnrutaseg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(1, 1, 1)
                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblClaveBaseSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtClaveUsuarioSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblClaveCertSeg)
                            .addComponent(txtClaveCertSeg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(1, 1, 1)
                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblPuertoEsquemaSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtPuertoEsquemaSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblhostinameSeg)
                            .addComponent(txtNomHostCertSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(1, 1, 1)
                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblServicioSwt1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtBaseSeg, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(6, 6, 6)
                        .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblproducto)
                            .addGroup(pnlformaengineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtestadoposmobil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnposmobil))))
                    .addComponent(txtHostNameCertEngine, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnaceptar.setText("Aceptar");
        btnaceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaceptarActionPerformed(evt);
            }
        });
        pnlcomando.add(btnaceptar);

        javax.swing.GroupLayout pnlcontenedorLayout = new javax.swing.GroupLayout(pnlcontenedor);
        pnlcontenedor.setLayout(pnlcontenedorLayout);
        pnlcontenedorLayout.setHorizontalGroup(
            pnlcontenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlcontenedorLayout.createSequentialGroup()
                .addGroup(pnlcontenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlcontenedorLayout.createSequentialGroup()
                        .addGap(331, 331, 331)
                        .addComponent(pnlcomando, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(pnlformaengine, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4))
        );
        pnlcontenedorLayout.setVerticalGroup(
            pnlcontenedorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlcontenedorLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(pnlformaengine, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlcomando, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(pnlcontenedor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(pnlcontenedor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cbotipoconengineItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbotipoconengineItemStateChanged
            
        if (evt.getStateChange() == ItemEvent.SELECTED) {

            if(cbotipoconengine.getSelectedIndex()==0){
                txtrutaengine.setText("");
                txtClaveCertEngine.setText("");
                txtHostNameCertEngine.setText("");
                btnrutaengine.setEnabled(true);
                txtClaveCertEngine.setEnabled(true);
                txtHostNameCertEngine.setEnabled(true);
            
                
            }
            else if(cbotipoconengine.getSelectedIndex()==1){
                txtrutaengine.setText("");
                txtHostNameCertEngine.setText("");
                txtClaveCertEngine.setText("");
                btnrutaengine.setEnabled(false);
                txtClaveCertEngine.setEnabled(false);
                txtHostNameCertEngine.setEnabled(false);
              
            }
        }
             
    }//GEN-LAST:event_cbotipoconengineItemStateChanged

    private void txtServidorSwtkeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtServidorSwtkeyTyped

        if (evt.getSource()==txtServidorSwt){
            if(txtServidorSwt.getText().length()==80)
            evt.consume();
        }

        
        if(evt.getSource()==txtPuertoSwt){
            if(txtPuertoSwt.getText().length()==6)
            evt.consume();
            else if(txtPuertoSwt.getText().length()>6){
                txtPuertoSwt.setText("");
                evt.consume();
            }
        }

        if(evt.getSource()==txtPuertoEsquemaSeg){
            if(txtPuertoEsquemaSeg.getText().length()==6)
            evt.consume();
            else if(txtPuertoEsquemaSeg.getText().length()>6){
                txtPuertoEsquemaSeg.setText("");
                evt.consume();
            }
        }
  
    }//GEN-LAST:event_txtServidorSwtkeyTyped

    private void txtPuertoSwtkeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPuertoSwtkeyTyped

        if (evt.getSource()==txtServidorSwt){
            if(txtServidorSwt.getText().length()==80)
            evt.consume();
        }

        if(evt.getSource()==txtPuertoSwt){
            if(txtPuertoSwt.getText().length()==6)
            evt.consume();
            else if(txtPuertoSwt.getText().length()>6){
                txtPuertoSwt.setText("");
                evt.consume();
            }
        }

        /*
        if(evt.getSource()==txtPuertoEsquemaSeg){
            if(txtPuertoEsquemaSeg.getText().length()==6)
            evt.consume();
            else if(txtPuertoEsquemaSeg.getText().length()>6){
                txtPuertoEsquemaSeg.setText("");
                evt.consume();
            }
        }
        
        */
        
    }//GEN-LAST:event_txtPuertoSwtkeyTyped

    private void btnrutaengineActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnrutaengineActionPerformed
        
        txtrutaengine.setText("");

        JFileChooser jfiledialog=new JFileChooser();
        jfiledialog.setFileSelectionMode(JFileChooser.FILES_ONLY);
        
        FileNameExtensionFilter filtro = new FileNameExtensionFilter("Certificado de Motor Base de Datos", "jks");
        jfiledialog.setFileFilter(filtro);
        
        jfiledialog.setAcceptAllFileFilterUsed(false);

        jfiledialog.setDialogTitle("Selecciòn de Certificado de Motor de Base de Datos Engine");
        int seleccion=jfiledialog.showOpenDialog(this);

        if(seleccion==JFileChooser.APPROVE_OPTION){
            File archivoScript=jfiledialog.getSelectedFile();
            txtrutaengine.setText(archivoScript.getAbsolutePath());
        }

    }//GEN-LAST:event_btnrutaengineActionPerformed

    private void cbotipoconsegItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbotipoconsegItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED) {

            if(cbotipoconseg.getSelectedIndex()==0){
                txtrutaseg.setText("");
                txtClaveCertSeg.setText("");
                txtNomHostCertSeg.setText("");
                btnrutaseg.setEnabled(true);
                txtClaveCertSeg.setEnabled(true);
                txtNomHostCertSeg.setEnabled(true);
              
            }
            else if(cbotipoconseg.getSelectedIndex()==1){
                txtrutaseg.setText("");
                txtClaveCertSeg.setText("");
                txtNomHostCertSeg.setText("");
                btnrutaseg.setEnabled(false);
                txtClaveCertSeg.setEnabled(false);
                txtNomHostCertSeg.setEnabled(false);
            }
        }
    }//GEN-LAST:event_cbotipoconsegItemStateChanged

    private void txtPuertoEsquemaSegkeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPuertoEsquemaSegkeyTyped

        if (evt.getSource()==txtServidorSwt){
            if(txtServidorSwt.getText().length()==80)
            evt.consume();
        }

        if(evt.getSource()==txtPuertoSwt){
            if(txtPuertoSwt.getText().length()==6)
            evt.consume();
            else if(txtPuertoSwt.getText().length()>6){
                txtPuertoSwt.setText("");
                evt.consume();
            }
        }

        if(evt.getSource()==txtPuertoEsquemaSeg){
            if(txtPuertoEsquemaSeg.getText().length()==6)
            evt.consume();
            else if(txtPuertoEsquemaSeg.getText().length()>6){
                txtPuertoEsquemaSeg.setText("");
                evt.consume();
            }
        }
    }//GEN-LAST:event_txtPuertoEsquemaSegkeyTyped

    private void llamarForma(){
        JDialog odialogo= new JDialog(this,true);
        
        FrmPosMobil ofrmposmobil=null;
        FrmWallet ofrmWallet=null;
        

        if (licencia.getModulo().equalsIgnoreCase("POSMOVIL")  )
            ofrmposmobil= new FrmPosMobil(odialogo,EsquemaCliente,TableSpaceEsquemaCliente,
            ClaveEsquemaCliente,nombreRolCliente,licencia.getModulo());
        
        else if(licencia.getModulo().equalsIgnoreCase("WALLET") ){
            ofrmWallet= new FrmWallet(odialogo,EsquemaCliente,TableSpaceEsquemaCliente,ClaveEsquemaCliente,
            EsquemaDatos,this.TableSpaceDatos,ClaveEsquemaDatos, nombreRolCliente, nombreRolDatos,licencia.getMotorBase() );
        }
       
     
        if(ofrmposmobil!=null ){
            odialogo.setTitle(ofrmposmobil.getTitle());
            odialogo.add(ofrmposmobil.getContentPane());
            odialogo.setSize(ofrmposmobil.getSize());
        }
        
        if (ofrmWallet!=null && ofrmposmobil==null){
              odialogo.setTitle(ofrmWallet.getTitle());
              odialogo.add(ofrmWallet.getContentPane());
              odialogo.setSize(ofrmWallet.getSize());
        }
        
        if(ofrmposmobil!=null || ofrmWallet!=null){
            odialogo.setResizable(false);
            odialogo.pack();
            odialogo.setLocationRelativeTo(this);
            odialogo.setVisible(true);
        }
        
        
        if(ofrmposmobil!=null){
            EsquemaCliente=ofrmposmobil.getEsquemaCliente();
            TableSpaceEsquemaCliente=ofrmposmobil.getTableSpaceCliente();
            ClaveEsquemaCliente=ofrmposmobil.getClaveEsquemaCliente();
            //nombreRolCliente=ofrmposmobil.getRolCliente();
        }
        
        if(ofrmWallet!=null){
            EsquemaCliente=ofrmWallet.getEsquemaCliente();
            TableSpaceEsquemaCliente=ofrmWallet.getTableSpaceCliente();
            ClaveEsquemaCliente=ofrmWallet.getClaveEsquemaCliente();
            
            EsquemaDatos=ofrmWallet.getEsquemaDatos();
            ClaveEsquemaDatos=ofrmWallet.getClaveEsquemaDatos();
            TableSpaceDatos=ofrmWallet.getTableSpaceDatos();
            //nombreRolCliente=ofrmWallet.getRolCliente();
            //nombreRolDatos=ofrmWallet.getRolDatos();
        }
        
        if (ofrmposmobil!=null && ofrmWallet==null){
            if(EsquemaCliente!=null  && TableSpaceEsquemaCliente!=null && ClaveEsquemaCliente!=null ){
                txtestadoposmobil.setText("CONFIGURADO");
            }
            else
                txtestadoposmobil.setText("NO CONFIGURADO");
        }
        
        if (ofrmposmobil==null && ofrmWallet!=null){
            if(EsquemaCliente!=null  && TableSpaceEsquemaCliente!=null && 
               ClaveEsquemaCliente!=null && TableSpaceDatos!=null && 
               EsquemaDatos!=null && ClaveEsquemaDatos!=null){
                 txtestadoposmobil.setText("CONFIGURADO");
            }
            else
                txtestadoposmobil.setText("NO CONFIGURADO"); 
        }      
    }
    
    private void btnrutasegActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnrutasegActionPerformed
        txtrutaseg.setText("");
        
        JFileChooser jfiledialog=new JFileChooser();
        jfiledialog.setFileSelectionMode(JFileChooser.FILES_ONLY);
        
        FileNameExtensionFilter filtro = new FileNameExtensionFilter("Certificado de Motor Base de Datos", "jks");
        jfiledialog.setFileFilter(filtro);
        
        jfiledialog.setAcceptAllFileFilterUsed(false);
        
        jfiledialog.setDialogTitle("Selecciòn de Certificado de Motor de Base de Seguridad");
        int seleccion=jfiledialog.showOpenDialog(this);

        if(seleccion==JFileChooser.APPROVE_OPTION){
            File archivoScript=jfiledialog.getSelectedFile();
            txtrutaseg.setText(archivoScript.getAbsolutePath());
        }
        
        
    }//GEN-LAST:event_btnrutasegActionPerformed

    private void btnposmobilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnposmobilActionPerformed
         llamarForma();
    }//GEN-LAST:event_btnposmobilActionPerformed

    private void btnaceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaceptarActionPerformed
        procesar();
    }//GEN-LAST:event_btnaceptarActionPerformed

    private boolean validarentrada(){
        boolean flagresultado=true;
        
        if(txtServidorSwt.getText()==null || txtServidorSwt.getText().trim().length()==0){
            flagresultado=false;
            JOptionPane.showMessageDialog(this, "Ingrese la información del Servidor de Base de Datos del Engine", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
        }
        
        if(flagresultado==true)
            if ( txtPuertoSwt.getText().trim().length()==0){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese la información del puerto de  Base de Datos del Engine", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        
         
        if(flagresultado==true)
            if ( txtUsuarioSwt.getText().trim().length()==0){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese el usuario de Motor de Base de Datos del Engine", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
         
        
        if(flagresultado==true)
            if ( txtUsuarioEngine.getText().trim().length()==0){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese el nombre usuario de Base de Datos Engine", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        
        if(flagresultado==true)
            if ( txtClaveSwt.getPassword()==null ||  txtClaveSwt.getPassword().length==0) {
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese la contraseña de usuario Administrador de Base de Datos Engine", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        
        if(flagresultado==true)
            if (txtclavenegine.getPassword().length==0){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese la contraseña de usuario de Base de Datos del Engine", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }

        if(flagresultado==true)
            if (txtBaseEngine.getText()==null || txtBaseEngine.getText().trim().length()==0){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese nombre de la Base de Datos del Engine", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        
        if(flagresultado==true)
            if (txtServidorEsquemaSeg.getText()==null || txtServidorEsquemaSeg.getText().trim().length()==0){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese la información del servidor de la Base de Datos de Seguridad", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        
        if(flagresultado==true)
            if (txtPuertoEsquemaSeg.getText()==null || txtPuertoEsquemaSeg.getText().trim().length()==0){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese el puerto de la Base de Datos de Seguridad", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        
  
        if(flagresultado==true)
            if (txtUsuarioSeg.getText()==null || txtUsuarioSeg.getText().trim().length()==0){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese el nombre usuario de Base de Datos de Seguridad", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        
        if(flagresultado==true)
            if(txtClaveUsuarioSeg.getPassword()==null || txtClaveUsuarioSeg.getPassword().length==0){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese la contraseña del usuario Administradoe de Base de Datos de Seguridad", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
                
        if(flagresultado==true)
            if (txtClaveEsquemaSeg.getPassword()==null || txtClaveEsquemaSeg.getPassword().length==0){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese la contraseña de usuario de Base de Datos de Seguridad", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        
        if(flagresultado==true)
            if (txtUsuarioLllave.getText()==null || txtUsuarioLllave.getText().trim().length()==0){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese el nombre de Usuario de la Base de Datos de Seguridad", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        
        if(flagresultado==true)
            if (txtClaveEsquemaSeg.getPassword()==null || txtClaveEsquemaSeg.getPassword().length==0 ){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese la clave de Usuario de la Base de Datos de Seguridad", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        
        if(flagresultado==true)
            if (txtBaseSeg.getText()==null || txtBaseSeg.getText().trim().length()==0){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese el nombre de la Base de Datos de Seguridad", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        

        if(flagresultado==true){
            if(cbotipoconengine.getSelectedIndex()==0 && txtrutaengine.getText().trim().equals("")==true){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese la ruta wallet de la Base de Datos Engine", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        }
        
        if(flagresultado==true){
            if(cbotipoconengine.getSelectedIndex()==0 && txtClaveCertEngine.getPassword().length==0){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese la clave del certificado de Base de Datos Engine", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        }
        
        if(flagresultado==true){
            if(cbotipoconengine.getSelectedIndex()==0 && txtHostNameCertEngine.getText().trim().equals("")==true){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese el nombre de host del certificado Base de Datos Engine", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        }
        
        if(flagresultado==true){
            
            if(cbotipoconseg.getSelectedIndex()==0 && txtrutaseg.getText().trim().equals("")==true){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese la ruta wallet de la Base de Datos Seguridad", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        }
        
        if(flagresultado==true){
            if(cbotipoconseg.getSelectedIndex()==0 && txtClaveCertSeg.getPassword().length==0){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese la clave del certificado de Base de Datos de Seguridad", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        }
        
        
        if(flagresultado==true){
            if(cbotipoconseg.getSelectedIndex()==0 && txtNomHostCertSeg.getText().trim().equals("")==true){
                flagresultado=false;
                JOptionPane.showMessageDialog(this, "Ingrese el host del certificado de Base de Datos de Seguridad", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
            
        }
        
        if(flagresultado==true){
            
            if(licencia.getModulo().equalsIgnoreCase("POSMOVIL")==true || licencia.getModulo().equalsIgnoreCase("WALLET")==true){
                if( ClaveEsquemaCliente==null  ||EsquemaCliente==null || TableSpaceEsquemaCliente==null){
                        flagresultado=false;
                        JOptionPane.showMessageDialog(this, "Ingrese la información de configuración de la Base de Datos Cliente", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
        
        if(flagresultado==true){
            if(licencia.getModulo().equalsIgnoreCase("WALLET")==true){
                if(EsquemaDatos==null || TableSpaceDatos==null ||ClaveEsquemaDatos==null  ){
                    flagresultado=false;
                        JOptionPane.showMessageDialog(this, "Ingrese la información de configuración de la Base de Datos Vault", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
        
        return flagresultado;
    }
    
    
     private boolean llenaarDatosBase(){
        boolean flagresultado=true;
        
        conexionBaseAdminSwt=null;
        conexionBaseEsquemaSwt=null;
        conexionBaseAdminLlaves=null;
        conexionBaseEsquemaLlaves=null;
        
        conexionBaseAdminClientes=null;
        conexionBaseEsquemaClientes=null;
        
        conexionBaseAdminDatos=null;
        conexionBaseEsquemaDatos=null;
        
        flagresultado= validarentrada();
        
        
        if (flagresultado){
        
            licencia.getListaFiles().forEach((final ArchivoComandos ob) -> {
                parametrosConexion conexionBase;
                
                if (ob.getEsquema().equals("ESQUEMABASESWITCH")==true){

                    conexionBase= new parametrosConexion();
                    conexionBase.setMotorBaseDatos(licencia.getMotorBase());
                    
                    conexionBase.setNombreBaseDatos(txtBaseEngine.getText());
                    
                    conexionBase.setEsquema(txtUsuarioEngine.getText());

                    conexionBase.setServidorBaseDatos(txtServidorSwt.getText());
                    conexionBase.setClaveBaseDatos( new String( txtclavenegine.getPassword()));
                    conexionBase.setPuertoBase(Integer.parseInt(txtPuertoSwt.getText()));
                 
                    conexionBase.setTipoConexion(cbotipoconengine.getSelectedIndex());
                    
                    if(cbotipoconengine.getSelectedIndex()==0){
                        conexionBase.setRutaWallet(txtrutaengine.getText());
                        conexionBase.setHostNameCertificado(txtHostNameCertEngine.getText());
                        conexionBase.setClaveCertificado(new String(txtClaveCertEngine.getPassword()));    
                    }
                    
                  
                    ob.setParametrosConexion(conexionBase);

                    if (conexionBaseEsquemaSwt==null)
                        conexionBaseEsquemaSwt=conexionBase;

                    ob.setContenidoArchivo(recuperarContenidoArchivo(ob.getfilename(),
                    ob.getEsquema(),conexionBase.getEsquema(),conexionBase.getNombreTableSpace())); 
                }
                
               
                
                if (ob.getEsquema().equals("ESQUEMABASELLAVES")==true) {
                    conexionBase= new parametrosConexion();
                    conexionBase.setMotorBaseDatos(licencia.getMotorBase());
                    
                    conexionBase.setEsquema(txtUsuarioLllave.getText());
                    
                    conexionBase.setServidorBaseDatos(txtServidorEsquemaSeg.getText());
                    conexionBase.setClaveBaseDatos(new String( txtClaveEsquemaSeg.getPassword()));
                    conexionBase.setPuertoBase(Integer.parseInt(txtPuertoEsquemaSeg.getText()));
                    
                    conexionBase.setNombreBaseDatos(txtBaseSeg.getText());
                    
                    conexionBase.setTipoConexion(cbotipoconseg.getSelectedIndex());
                    
                    if(cbotipoconseg.getSelectedIndex()==0){
                        conexionBase.setRutaWallet(txtrutaseg.getText());
                        conexionBase.setHostNameCertificado(txtNomHostCertSeg.getText());
                        conexionBase.setClaveCertificado(new String(txtClaveCertSeg.getPassword()));    
                    }
                  
                    ob.setParametrosConexion(conexionBase);

                    if (conexionBaseEsquemaLlaves==null)
                        conexionBaseEsquemaLlaves=conexionBase;

                    ob.setContenidoArchivo(recuperarContenidoArchivo(ob.getfilename(),ob.getEsquema(),
                    conexionBase.getEsquema(),conexionBase.getNombreTableSpace())); 
                }
                
                if(ob.getEsquema().equals("ESQUEMABASECUENTA")==true){
                    conexionBase= new parametrosConexion();
                    
                    conexionBase.setMotorBaseDatos(licencia.getMotorBase());
                    conexionBase.setEsquema(EsquemaCliente);
                    
                    conexionBase.setClaveBaseDatos( this.ClaveEsquemaCliente);
                    conexionBase.setServidorBaseDatos(txtServidorSwt.getText());
                    conexionBase.setPuertoBase(Integer.parseInt(txtPuertoSwt.getText()));

                    conexionBase.setNombreBaseDatos(this.TableSpaceEsquemaCliente);
                 
                    
                    conexionBase.setTipoConexion(cbotipoconengine.getSelectedIndex());
                     
                    if(cbotipoconengine.getSelectedIndex()==0){
                        conexionBase.setRutaWallet(txtrutaengine.getText());
                        conexionBase.setHostNameCertificado(txtHostNameCertEngine.getText());
                        conexionBase.setClaveCertificado(new String(txtClaveCertEngine.getPassword()));
                    }
                    
                    conexionBase.setNombreRol(this.nombreRolCliente);
                    
                    ob.setParametrosConexion(conexionBase);
                    
                     if (conexionBaseEsquemaClientes==null)
                        conexionBaseEsquemaClientes=conexionBase;
                    
                    ob.setContenidoArchivo(recuperarContenidoArchivo(ob.getfilename(),ob.getEsquema(),
                    ClaveEsquemaCliente,TableSpaceEsquemaCliente));    
                }
                
                if(ob.getEsquema().equalsIgnoreCase("ESQUEMABASEDATOS")==true){
                    conexionBase= new parametrosConexion();
                    conexionBase.setMotorBaseDatos(licencia.getMotorBase());
                    conexionBase.setEsquema(this.EsquemaDatos);
                    conexionBase.setClaveBaseDatos( this.ClaveEsquemaDatos);
                    conexionBase.setServidorBaseDatos(txtServidorSwt.getText());
                    conexionBase.setPuertoBase(Integer.parseInt(txtPuertoSwt.getText()));
                    
                    conexionBase.setNombreBaseDatos(this.TableSpaceDatos);
                    
                    //conexionBase.setServiceName(txtServicioSwt.getText());
                    conexionBase.setTipoConexion(cbotipoconengine.getSelectedIndex());
                    
                    if(cbotipoconengine.getSelectedIndex()==0){
                        conexionBase.setRutaWallet(txtrutaengine.getText());
                        conexionBase.setHostNameCertificado(txtHostNameCertEngine.getText());
                        conexionBase.setClaveCertificado(new String(txtClaveCertEngine.getPassword()));
                    }
                    
                    conexionBase.setNombreRol(nombreRolDatos);
                    
                    ob.setParametrosConexion(conexionBase);
                    
                    if(conexionBaseEsquemaDatos==null)
                        conexionBaseEsquemaDatos=conexionBase;
                    
                    ob.setContenidoArchivo(recuperarContenidoArchivo(ob.getfilename(),ob.getEsquema(),
                    this.ClaveEsquemaDatos,TableSpaceDatos));      
                }
                
            }); //Fin de for(ArchivoComandos ob : listafiles)
            
            conexionBaseAdminSwt= new parametrosConexion();
            conexionBaseAdminSwt.setMotorBaseDatos(licencia.getMotorBase());
            conexionBaseAdminSwt.setEsquema(txtUsuarioSwt.getText());
            conexionBaseAdminSwt.setServidorBaseDatos(txtServidorSwt.getText());
            conexionBaseAdminSwt.setClaveBaseDatos( new String( this.txtClaveSwt.getPassword()));
            conexionBaseAdminSwt.setPuertoBase(Integer.parseInt(txtPuertoSwt.getText()));

            conexionBaseAdminSwt.setNombreBaseDatos("master");    
            conexionBaseAdminSwt.setTipoConexion(cbotipoconengine.getSelectedIndex());
                    
            if(cbotipoconengine.getSelectedIndex()==0){
                conexionBaseAdminSwt.setRutaWallet(txtrutaengine.getText());
                conexionBaseAdminSwt.setHostNameCertificado(txtHostNameCertEngine.getText());
                conexionBaseAdminSwt.setClaveCertificado(new String(txtClaveCertEngine.getPassword()));     
            }
            

            conexionBaseAdminLlaves= new parametrosConexion();
            conexionBaseAdminLlaves.setMotorBaseDatos(licencia.getMotorBase());
            conexionBaseAdminLlaves.setEsquema(txtUsuarioSeg.getText());
            conexionBaseAdminLlaves.setServidorBaseDatos(txtServidorEsquemaSeg.getText());
            conexionBaseAdminLlaves.setClaveBaseDatos(new String( txtClaveUsuarioSeg.getPassword()));
            conexionBaseAdminLlaves.setPuertoBase(Integer.parseInt(txtPuertoEsquemaSeg.getText()));
            conexionBaseAdminLlaves.setNombreBaseDatos("master");
                 
            conexionBaseAdminLlaves.setTipoConexion(cbotipoconseg.getSelectedIndex());
                    
            if(cbotipoconseg.getSelectedIndex()==0){
                conexionBaseAdminLlaves.setRutaWallet(txtrutaseg.getText());
                conexionBaseAdminLlaves.setHostNameCertificado(txtNomHostCertSeg.getText());
                conexionBaseAdminLlaves.setClaveCertificado(new String(txtClaveCertSeg.getPassword()));    
            }
            
            if(licencia.getModulo().equalsIgnoreCase("WALLET")){
                conexionBaseAdminDatos= new parametrosConexion();
                conexionBaseAdminDatos.setMotorBaseDatos(licencia.getMotorBase());
                conexionBaseAdminDatos.setEsquema(txtUsuarioSwt.getText());
                conexionBaseAdminDatos.setServidorBaseDatos(txtServidorSwt.getText());
                conexionBaseAdminDatos.setClaveBaseDatos( new String( this.txtClaveSwt.getPassword()));
                conexionBaseAdminDatos.setPuertoBase(Integer.parseInt(txtPuertoSwt.getText()));

                conexionBaseAdminDatos.setNombreBaseDatos("master");    
                conexionBaseAdminDatos.setTipoConexion(cbotipoconengine.getSelectedIndex());

                 if(cbotipoconengine.getSelectedIndex()==0){
                    conexionBaseAdminDatos.setRutaWallet(txtrutaengine.getText());
                    conexionBaseAdminDatos.setHostNameCertificado(txtHostNameCertEngine.getText());
                    conexionBaseAdminDatos.setClaveCertificado(new String(txtClaveCertEngine.getPassword()));     
                }
            }
            
            if(licencia.getModulo().equalsIgnoreCase("POSMOVIL") || licencia.getModulo().equalsIgnoreCase("WALLET")){
                conexionBaseAdminClientes= new parametrosConexion();
                conexionBaseAdminClientes.setMotorBaseDatos(licencia.getMotorBase());
                conexionBaseAdminClientes.setEsquema(txtUsuarioSwt.getText());
                conexionBaseAdminClientes.setServidorBaseDatos(txtServidorSwt.getText());
                conexionBaseAdminClientes.setClaveBaseDatos( new String( this.txtClaveSwt.getPassword()));
                conexionBaseAdminClientes.setPuertoBase(Integer.parseInt(txtPuertoSwt.getText()));

                conexionBaseAdminClientes.setNombreBaseDatos("master");    
                conexionBaseAdminClientes.setTipoConexion(cbotipoconengine.getSelectedIndex());

                if(cbotipoconengine.getSelectedIndex()==0){
                    conexionBaseAdminClientes.setRutaWallet(txtrutaengine.getText());
                    conexionBaseAdminClientes.setHostNameCertificado(txtHostNameCertEngine.getText());
                    conexionBaseAdminClientes.setClaveCertificado(new String(txtClaveCertEngine.getPassword()));     
                }
            }
            
        }
        
        return flagresultado; 
     }
     
     
     private String recuperarContenidoArchivo(String vnombreArchivo, String Esquema, String EsquemaBase, 
     String nombreTableSpace){
        String contenidoArchivo="";
        File archivo;
       
        archivo= new File(System.getProperties().getProperty("user.dir") + File.separator + 
        "File" + File.separator + vnombreArchivo);
       
        try{
            StringBuffer buffer;
            String contenidoTemporal;
            RandomAccessFile fichero ;
           
            if (archivo.exists()){
                 fichero= new RandomAccessFile(archivo,"r");
                 buffer=new StringBuffer();
                 
                 while((contenidoTemporal=fichero.readLine())!=null){
                     
                    buffer.append(contenidoTemporal );
                 }
                 
                
                fichero.close();
                
                contenidoTemporal=buffer.toString();  
                
                contenidoArchivo= Encripcion.desencriptar(contenidoTemporal);
                
                
                if (Esquema.equals("ADMINBASESWITCH")==true || Esquema.equals("ESQUEMABASESWITCH")==true){
                    ;//contenidoArchivo=contenidoArchivo.replace("set @baseDatos='DBSWITCH'", "set @baseDatos='" + EsquemaBase + "'" );
                
                    contenidoArchivo=contenidoArchivo.replace("set @baseDatosCliente='DBCUENTA'", "set @baseDatosCliente='" + this.TableSpaceEsquemaCliente + "';");
                }
                
                if (Esquema.equals("ESQUEMABASELLAVES")==true || Esquema.equals("ADMINBASELLAVES")==true){
                    contenidoArchivo=contenidoArchivo.replace("v_esquema_swt varchar2(40):= 'ADMINLLAVES_ADQ';", "v_esquema_swt varchar2(40) := '" + EsquemaBase + "';" ); 
                    contenidoArchivo=contenidoArchivo.replace("v_table_space varchar2(40):= 'SWITCH_LLAVES_ADQ';", "v_table_space varchar2(40) := '" + nombreTableSpace + "';" ); 
                    
                    contenidoArchivo=contenidoArchivo.replace("v_esquema_swt varchar2(40):='ADMINLLAVES_ADQ';", "v_esquema_swt varchar2(40) :='" + EsquemaBase + "';" ); 
                    contenidoArchivo=contenidoArchivo.replace("v_table_space varchar2(40):='SWITCH_LLAVES_ADQ';", "v_table_space varchar2(40):='" + nombreTableSpace + "';" ); 
                    
                    contenidoArchivo=contenidoArchivo.replace("v_esquema_swt varchar2(40):= 'ADMINLLAVES';", "v_esquema_swt varchar2(40) := '" + EsquemaBase + "';" ); 
                    contenidoArchivo=contenidoArchivo.replace("v_table_space varchar2(40):= 'SWITCH_LLAVES';", "v_table_space varchar2(40) := '" + nombreTableSpace + "';" ); 
                    
                    contenidoArchivo=contenidoArchivo.replace("v_esquema_swt varchar2(40):='ADMINLLAVES';", "v_esquema_swt varchar2(40):='" + EsquemaBase + "';" ); 
                    contenidoArchivo=contenidoArchivo.replace("v_table_space varchar2(40):='SWITCH_LLAVES';","v_table_space varchar2(40):='" + nombreTableSpace + "';" ); 
                    

                    contenidoArchivo=contenidoArchivo.replace("v_clave_defecto varchar2(100):='hiper';", "v_clave_defecto varchar2(100):='" + new String( txtClaveEsquemaSeg.getPassword()) + "';" );
                    //contenidoArchivo= contenidoArchivo.replace("v_nombre_rol varchar(50):='CONTROL_LLAVES';", "v_nombre_rol varchar(50):='" + txtrolseg.getText() + "';");
                }
                 
                if (Esquema.equals("ADMINBASECUENTA")==true || Esquema.equals("ESQUEMABASECUENTA")==true){
                    contenidoArchivo=contenidoArchivo.replace("set @baseDatos='DBSWITCH'", "set @baseDatos='" + txtBaseEngine.getText() + "'" );
                }
                  
                if(Esquema.equalsIgnoreCase("ADMINBASEDATOS")==true || Esquema.equalsIgnoreCase("ESQUEMABASEDATOS")==true){
                    contenidoArchivo=contenidoArchivo.replace("set @baseDatos='DBENGINE'", "set @baseDatos='" + txtBaseEngine.getText() + "';");
                    contenidoArchivo=contenidoArchivo.replace("set @baseDatosCliente='DBCUENTA'", "set @baseDatosCliente='" + this.TableSpaceEsquemaCliente + "';");
                }
                  
            } //Fin de if (archivo.exists())
        }
        
        catch(IOException e){
            ;
        }
       
        return contenidoArchivo;
    }
   

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel blbclavengine;
    private javax.swing.JButton btnaceptar;
    private javax.swing.JButton btnposmobil;
    private javax.swing.JButton btnrutaengine;
    private javax.swing.JButton btnrutaseg;
    private javax.swing.JComboBox<String> cbotipoconengine;
    private javax.swing.JComboBox<String> cbotipoconseg;
    private javax.swing.JLabel lblBaseSeg;
    private javax.swing.JLabel lblClaveBaseSeg;
    private javax.swing.JLabel lblClaveCertSeg;
    private javax.swing.JLabel lblClaveEsquemaSeg;
    private javax.swing.JLabel lblClaveUsuarioSwt;
    private javax.swing.JLabel lblEsquemaSwt;
    private javax.swing.JLabel lblPuertoEsquemaSeg;
    private javax.swing.JLabel lblPuertoSwt;
    private javax.swing.JLabel lblRutaCertSeg;
    private javax.swing.JLabel lblServicioSwt;
    private javax.swing.JLabel lblServicioSwt1;
    private javax.swing.JLabel lblServidorEsquemaSwt;
    private javax.swing.JLabel lblServidorSeg;
    private javax.swing.JLabel lblTipoConSeg;
    private javax.swing.JLabel lblUsuarioBaseSeg;
    private javax.swing.JLabel lblUsuarioSwt;
    private javax.swing.JLabel lblhostiname;
    private javax.swing.JLabel lblhostinameSeg;
    private javax.swing.JLabel lblproducto;
    private javax.swing.JLabel lblrutaengine;
    private javax.swing.JLabel lblrutaengine1;
    private javax.swing.JLabel lbltipoconexionengine;
    private javax.swing.JPanel pnlcomando;
    private javax.swing.JPanel pnlcontenedor;
    private javax.swing.JPanel pnlformaengine;
    private javax.swing.JTextField txtBaseEngine;
    private javax.swing.JTextField txtBaseSeg;
    private javax.swing.JPasswordField txtClaveCertEngine;
    private javax.swing.JPasswordField txtClaveCertSeg;
    private javax.swing.JPasswordField txtClaveEsquemaSeg;
    private javax.swing.JPasswordField txtClaveSwt;
    private javax.swing.JPasswordField txtClaveUsuarioSeg;
    private javax.swing.JTextField txtHostNameCertEngine;
    private javax.swing.JTextField txtNomHostCertSeg;
    private javax.swing.JTextField txtPuertoEsquemaSeg;
    private javax.swing.JTextField txtPuertoSwt;
    private javax.swing.JTextField txtServidorEsquemaSeg;
    private javax.swing.JTextField txtServidorSwt;
    private javax.swing.JTextField txtUsuarioEngine;
    private javax.swing.JTextField txtUsuarioLllave;
    private javax.swing.JTextField txtUsuarioSeg;
    private javax.swing.JTextField txtUsuarioSwt;
    private javax.swing.JPasswordField txtclavenegine;
    private javax.swing.JTextField txtestadoposmobil;
    private javax.swing.JTextField txtrutaengine;
    private javax.swing.JTextField txtrutaseg;
    // End of variables declaration//GEN-END:variables
}
