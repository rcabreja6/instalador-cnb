
package soft.gui.sqlserver;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;


public class FrmRutaBase extends javax.swing.JFrame {
    final JDialog dialogo;
    
    public String getRuta(){
        String valor=null;
        
        if (txtRutaBase.getText().trim().length()>0)
            valor = txtRutaBase.getText();
        
        return valor;
    }
   
    public FrmRutaBase(JDialog odialogo) {
        initComponents();
          dialogo=odialogo;
          dialogo.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
          
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlcontendor = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtRutaBase = new javax.swing.JTextField();
        btnaceptar = new javax.swing.JButton();
        btncerrar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Ruta de Servidor  de archivo de Base de Datos");

        pnlcontendor.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEtchedBorder(java.awt.Color.white, null), javax.swing.BorderFactory.createEmptyBorder(3, 5, 5, 5)));
        pnlcontendor.setPreferredSize(new java.awt.Dimension(295, 130));

        jLabel1.setText("Directorio de instalacion:");

        txtRutaBase.setPreferredSize(new java.awt.Dimension(6, 23));

        javax.swing.GroupLayout pnlcontendorLayout = new javax.swing.GroupLayout(pnlcontendor);
        pnlcontendor.setLayout(pnlcontendorLayout);
        pnlcontendorLayout.setHorizontalGroup(
            pnlcontendorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlcontendorLayout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtRutaBase, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        pnlcontendorLayout.setVerticalGroup(
            pnlcontendorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlcontendorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(txtRutaBase, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        btnaceptar.setText("Aceptar");
        btnaceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaceptarActionPerformed(evt);
            }
        });

        btncerrar.setText("Cerrar");
        btncerrar.setMaximumSize(new java.awt.Dimension(71, 23));
        btncerrar.setMinimumSize(new java.awt.Dimension(71, 23));
        btncerrar.setPreferredSize(new java.awt.Dimension(71, 23));
        btncerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncerrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlcontendor, javax.swing.GroupLayout.DEFAULT_SIZE, 547, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(233, Short.MAX_VALUE)
                .addComponent(btnaceptar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btncerrar, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(186, 186, 186))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlcontendor, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnaceptar)
                    .addComponent(btncerrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnaceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaceptarActionPerformed
      
       if(this.txtRutaBase.getText()==null || txtRutaBase.getText().length()==0 )
           JOptionPane.showMessageDialog(this,"Ingrese la ruta de los archivos de Base de Datos"  , "Mensaje del Sisterma", JOptionPane.ERROR_MESSAGE);
        else   
            dialogo.dispose();
    }//GEN-LAST:event_btnaceptarActionPerformed

    private void btncerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncerrarActionPerformed
        this.txtRutaBase.setText(null);
        dialogo.dispose();
    }//GEN-LAST:event_btncerrarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnaceptar;
    private javax.swing.JButton btncerrar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel pnlcontendor;
    private javax.swing.JTextField txtRutaBase;
    // End of variables declaration//GEN-END:variables
}
