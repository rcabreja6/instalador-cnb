
/******************************************************************************/
//                       Clase FrminfoProceso Versión 2.0
//Objetivo:        Interfaz GUI que informa al usuario el estado del proceso de instalación Bae de Datos
//Fecha Creación:  2019-03-02
//Responsable:     Sistemas
//Empresa:         Hiper S.A
/******************************************************************************/

package soft.gui;

//import java.awt.Dialog.ModalityType;
import javax.swing.JDialog;
import soft.deployment.deploymentLic;
import java.awt.HeadlessException;
//import java.beans.PropertyChangeEvent;
//import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.concurrent.ExecutionException;
import javax.swing.JFrame;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import javax.swing.JFrame;
import javax.swing.JOptionPane;
import soft.deployment.baseDeployment;
import soft.deployment.parametrosConexion;
import soft.gui.sqlserver.FrmRutaBase;

public class FrminfoProceso extends javax.swing.JFrame {
    private final JDialog dialogo;
    private final deploymentLic licencia;
    private final FrminfoProceso ofrminfoProceso;
   
    private final parametrosConexion conexionBaseEsquemaSwt;
    private final parametrosConexion conexionBaseAdminSwt;
    private final parametrosConexion conexionBaseEsquemaLlaves;
    private final parametrosConexion conexionBaseAdminLlaves;
    private final parametrosConexion conexionBaseAdminClientes;
    private final parametrosConexion conexionBaseEsquemaClientes;
    private final parametrosConexion conexionBaseAdminDatos;
    private final parametrosConexion conexionBaseEsquemaDatos;
    
    
    private void procesarOracle(){
        jbarraprogreso.setValue(0);
        
        baseDeployment obaseDeployment=null;
        jbarraprogreso.setValue(0);
        
        if(licencia.getMotorBase().equalsIgnoreCase("ORACLE"))
            obaseDeployment=  new baseDeployment(licencia.getListaFiles(),conexionBaseAdminSwt,conexionBaseEsquemaSwt,
            conexionBaseAdminLlaves,conexionBaseEsquemaLlaves){                   
                    @Override
                    protected void done() {
                        int resultado=0;

                        try {
                            resultado = get();

                            if (this.getProgress()==100)
                                lblestado.setText("Estado: Proceso culminado" );

                            if (resultado!=-2){ 
                                JOptionPane.showMessageDialog(ofrminfoProceso,"Proceso de instalación de Base de Datos ha culminado correctamente"  , "Mensaje del Sisterma", JOptionPane.INFORMATION_MESSAGE);    

                            }
                            else {

                                JOptionPane.showMessageDialog(ofrminfoProceso,this.getMensajeError(), "Mensaje del Sisterma", JOptionPane.ERROR_MESSAGE);
                            }  
                        } 
                        catch (HeadlessException | InterruptedException | ExecutionException ex) {
                            JOptionPane.showMessageDialog(ofrminfoProceso,"Error de proceso: " + ex.getMessage(), "Mensaje del Sisterma", JOptionPane.ERROR_MESSAGE);          
                        }
                        finally{
                            lblestado.setText("Estado: No inicializado" );
                            jbarraprogreso.setValue(0);
                            dialogo.dispose();
                        }
                    }

                    @Override
                    protected void process(List <String > chunks){
                        int avance=this.getProgress();
                        jbarraprogreso.setValue(avance); 
                        lblestado.setText("Estado: " + chunks.get(0) );
                    }   
            };
        else if(licencia.getMotorBase().equalsIgnoreCase("SQLSERVER")){
            
            JDialog odialogoadicEng= new JDialog(dialogo,true);
            JDialog odialogoadicSeg=new JDialog(dialogo,true);
            
            final FrmRutaBase ofrmrutabaseEngien;
            final FrmRutaBase ofrmrutabaseSeg;
            
            ofrmrutabaseEngien= new FrmRutaBase(odialogoadicEng);
            ofrmrutabaseSeg=new FrmRutaBase(odialogoadicSeg);
           
            odialogoadicEng.setTitle("Registro de Directorio de Archivos Base del Engine");
            odialogoadicEng.add(ofrmrutabaseEngien.getContentPane());
            odialogoadicEng.setSize(ofrmrutabaseEngien.getSize());
            odialogoadicEng.setResizable(false);
            odialogoadicEng.pack();
            odialogoadicEng.setLocationRelativeTo(null); 
            
            odialogoadicSeg.setTitle("Registro de Directorio de Archivos Base de Seguridad");
            odialogoadicSeg.add(ofrmrutabaseSeg.getContentPane());
            odialogoadicSeg.setSize(ofrmrutabaseSeg.getSize());
            odialogoadicSeg.setResizable(false);
            odialogoadicSeg.pack();
            odialogoadicSeg.setLocationRelativeTo(null);
            
            obaseDeployment=  new baseDeployment(licencia.getListaFiles(),conexionBaseAdminSwt,
            conexionBaseEsquemaSwt,
            conexionBaseAdminLlaves,conexionBaseEsquemaLlaves){    
                    
                    @Override
                    protected Integer doInBackground() throws Exception {
                        int resultado;
       
                        setProgress(0);
                        
                        publish("Validando datos ingresados");
                        
                        resultado= getNotificarRuta(0); //Notificación Base Engine
                        
                        if(resultado==1){
                            odialogoadicEng.setVisible(true);
                            rutaBaseEngine=ofrmrutabaseEngien.getRuta();
                        
                            if (rutaBaseEngine==null){
                                resultado=-2;
                                error_app="Instalacion se aborto por no ingresar la ruta de instalacion de la Base de Datos del Engine";
                            }    
                        }
                        
                        if(resultado!=-2){
                            resultado= getNotificarRuta(2); //Notificación de ruta de Base de Llaves
                        }
                       
                        if(resultado==1){   
                            odialogoadicSeg.setVisible(true);
                            rutaBaseLlaves=ofrmrutabaseSeg.getRuta();
                        
                            if (rutaBaseLlaves==null){
                                resultado=-2;
                                error_app="Instalacion se aborto por no ingresar la ruta de instalacion de la Base de Datos de Seguridad";
                            }    
                        }
                        
                        if(resultado!=-2){
                            resultado=crearBaseSQL();
                        }
 
                        return resultado;
                    }
                
                    @Override
                    protected void done() {
                        int resultado=0;
                        
                        try {
                            resultado = get();

                            if (this.getProgress()==100)
                                lblestado.setText("Estado: Proceso culminado" );

                            if (resultado!=-2){ 
                                JOptionPane.showMessageDialog(ofrminfoProceso,"Proceso de instalación de Base de Datos ha culminado correctamente"  , "Mensaje del Sisterma", JOptionPane.INFORMATION_MESSAGE);    

                            }
                            else {

                                JOptionPane.showMessageDialog(ofrminfoProceso,this.getMensajeError(), "Mensaje del Sisterma", JOptionPane.ERROR_MESSAGE);
                            }  
                        } 
                        catch (HeadlessException | InterruptedException | ExecutionException ex) {
                            JOptionPane.showMessageDialog(ofrminfoProceso,"Error de proceso: " + ex.getMessage(), "Mensaje del Sisterma", JOptionPane.ERROR_MESSAGE);          
                        }
                        finally{
                            lblestado.setText("Estado: No inicializado" );
                            jbarraprogreso.setValue(0);
                            dialogo.dispose();
                        }
                    }

                    @Override
                    protected void process(List <String > chunks){
                        int avance=this.getProgress();

                        jbarraprogreso.setValue(avance); 
                        lblestado.setText("Estado: " + chunks.get(0) );
                    }   
            };
        }
             
        if(obaseDeployment!=null){ 
            obaseDeployment.setConexionAdminCliente(conexionBaseAdminClientes);
            obaseDeployment.setConexionEsquemaCliente(conexionBaseEsquemaClientes); 

            obaseDeployment.setConexionAdminDatos(conexionBaseAdminDatos); 
            obaseDeployment.setConexionEsquemaDatos(conexionBaseEsquemaDatos);     
            obaseDeployment.execute();  
        }
    }
 
    public FrminfoProceso(JDialog odialogo,deploymentLic infolicencia, 
    parametrosConexion oconexionBaseEsquemaSwt,parametrosConexion oconexionBaseAdminSwt,
    parametrosConexion oconexionBaseEsquemaLlaves, parametrosConexion oconexionBaseAdminLlaves,
    parametrosConexion oconexionBaseAdminClientes,parametrosConexion oconexionBaseEsquemaClientes,
    parametrosConexion oconexionBaseAdminDatos,parametrosConexion oconexionBaseEsquemaDatos ) {
        initComponents();
        
        jbarraprogreso.setValue(0);
        jbarraprogreso.setMaximum(100);
        jbarraprogreso.setStringPainted(true);
        lblestado.setText("Estado: No inicializado" );
            
        dialogo=odialogo;
        dialogo.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        
        licencia=infolicencia;
        ofrminfoProceso=this;
        
      
        conexionBaseEsquemaSwt=oconexionBaseEsquemaSwt;
        conexionBaseAdminSwt=oconexionBaseAdminSwt;
        conexionBaseEsquemaLlaves=oconexionBaseEsquemaLlaves;
        conexionBaseAdminLlaves=oconexionBaseAdminLlaves;
        conexionBaseAdminClientes=oconexionBaseAdminClientes;
        conexionBaseEsquemaClientes=oconexionBaseEsquemaClientes;
        conexionBaseAdminDatos=oconexionBaseAdminDatos;
        conexionBaseEsquemaDatos=oconexionBaseEsquemaDatos;
        
     
        procesarOracle();
        
    }

  
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlbarraestado = new javax.swing.JPanel();
        pnlcontprogressbar = new javax.swing.JPanel();
        lblestado = new javax.swing.JLabel();
        jbarraprogreso = new javax.swing.JProgressBar();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Informacion del Proceso");
        setUndecorated(true);
        setResizable(false);

        pnlbarraestado.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createEtchedBorder(java.awt.Color.white, null), javax.swing.BorderFactory.createEmptyBorder(1, 0, 1, 1)));
        pnlbarraestado.setMaximumSize(new java.awt.Dimension(425, 50));
        pnlbarraestado.setMinimumSize(new java.awt.Dimension(425, 50));
        pnlbarraestado.setPreferredSize(new java.awt.Dimension(425, 500));

        pnlcontprogressbar.setPreferredSize(new java.awt.Dimension(50, 50));

        lblestado.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblestado.setText("Estado:");
        lblestado.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        lblestado.setMaximumSize(new java.awt.Dimension(50, 14));
        lblestado.setPreferredSize(new java.awt.Dimension(40, 14));

        jbarraprogreso.setMaximumSize(new java.awt.Dimension(425, 14));
        jbarraprogreso.setMinimumSize(new java.awt.Dimension(425, 14));
        jbarraprogreso.setPreferredSize(new java.awt.Dimension(425, 14));

        javax.swing.GroupLayout pnlcontprogressbarLayout = new javax.swing.GroupLayout(pnlcontprogressbar);
        pnlcontprogressbar.setLayout(pnlcontprogressbarLayout);
        pnlcontprogressbarLayout.setHorizontalGroup(
            pnlcontprogressbarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlcontprogressbarLayout.createSequentialGroup()
                .addGroup(pnlcontprogressbarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jbarraprogreso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(pnlcontprogressbarLayout.createSequentialGroup()
                        .addComponent(lblestado, javax.swing.GroupLayout.PREFERRED_SIZE, 434, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 70, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnlcontprogressbarLayout.setVerticalGroup(
            pnlcontprogressbarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlcontprogressbarLayout.createSequentialGroup()
                .addComponent(lblestado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbarraprogreso, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8))
        );

        javax.swing.GroupLayout pnlbarraestadoLayout = new javax.swing.GroupLayout(pnlbarraestado);
        pnlbarraestado.setLayout(pnlbarraestadoLayout);
        pnlbarraestadoLayout.setHorizontalGroup(
            pnlbarraestadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlbarraestadoLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(pnlcontprogressbar, javax.swing.GroupLayout.DEFAULT_SIZE, 514, Short.MAX_VALUE)
                .addContainerGap())
        );
        pnlbarraestadoLayout.setVerticalGroup(
            pnlbarraestadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlbarraestadoLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(pnlcontprogressbar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(pnlbarraestado, javax.swing.GroupLayout.DEFAULT_SIZE, 534, Short.MAX_VALUE)
                .addGap(6, 6, 6))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(pnlbarraestado, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JProgressBar jbarraprogreso;
    private javax.swing.JLabel lblestado;
    private javax.swing.JPanel pnlbarraestado;
    private javax.swing.JPanel pnlcontprogressbar;
    // End of variables declaration//GEN-END:variables
}
