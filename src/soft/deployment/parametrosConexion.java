
/******************************************************************************/
//                      Clase parametrosConexion Versión 2.0
//Objetivo:        Clase que almacena Parametros de Conexión de Base de Datos
//Fecha Creación:  2019-03-02
//Responsable:     Sistemas
//Empresa:         Hiper S.A
/******************************************************************************/

package soft.deployment;

public class parametrosConexion {
    private String mEsquema;
    private String mServidorBaseDatos;
    private String mClaveBaseDatos;
    private int mpuertoBase;
    private String mNombreTableSpace;
    private String mServiceName;
    private int mtipoConexiom;
    private String mrutaWallet;
    private String mNombreRol;
    private String mMotorBase;
    
    private String mHostNameCertificado;
    private String mClaveCertificado;
    
    private String mNomnbreBaseDatos;
    
    public String getNombreBaseDatos(){
        return mNomnbreBaseDatos;
    }
    
    public void setNombreBaseDatos(String nomnbreBD){
        mNomnbreBaseDatos=nomnbreBD;
    }
    
    public String getHostNameCertificado(){
        return mHostNameCertificado;
    }
    
    public void setHostNameCertificado(String hostnameCertificado){
        mHostNameCertificado=hostnameCertificado;
    }
    
    public String getClaveCertificado(){
        return mClaveCertificado;
    }
    
    public void setClaveCertificado(String claveCertificado){
        mClaveCertificado=claveCertificado;
    }
    
    public String getMotorBaseDatos(){
        return mMotorBase;
    }
    
    public void setMotorBaseDatos(String Base){
        mMotorBase=Base;
    }
    
    public void setNombreRol(String rol){
        mNombreRol=rol;
    }
   
    public void setRutaWallet(String value){
        mrutaWallet=value;
    }
    
    public String getNombreRol(){
        return mNombreRol;
    }
    
    public String getRutaWallet(){
        return mrutaWallet;
    }
    
    public int getTipoConexion(){
        return mtipoConexiom;
    }
    
    public void setTipoConexion(int value){
        mtipoConexiom=value;
    }
    
    public void setServiceName(String ServiceName){
        mServiceName=ServiceName;
    }
    
    public String getServiceName(){
        return mServiceName;
    }
    
    public void setNombreTableSpace(String TableSpace){
        mNombreTableSpace=TableSpace;
    }
    
    public String getNombreTableSpace(){
        return mNombreTableSpace;
    }
    
    public void setPuertoBase(int puerto){
        mpuertoBase=puerto;
    }
    
    public int getPuertoBase(){
        return mpuertoBase;
    }
    
    public void setClaveBaseDatos(String claveBaseDatos){
        mClaveBaseDatos=claveBaseDatos;
    }
    
    public String getClaveBaseDatos(){
        return mClaveBaseDatos;
    }
    
    public void setServidorBaseDatos(String nombreServidor) {
        mServidorBaseDatos=nombreServidor;
    }
    
    public String getServidorBaseDatos(){
        return mServidorBaseDatos;
    }

    public String getEsquema() {
        return mEsquema;
    }
    
    
    public void setEsquema(String Esquema) {
        mEsquema = Esquema;
    }
    
    
}
