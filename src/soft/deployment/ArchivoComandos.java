
/******************************************************************************/
//                       Clase ArchivoComandos Versión 20.
//Objetivo:        Clase que proporciona los scripts de creación de Base de Datos
//Fecha Creación:  2019-03-02
//Responsable:     Sistemas
//Empresa:         Hiper S.A
/******************************************************************************/

package soft.deployment;

public class ArchivoComandos {

    private int midsec ;
    private String mfilename;
    private String mContenidoArchivo;
    
    private String mEsquema;
    private parametrosConexion conexionBase;
    
    public void setParametrosConexion(parametrosConexion conexion){
        conexionBase=conexion;
    }
    
    public parametrosConexion getParametrosConexion(){
        return conexionBase;
    }
    
    public String getContenidoArchivo(){
        return mContenidoArchivo;
    }
    
    public void setContenidoArchivo(String contenido){
        mContenidoArchivo=contenido;
    }
    
 
    public String getEsquema() {
        return mEsquema;
    }
    
   
    public void setEsquema(String Esquema) {
        mEsquema = Esquema;
    }
    
    
    public void setSecuencia(int value){
        midsec=value;
    }
    
    public void setFileName(String value){
        mfilename=value;
    }
    
    public int getidsec() {
        return midsec;
    }

  
    public String getfilename() {
        return mfilename;
    }
  
}