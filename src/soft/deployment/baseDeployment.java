
/******************************************************************************/
//                       Clase baseDeployment Versión 20.
//Objetivo:        Clase que ejecuta  instalación de  la Base de Datos
//Fecha Creación:  2019-03-02
//Responsable:     Sistemas
//Empresa:         Hiper S.A
/******************************************************************************/

package soft.deployment;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.swing.SwingWorker;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import soft.conexionbase.*;
import soft.conexionbase.parametrosDatos.tipoDatoValor;


public  class baseDeployment  extends SwingWorker<Integer, String>{
    protected String error_app;
    private final parametrosConexion conexionEsquemaSwt,conexionAdminSwt;
    private final parametrosConexion conexionEsquemaLlaves, conexionAdminLlaves;
    
    private parametrosConexion conexionAdminClientes,conexionEsquemaClientes;
    private parametrosConexion conexionAdminDatos, conexionEsquemaDatos;
    
    private  int limite;
    private int icount=0;
    
    protected String rutaBaseEngine;
    protected String rutaBaseLlaves;
    
  
    public void setConexionAdminDatos(parametrosConexion value){
        conexionAdminDatos=value;
    }
    
    public void setConexionEsquemaDatos(parametrosConexion value){
        conexionEsquemaDatos=value;
        
        if(conexionEsquemaDatos!=null)
            limite+=2;
    }
    
    public void setConexionEsquemaCliente(parametrosConexion value){
        conexionEsquemaClientes=value;
        
        if(conexionEsquemaClientes!=null)
            limite+=2;
    }
    
    public void setConexionAdminCliente(parametrosConexion value){
        conexionAdminClientes=value;
    }
    
    public String getMensajeError(){
        return error_app;
    }
    
    private final ArrayList <ArchivoComandos> listaArchivoComandos;

    @Override
    protected Integer doInBackground() throws Exception {
       int resultado;
       
       setProgress(0);
       resultado= this.crearBaseOracle();
       
       return resultado;
    }
    

    public baseDeployment(ArrayList <ArchivoComandos> listafiles,
    parametrosConexion conexionBaseAdminSwt,parametrosConexion conexionBaseEsquemaSwt,
    parametrosConexion conexionBaseAdminLlaves, parametrosConexion conexionBaseEsquemaLlaves){
        conexionEsquemaSwt=conexionBaseEsquemaSwt;
        conexionAdminSwt=conexionBaseAdminSwt;
        conexionEsquemaLlaves=conexionBaseEsquemaLlaves;
        conexionAdminLlaves=conexionBaseAdminLlaves;
        listaArchivoComandos=listafiles;
        limite=listafiles.size() +5;
        
    }
    
    protected int getNotificarRuta(int opcion ){
        int resultado=0;
        
        try{
            resultado=validarNotificacion(opcion);
        }
        catch(SQLException ex){
            resultado=-2;
            this.error_app=ex.getMessage();
        }
        
        
        return resultado;
    }
    
    private int validarNotificacion(int opcion) throws SQLException{
        int resultado=0;
        ArrayList <parametrosDatos> parametros;
        parametrosDatos parametroDato = null;
        ConexionBD oconexion=null;
        
        String vsql ;
        ResultSet rs;
        PreparedStatement stmt;
        String resultadoConsulta=null;
        
       switch(opcion){
            case 0: //Usuario Admin ENGINE
//                
//                if(conexionAdminSwt.getEsquema().equalsIgnoreCase(this.conexionEsquemaSwt.getEsquema())){
//                    resultado=-2;
//                    error_app="El nombre de usuario de la Base de Datos del Engine no puede ser el usuario Administrador";
//                }
                
                if(resultado!=-2){
                    if(conexionAdminSwt.getNombreBaseDatos().equalsIgnoreCase(this.conexionEsquemaSwt.getNombreBaseDatos())){
                        resultado=-2;
                        error_app="El nombre de la Base de Datos del Engine es incorrecto. Verifique su ingreso";
                    }
                }
                
                if(resultado!=-2){
                    oconexion= new ConexionBD(conexionAdminSwt);
                    error_app=  oconexion.getMensajeError();

                     if(error_app!=null )
                         error_app="Error con el Usuario de Base de Datos del Engine: " + error_app;
                }
                 
                 break;

            case 1: //Esquema ENGINE
               oconexion= new ConexionBD(conexionEsquemaSwt);
               error_app=  oconexion.getMensajeError();
                
                if(error_app!=null )
                     error_app="Error con el Esquema de Base de Datos del Engine: " + error_app;
               
               break;

            case 2: //Usuario Admin Base Llaves
                
//                if(conexionEsquemaLlaves.getEsquema().equalsIgnoreCase(conexionAdminLlaves.getEsquema())){
//                    resultado=-2;
//                    error_app="El nombre de usuario de la Base de Datos de Seguridad no puede ser el usuario Administrador";
//                }
                
                if(resultado!=-2){
                    if(conexionEsquemaLlaves.getNombreBaseDatos().equalsIgnoreCase(conexionAdminLlaves.getNombreBaseDatos())){
                        resultado=-2;
                        error_app="El nombre de la Base de Datos de Seguridad no es correcto. Verifique su ingreso";
                    }
                }
                
                if(resultado!=-2){
                
                    oconexion= new ConexionBD(conexionAdminLlaves);
                    error_app=  oconexion.getMensajeError();

                    if(error_app!=null )
                         error_app="Error con el Usuario de Base de Datos de Seguridad: " + error_app;
                }
                
                break;

            case 3: //Esquema Base Llaves
                oconexion= new ConexionBD(conexionEsquemaLlaves);
                error_app=  oconexion.getMensajeError();
                
                if(error_app!=null )
                     error_app="Error con el Esquema de Base de Datos de Seguridad: " + error_app;
                
                break;
                
                 
            case 4: //Uusuario Base Cliente
               oconexion= new ConexionBD(this.conexionAdminClientes);
              
                error_app=  oconexion.getMensajeError();
                
                if(error_app!=null )
                     error_app="Error con el Usuario de Base de Datos Clientes: " + error_app;
                
                break;
                
            case 5: //Esquema Base Cliente
                oconexion = new ConexionBD(this.conexionEsquemaClientes);
                
                 error_app=  oconexion.getMensajeError();
                
                if(error_app!=null )
                     error_app="Error con el Esquema de Base de Datos Clientes: " + error_app;
                break;
               
            case 6: //Usuario de Base de Datos Vault
                oconexion=  new ConexionBD(this.conexionAdminDatos);
                
                error_app=  oconexion.getMensajeError();
                
                if(error_app!=null )
                     error_app="Error con el Usuario de Base de Datos Vault: " + error_app;
                
                break;
                
            case 7: //Esquema Base de Datos Vault
                oconexion= new ConexionBD(this.conexionEsquemaDatos);
                
                error_app=  oconexion.getMensajeError();
                
                if(error_app!=null )
                     error_app="Error con el Esquema de Base de Datos Vault: " + error_app;
                
                break;
                
        }//Fin de switch(opcion)
        
         if(error_app!=null )
            resultado=-2;
         
        if (resultado!=-2){

            if(opcion==0 || opcion==2){
                vsql=   "select  1 resultado, upper(default_database_name)  from sys.server_principals ";
                vsql+=  "where ";
                vsql+=  "     upper(name)=upper(?) ";

                parametros=new ArrayList();

                if(opcion==0)
                    parametroDato= new parametrosDatos(1,conexionEsquemaSwt.getEsquema(),tipoDatoValor.String);
                else if(opcion==2)
                    parametroDato= new parametrosDatos(1,conexionEsquemaLlaves.getEsquema(),tipoDatoValor.String);

                parametros.add(parametroDato);

                rs= oconexion.getResultado(vsql, parametros);

                error_app= oconexion.getMensajeError();

                if (error_app!=null )
                    resultado=-2;

                if (resultado!=-2){
                    if (rs!=null){

                        while(rs.next()){
                            resultadoConsulta=rs.getString(2); 
                        }

                        if(resultadoConsulta!=null ){ //Si el usuario existe se verifica la Base de Datos
                            //resultado=1;
                            
                            if (opcion==0){ //Esquema de Base de Datos de Engine
                                
                                if(conexionEsquemaSwt.getEsquema().equalsIgnoreCase(conexionAdminSwt.getEsquema())==false)
                                    if( resultadoConsulta.equalsIgnoreCase(conexionEsquemaSwt.getNombreBaseDatos())==false){
                                        error_app="Error en Base de Datos Engine: El usuario tiene asociado otra Base de Datos";
                                        resultado=-2;
                                    }  
                            }

                            if(opcion==2){ //Esquema de Base de Datos de Seguridad
                                if(conexionEsquemaLlaves.getEsquema().equalsIgnoreCase(conexionAdminLlaves.getEsquema())==false)
                                    if( resultadoConsulta.equalsIgnoreCase(conexionEsquemaLlaves.getNombreBaseDatos())==false){
                                        error_app="Error en Base de Datos Seguridad: El usuario tiene asociado otra Base de Datos";
                                        resultado=-2;
                                    }
                              
                            }
                        }
                        else if (resultadoConsulta==null)
                            resultado=0; //Esquema de Base de Datos Engine no existe

                        rs.close();
                    }//Fin de if (rs!=null)
                }//Fin de if (resultado!=-2)
                
                if(resultado!=-2 && resultado==0){
                    vsql=   "select  count(1) from sys.databases ";
                    vsql+=  "where ";
                    vsql+=  "     upper(name)=upper(?) ";
                    
                     parametros.clear();
                     
                    if(opcion==0)
                        parametroDato= new parametrosDatos(1,conexionEsquemaSwt.getNombreBaseDatos(),tipoDatoValor.String);
                    else if(opcion==2)
                        parametroDato= new parametrosDatos(1,conexionEsquemaLlaves.getNombreBaseDatos(),tipoDatoValor.String);
                    
                    parametros.add(parametroDato);

                    rs= oconexion.getResultado(vsql, parametros);

                    error_app= oconexion.getMensajeError();

                    if (error_app!=null )
                        resultado=-2;
                    }
                
                    if(resultado!=-2 && rs!=null){
                        while(rs.next()){
                            resultado=rs.getInt(1);
                        }
                        
                        if(resultado==1) 
                            resultado=0; //Si existe la Base entonces no notificar ruta
                        else
                            resultado=1; //Notificar ruta si no existe la Base de Datos
                        
                        rs.close();
                    }           
            }//Fin de if(opcion==0 || opcion==2){   
        }//Fin de if (resultado!=-2)
        
        if(oconexion!=null && oconexion.isConectado())
            oconexion.cerrar(); 
        
        return resultado;
    }
    
    private int crearLoginSql(String nombreLogin, String DefaultBase,parametrosConexion paramcon){
        int resultado=0;
        ArrayList <parametrosDatos> parametros;
        parametrosDatos parametroDato ;
        ResultSet rs;
       
        ConexionBD oconexionbd;
        
        String vsql ;
        
        vsql=   "select  1 resultado, upper(default_database_name)  from sys.server_principals ";
        vsql+=  "where ";
        vsql+=  "     upper(name)=upper(?) ";
        
        try{
        
            parametros=new ArrayList();
            parametroDato= new parametrosDatos(1,nombreLogin,tipoDatoValor.String);
        
            parametros.add(parametroDato);
            
            oconexionbd= new ConexionBD(paramcon);
        
            rs= oconexionbd.getResultado(vsql, parametros);
            
            error_app= oconexionbd.getMensajeError();
				
            if (error_app!=null ){
                error_app= "Error en proceso crearLoginSql: " + error_app;
                resultado=-2;    
            }
        
            if (resultado!=-2){

                if (rs!=null){

                    while(rs.next()){
                        resultado=rs.getInt(1);   
                    }
                    
                    rs.close();
                }
            }//Fin de  if (resultado!=-2)
            
            if (resultado!=2 && resultado==0){ //Si el login no existe en el motor de Base de Datos
                vsql="create login " + nombreLogin + "\n";
                vsql +="with password= 'hiper' ," + "\n" ;
                vsql +="default_database=" + DefaultBase + ", " + "\n";
                vsql +="check_expiration=Off, " + "\n";
                vsql +="check_policy=Off ";
                
                resultado=oconexionbd.ejecutarSql(vsql); 
                error_app= oconexionbd.getMensajeError();
                
                 if(error_app!=null)   
                    error_app= "Error en proceso de creación de login Base de Datos  " + nombreLogin + ": " + error_app;

                if(resultado!=-2){
                   oconexionbd.confirmar();
                   error_app=  oconexionbd.getMensajeError();
                }
                
            }//Fin de if (resultado!=2 && resultado==0)
            
             if(oconexionbd.isConectado())
                oconexionbd.cerrar();
   
        }
        catch(SQLException ex){
            resultado=-2;
            error_app= "Ocurrio el siguiente error en la Base de Datos: " +  ex.getMessage();
        }
 
        return resultado;
    }
    
    private int crearUserSqlRol(String nombreUser,String nombreRol, String nombreLogin,int opcion, parametrosConexion paramcom){
        int resultado=0;
        ArrayList <parametrosDatos> parametros;
        parametrosDatos parametroDato =null;
        ResultSet rs=null;
       
        ConexionBD oconexionbd;
        
        String vsql ;
        
        vsql=   "select  1 resultado   from sys.database_principals  ";
        vsql+=  "where ";
        
        if(opcion==0){
            vsql+= "    type_desc='SQL_USER' and ";
            vsql+=  "     upper(name)=upper(?) ";
        
        }
        else if(opcion==1){
            vsql+= "    type_desc='DATABASE_ROLE' and ";
            vsql+=  "     upper(name)=upper(?) ";
        }
        
        try{
        
            parametros=new ArrayList();
            
            if(opcion==0)
                parametroDato= new parametrosDatos(1,nombreUser,tipoDatoValor.String);
            else if (opcion==1)
                 parametroDato= new parametrosDatos(1,nombreRol,tipoDatoValor.String);
            
            parametros.add(parametroDato);
            
            //paramcom.setNombreBaseDatos(nombreBaseDatos);
        
            oconexionbd= new ConexionBD(paramcom);
            error_app= oconexionbd.getMensajeError();
            
            if(error_app!=null){
                error_app= "Ocurrio en proceso crearUserSqlRol: " + error_app;
                resultado=-2;
            }
            
            if(resultado!=-2){
                rs= oconexionbd.getResultado(vsql, parametros);
                error_app= oconexionbd.getMensajeError(); 
                
                if (error_app!=null ){
                    error_app= "Ocurrio el siguiente error al verificar los datos del usuario: " + error_app;
                    resultado=-2;
                }
            }
            

            if (resultado!=-2){

                if (rs!=null){

                    while(rs.next()){
                        resultado=rs.getInt(1);   
                    }
                    
                    rs.close();
                }
            }//Fin de  if (resultado!=-2)
            
            if (resultado!=2 && resultado==0){ //Si el usuario no existe en la Base de Datos
                
                if(opcion==0){
                    vsql="create user " + nombreUser + "\n";
                    vsql +="for login " + nombreLogin  + "\n" ;
                    vsql +="with default_schema=dbo";
                    
                    
                    resultado=oconexionbd.ejecutarSql(vsql); 
                    error_app=oconexionbd.getMensajeError();
                
                    if(error_app!=null)
                        error_app= "Error en proceso de creación de usuario " + nombreUser + ": " + error_app;
   
                }
                else if (opcion==1){
                    vsql="create role " + nombreRol + "\n";
                    resultado=oconexionbd.ejecutarSql(vsql); 
                    error_app=oconexionbd.getMensajeError();
                    
                    if(error_app!=null)
                        error_app= "Error en proceso de creación de rol " + nombreUser + ": " + error_app;
                
                    if(resultado!=-2){
                         vsql="alter role " + nombreRol + "\n";
                         vsql+="add member " + nombreUser ;
                         
                        resultado=oconexionbd.ejecutarSql(vsql); 
                        error_app=oconexionbd.getMensajeError();
                        
                        if(error_app!=null)
                            error_app= "Error en script de creacion de rol " + nombreUser + ": " + error_app;
                    }
                }
               
               
                if(resultado!=-2){
                    oconexionbd.confirmar();
                    
                    error_app=  oconexionbd.getMensajeError(); 
                    
                    if(error_app!=null){
                       resultado=-2;
                       error_app= "Error en confirmación de cambios Base de Datos: "   + error_app;
                    }
                }

            }//Fin de if (resultado!=2 && resultado==0)
            
            if(oconexionbd.isConectado())
                oconexionbd.cerrar();
   
        }
        catch(SQLException ex){
            resultado=-2;
            error_app= "Ocurrio el siguiente error en la Base de Datos: " +  ex.getMessage();
        }
 
        return resultado;
    }
    
    private int crearUserSql(String nombreUser,String nombreBaseDatos, parametrosConexion paramcom){
        int resultado=0;
        ArrayList <parametrosDatos> parametros;
        parametrosDatos parametroDato ;
        ResultSet rs;
       
        ConexionBD oconexionbd;
        
        String vsql ;
        
        vsql=   "select  1 resultado from sys.database_principals  ";
        vsql+=  "where ";
        vsql+= "    type_desc='SQL_USER' and ";
        vsql+=  "     upper(name)=upper(?) ";
        
        try{
            parametros=new ArrayList();
            parametroDato= new parametrosDatos(1,nombreUser,tipoDatoValor.String);
        
            parametros.add(parametroDato);
            
            paramcom.setNombreBaseDatos(nombreBaseDatos);
        
            oconexionbd= new ConexionBD(paramcom);
        
            rs= oconexionbd.getResultado(vsql, parametros);
            error_app= oconexionbd.getMensajeError();
				
            if (error_app!=null ){
                error_app= "Ocurrio el siguiente error al verificar los datos del usuari: " + error_app;
                resultado=-2;
            }
        
            if (resultado!=-2){

                if (rs!=null){

                    while(rs.next()){
                        resultado=rs.getInt(1);   
                    }
                    
                    rs.close();
                }
            }//Fin de  if (resultado!=-2)
            
            if (resultado!=2 && resultado==0){ //Si el usuario no existe en la Base de Datos
                vsql="create user " + nombreUser + "\n";
                vsql +="for login " + nombreUser  + "\n" ;
               
                resultado=oconexionbd.ejecutarSql(vsql); 
                error_app=oconexionbd.getMensajeError();
                
                if(error_app!=null)
                    error_app= "Error en proceso de creación de usuario Base de Datos  " + nombreUser + ": " + error_app;

                if(resultado!=-2){
                    vsql="exec sp_addrolemember N'db_owner', N'" + nombreUser + "'" ;
                    
                    resultado=oconexionbd.ejecutarSql(vsql); 
                    error_app=oconexionbd.getMensajeError();
                
                    if(error_app!=null)
                        error_app= "Error en proceso de creación de usuario Base de Datos  " + nombreUser + ": " + error_app;
                }
                
                
                if(resultado!=-2){
                    oconexionbd.confirmar();
                    
                    error_app=  oconexionbd.getMensajeError(); 
                    
                    if(error_app!=null){
                       resultado=-2;
                       error_app= "Error en proceso de creación de usuario Base de Datos  " + nombreUser + ": " + error_app;
                    }
                }

            }//Fin de if (resultado!=2 && resultado==0)
            

            if(oconexionbd.isConectado())
                oconexionbd.cerrar();
   
        }
        catch(SQLException ex){
            resultado=-2;
            error_app= "Ocurrio el siguiente error en la Base de Datos: " +  ex.getMessage();
        }
 
        return resultado;
    }
    
    private int crearBaseDatos(String nombreBase, String rutaInstalacion,parametrosConexion paramcon){
        int resultado=0;
        ConexionBD oconexionbd;
        
        String vsql ;
        
        vsql="create database " + nombreBase + "\n" ;
        vsql += "on " + "\n";
        vsql +="(" + "\n";
        vsql +="    name=" + nombreBase + "_dat," + "\n";
        vsql +="    filename='" + rutaInstalacion + "\\" + nombreBase + ".mdf' ," + "\n" ;
        vsql +="    size = 5MB," + "\n";
        vsql +="    filegrowth = 5MB " + "\n";
        vsql +=") " + "\n";
        
        vsql +="log on " + "\n";
        vsql +="(" + "\n";
        
        vsql +="    name=" + nombreBase + "_log," + "\n";
        vsql +="    filename='" + rutaInstalacion + "\\" + nombreBase + ".ldf' ," + "\n" ;
        vsql +="    size = 5MB," + "\n";
        vsql +="    filegrowth = 5MB " + "\n";
        vsql +=") " + "\n";
        
        vsql +="collate SQL_Latin1_General_CP1_CI_AS ";
        
        oconexionbd= new ConexionBD(paramcon);
        
        error_app= oconexionbd.getMensajeError();
        
        if(error_app!=null){
            error_app="Ocurrio el siguiente error en Base de Datos: " + error_app;
            resultado=-2;
        }
        
        if(resultado!=2){
            resultado=oconexionbd.ejecutarSql(vsql); 
            error_app= oconexionbd.getMensajeError(); 
            
            if(error_app!=null)  
                error_app= "Error en proceso de creación de Base de Datos  " + nombreBase + ": " + error_app;         
         }
        
        if(resultado!=-2){
            oconexionbd.confirmar();
            error_app=  oconexionbd.getMensajeError();
            
            if(error_app!=null)  {
                resultado=-2;
                error_app= "Error en proceso de creación de Base de Datos  " + nombreBase + ": " + error_app;
             }             
        }
        
        if(oconexionbd.isConectado())
             oconexionbd.cerrar();
        
        return resultado;
    }
    
    private int validarSQL(int opcion) throws SQLException{
        int resultado=0;
        
        ArrayList <parametrosDatos> parametros;
        parametrosDatos parametroDato = null;
        ConexionBD oconexion=null;
        
        String vsql ;
        ResultSet rs;
        //PreparedStatement stmt;
        
        String resultadoConsulta=null;
        
        switch(opcion){
            case 0: //Usuario Admin ENGINE
                oconexion= new ConexionBD(conexionAdminSwt);
                error_app=  oconexion.getMensajeError();
                
                 if(error_app!=null )
                     error_app="Error con el Usuario Administrador de Base de Datos del Engine: " + error_app;
                
                break;

            case 1: //Esquema ENGINE
               oconexion= new ConexionBD(conexionEsquemaSwt);
               error_app=  oconexion.getMensajeError();
                
                if(error_app!=null )
                     error_app="Error con el Usuario de Base de Datos del Engine: " + error_app;
               
               break;

            case 2: //Usuario Admin Base Llaves
                oconexion= new ConexionBD(conexionAdminLlaves);
                error_app=  oconexion.getMensajeError();
                
                if(error_app!=null )
                     error_app="Error con el Usuario Administrador de Base de Datos de Seguridad: " + error_app;
                
                break;

            case 3: //Esquema Base Llaves
                oconexion= new ConexionBD(conexionEsquemaLlaves);
                error_app=  oconexion.getMensajeError();
                
                if(error_app!=null )
                     error_app="Error con el Usuario de Base de Datos de Seguridad: " + error_app;
                
                break;
                
                 
            case 4: //Uusuario Base Cliente
               oconexion= new ConexionBD(this.conexionAdminClientes);
              
                error_app=  oconexion.getMensajeError();
                
                if(error_app!=null )
                     error_app="Error con el Usuario Administrador de Base de Datos Clientes: " + error_app;
                
                break;
                
            case 5: //Esquema Base Cliente
                oconexion = new ConexionBD(this.conexionEsquemaClientes);
                
                 error_app=  oconexion.getMensajeError();
                
                if(error_app!=null )
                     error_app="Error con el Usuario de Base de Datos Clientes: " + error_app;
                break;
               
            case 6: //Usuario de Base de Datos Vault
                oconexion=  new ConexionBD(this.conexionAdminDatos);
                
                error_app=  oconexion.getMensajeError();
                
                if(error_app!=null )
                     error_app="Error con el Usuario de Base de Datos Vault: " + error_app;
                
                break;
                
            case 7: //Esquema Base de Datos Vault
                oconexion= new ConexionBD(this.conexionEsquemaDatos);
                
                error_app=  oconexion.getMensajeError();
                
                if(error_app!=null )
                     error_app="Error con el Esquema de Base de Datos Vault: " + error_app;
                
                break;
                
        }//Fin de switch(opcion)
        
        if(error_app!=null )
            resultado=-2;
        
        if (resultado!=-2){

            if(opcion==0 || opcion==2){
                vsql=   "select  1 resultado, upper(default_database_name)  from sys.server_principals ";
                vsql+=  "where ";
                vsql+=  "     upper(name)=upper(?) ";

                parametros=new ArrayList();

                if(opcion==0)
                    parametroDato= new parametrosDatos(1,conexionEsquemaSwt.getEsquema(),tipoDatoValor.String);
                else if(opcion==2)
                    parametroDato= new parametrosDatos(1,conexionEsquemaLlaves.getEsquema(),tipoDatoValor.String);

                parametros.add(parametroDato);

                rs= oconexion.getResultado(vsql, parametros);

                error_app= oconexion.getMensajeError();

                if (error_app!=null )
                    resultado=-2;

                if (resultado!=-2){
                    if (rs!=null){

                        while(rs.next()){
                            resultadoConsulta=rs.getString(2); 
                            
                        }

                        if(resultadoConsulta!=null ){ //Si el usuario existe se verifica la Base de Datos
                            resultado=1;

                            if (opcion==0){ //Esquema de Base de Datos de Engine
                                
                                if(conexionEsquemaSwt.getEsquema().equalsIgnoreCase(conexionAdminSwt.getEsquema())==false){
                                    if( resultadoConsulta.equalsIgnoreCase(conexionEsquemaSwt.getNombreBaseDatos())==false){
                                        error_app="Error en Base de Datos Engine: El usuario tiene asociado otra Base de Datos";
                                        resultado=-2;
                                    }
                                }
                                else
                                    resultado=0;
                            }
                            
                            if(opcion==2){ //Esquema de Base de Datos de Seguridad
                                if(conexionEsquemaLlaves.getEsquema().equalsIgnoreCase(conexionAdminLlaves.getEsquema())==false){
                                    if( resultadoConsulta.equalsIgnoreCase(conexionEsquemaLlaves.getNombreBaseDatos())==false){
                                        error_app="Error en Base de Datos Seguridad: El usuario tiene asociado otra Base de Datos";
                                        resultado=-2;
                                    }
                                }
                                else
                                    resultado=0;
 
                            }
                        }
                        else if (resultadoConsulta==null)
                            resultado=0; //Esquema de Base de Datos Engine no existe

                        rs.close();
                    }//Fin de if (rs!=null)
                }//Fin de if (resultado!=-2)
                
                if(resultado!=-2 && resultado==0){
                    vsql=   "select  count(1) from sys.databases ";
                    vsql+=  "where ";
                    vsql+=  "     upper(name)=upper(?) ";
                    
                     parametros.clear();
                     
                    if(opcion==0)
                        parametroDato= new parametrosDatos(1,conexionEsquemaSwt.getNombreBaseDatos(),tipoDatoValor.String);
                    else if(opcion==2)
                        parametroDato= new parametrosDatos(1,conexionEsquemaLlaves.getNombreBaseDatos(),tipoDatoValor.String);
                    
                    parametros.add(parametroDato);

                    rs= oconexion.getResultado(vsql, parametros);

                    error_app= oconexion.getMensajeError();

                    if (error_app!=null )
                        resultado=-2;
                    
                    if(resultado!=-2 && rs!=null){
                        
                        while(rs.next()){
                            resultado=rs.getInt(1);
                        }

                        rs.close();
                    
                    } //Fin de if(resultado!=-2 && rs!=null)  
                    
                }//Fin de if(resultado!=-2 && resultado==0
                
               
            }//Fin de if(opcion==0 || opcion==2)
            
            if(opcion==4){ //Usuario de Base de Datos Admin Cliente
                vsql=   "select  1 resultado, upper(default_database_name)  from sys.server_principals ";
                vsql+=  "where ";
                vsql+=  "     upper(name)=upper(?) ";
                
                parametros=new ArrayList();

                parametroDato= new parametrosDatos(1,this.conexionEsquemaClientes.getEsquema(),tipoDatoValor.String);
                parametros.add(parametroDato);

                rs= oconexion.getResultado(vsql, parametros);
                
                error_app= oconexion.getMensajeError();

                if (error_app!=null)
                    resultado=-2;  
                
                if (resultado!=-2){
                    
                    if (rs!=null){

                        while(rs.next()){
                            resultadoConsulta=rs.getString(2); 
                        }

                        if(resultadoConsulta!=null ){ //Si el usuario existe se verifica la Base de Datos
                            resultado=1;

                            if( resultadoConsulta.equalsIgnoreCase(conexionEsquemaClientes.getNombreBaseDatos())==false){
                                error_app="Error en Base de Datos Cliente: El usuario tiene asociado otra Base de Datos";
                                resultado=-2;
                            } 
                        }
                        else if (resultadoConsulta==null)
                            resultado=0; //Esquema de Base de Datos Engine no existe

                        rs.close();
                    }//Fin de if (rs!=null)
                }//Fin de if (resultado!=-2)
                
                if(resultado!=-2 && resultado==0){
                    vsql=   "select  count(1) from sys.databases ";
                    vsql+=  "where ";
                    vsql+=  "     upper(name)=upper(?) ";
                    
                    parametros.clear();
                     
                    parametroDato= new parametrosDatos(1,conexionEsquemaClientes.getNombreBaseDatos(),tipoDatoValor.String);
                    
                    parametros.add(parametroDato);

                    rs= oconexion.getResultado(vsql, parametros);

                    error_app= oconexion.getMensajeError();

                    if (error_app!=null )
                        resultado=-2;
                
                    if(resultado!=-2 && rs!=null){
                        while(rs.next()){
                            resultado=rs.getInt(1);
                        }

                        rs.close();
                    }       
                }
                
   
            }//Fin de if(opcion==4)
            
            if(opcion==6){ //Base de Datos Vault
                vsql=   "select  1 resultado, upper(default_database_name)  from sys.server_principals ";
                vsql+=  "where ";
                vsql+=  "     upper(name)=upper(?) ";
                
                parametros=new ArrayList();

                parametroDato= new parametrosDatos(1,this.conexionEsquemaDatos.getEsquema(),tipoDatoValor.String);
                parametros.add(parametroDato);

                rs= oconexion.getResultado(vsql, parametros);
                
                error_app= oconexion.getMensajeError();

                if (error_app!=null)
                    resultado=-2;  
                
                if (resultado!=-2){
                    
                    if (rs!=null){

                        while(rs.next()){
                            resultadoConsulta=rs.getString(2); 
                        }

                        if(resultadoConsulta!=null ){ //Si el usuario existe se verifica la Base de Datos
                            resultado=1;

                            if( resultadoConsulta.equalsIgnoreCase(conexionEsquemaDatos.getNombreBaseDatos())==false){
                                error_app="Error en Base de Datos Vault: El usuario tiene asociado otra Base de Datos";
                                resultado=-2;
                            } 
                        }
                        else if (resultadoConsulta==null)
                            resultado=0; //Esquema de Base de Datos Engine no existe

                        rs.close();
                    }//Fin de if (rs!=null)
                }//Fin de if (resultado!=-2)
                
                if(resultado!=-2 && resultado==0){
                    vsql=   "select  count(1) from sys.databases ";
                    vsql+=  "where ";
                    vsql+=  "     upper(name)=upper(?) ";
                    
                    parametros.clear();
                     
                    parametroDato= new parametrosDatos(1,conexionEsquemaDatos.getNombreBaseDatos(),tipoDatoValor.String);
                    
                    parametros.add(parametroDato);

                    rs= oconexion.getResultado(vsql, parametros);

                    error_app= oconexion.getMensajeError();

                    if (error_app!=null )
                        resultado=-2;
                    
                    if(resultado!=-2 && rs!=null){
                        while(rs.next()){
                            resultado=rs.getInt(1);
                        }

                        rs.close();
                    }       
                }
  
            }
            
        }//Fin de if (resultado!=-2)
        
        
        if(oconexion!=null && oconexion.isConectado())
            oconexion.cerrar();
        
        return resultado;
    }
    
    private int validarOracle(int opcion) throws SQLException {
        int resultado=0;
      
        ArrayList <parametrosDatos> parametros;
        parametrosDatos parametroDato = null;
        ConexionBD oconexion=null;
        
        String vsql ;
        ResultSet rs;
        PreparedStatement stmt;
        
        String UsuarioTableSpace=null;
        
        switch(opcion){
            case 0: //Usuario Admin ENGINE
                oconexion= new ConexionBD(conexionAdminSwt);
                error_app=  oconexion.getMensajeError();
                
                 if(error_app!=null )
                     error_app="Error con el Usuario de Base de Datos del Engine: " + error_app;
                
                break;

            case 1: //Esquema ENGINE
               oconexion= new ConexionBD(conexionEsquemaSwt);
               error_app=  oconexion.getMensajeError();
                
                if(error_app!=null )
                     error_app="Error con el Esquema de Base de Datos del Engine: " + error_app;
               
               break;

            case 2: //Usuario Admin Base Llaves
                oconexion= new ConexionBD(conexionAdminLlaves);
                error_app=  oconexion.getMensajeError();
                
                if(error_app!=null )
                     error_app="Error con el Usuario de Base de Datos de Seguridad: " + error_app;
                
                break;

            case 3: //Esquema Base Llaves
                oconexion= new ConexionBD(conexionEsquemaLlaves);
                error_app=  oconexion.getMensajeError();
                
                if(error_app!=null )
                     error_app="Error con el Esquema de Base de Datos de Seguridad: " + error_app;
                
                break;
                
                 
            case 4: //Uusuario Base Cliente
               oconexion= new ConexionBD(this.conexionAdminClientes);
              
                error_app=  oconexion.getMensajeError();
                
                if(error_app!=null )
                     error_app="Error con el Usuario de Base de Datos Clientes: " + error_app;
                
                break;
                
            case 5: //Esquema Base Cliente
                oconexion = new ConexionBD(this.conexionEsquemaClientes);
                
                 error_app=  oconexion.getMensajeError();
                
                if(error_app!=null )
                     error_app="Error con el Esquema de Base de Datos Clientes: " + error_app;
                break;
               
            case 6: //Usuario de Base de Datos Vault
                oconexion=  new ConexionBD(this.conexionAdminDatos);
                
                error_app=  oconexion.getMensajeError();
                
                if(error_app!=null )
                     error_app="Error con el Usuario de Base de Datos Vault: " + error_app;
                
                break;
                
            case 7: //Esquema Base de Datos Vault
                oconexion= new ConexionBD(this.conexionEsquemaDatos);
                
                error_app=  oconexion.getMensajeError();
                
                if(error_app!=null )
                     error_app="Error con el Esquema de Base de Datos Vault: " + error_app;
                
                break;
                
        }//Fin de switch(opcion)
            
        if(error_app!=null )
            resultado=-2;
        
        if (resultado!=-2){
            
            if(opcion==0 || opcion==2){
                vsql=   "select username,default_tablespace from dba_users ";
                vsql+=  "where ";
                vsql+=  "     username=upper(?) ";

                parametros=new ArrayList();

                if(opcion==0)
                    parametroDato= new parametrosDatos(1,conexionEsquemaSwt.getEsquema(),tipoDatoValor.String);
                else if(opcion==2)
                    parametroDato= new parametrosDatos(1,conexionEsquemaLlaves.getEsquema(),tipoDatoValor.String);

                parametros.add(parametroDato);

                rs= oconexion.getResultado(vsql, parametros);

                error_app= oconexion.getMensajeError();

                if (error_app!=null )
                    resultado=-2;

                if (resultado!=-2){
                    if (rs!=null){

                        while(rs.next()){
                            UsuarioTableSpace=rs.getString(2); 
                        }

                        if(UsuarioTableSpace!=null ){ //Si el usuario existe se verifica el TableSpace por Defecto
                            resultado=1;
                            
                            if (opcion==0) //Esquema de Base de Datos de Engine
                                if( UsuarioTableSpace.equalsIgnoreCase(conexionEsquemaSwt.getNombreTableSpace())==false){
                                    error_app="Error en Base de Datos Engine: El nombre del TableSpace no es el correcto";
                                    resultado=-2;
                                }

                            if(opcion==2) //Esquema de Base de Datos de Seguridad
                                if( UsuarioTableSpace.equalsIgnoreCase(conexionEsquemaLlaves.getNombreTableSpace())==false){
                                    error_app="Error en Base de Datos Seguridad: El nombre del TableSpace no es el correcto";
                                    resultado=-2;
                                }
                        }
                        else if (UsuarioTableSpace==null)
                            resultado=0; //Esquema de Base de Datos Engine no existe

                        rs.close();
                    }//Fin de if (rs!=null)

                }//Fin de if (resultado!=-2)
                
                if (resultado!=-2 && resultado==0){
                    vsql=   "select count(1) from dba_tablespaces ";
                    vsql+=  "where ";
                    vsql+=  "     upper(tablespace_name)=upper(?)";

                    parametros.clear();

                    if(opcion==0)
                        parametroDato= new parametrosDatos(1,conexionEsquemaSwt.getNombreTableSpace(),tipoDatoValor.String);
                    else if(opcion==2)
                        parametroDato= new parametrosDatos(1,conexionEsquemaLlaves.getNombreTableSpace(),tipoDatoValor.String);

                    parametros.add(parametroDato);

                    rs=oconexion.getResultado(vsql, parametros);

                    error_app= oconexion.getMensajeError();

                    if (error_app!=null )
                        resultado=-2;
                    
                    if(resultado!=-2){

                        while(rs.next()){
                            resultado=rs.getInt(1); 
                        }

                        if(resultado==0){
                            resultado=-2;

                            if (opcion==0) //Esquema de Base de Datos de Engine
                                error_app="Error en Base de Datos Engine: El tablespace ingresado no existe";
                            else if(opcion==2)
                                error_app="Error en Base de Datos de Seguridad: El tablespace ingresado no existe";
                        }
                        else 
                            resultado=0;

                        rs.close();
                        
                    }//Fin de if(resultado!=-2)
                }//Fin de  if (resultado!=-2 && resultado==0)
                
                if(resultado!=-2 && resultado==0){ //Verificae si el TableSpace esta asociado a otro usuario
                    vsql=   "select username,default_tablespace from dba_users ";
                    vsql+=  "where ";
                    vsql+=  "     default_tablespace=upper(?) ";   
                    
                     parametros.clear();

                    if(opcion==0)
                        parametroDato= new parametrosDatos(1,conexionEsquemaSwt.getNombreTableSpace(),tipoDatoValor.String);
                    else if(opcion==2)
                        parametroDato= new parametrosDatos(1,conexionEsquemaLlaves.getNombreTableSpace(),tipoDatoValor.String);
                    
                    parametros.add(parametroDato);
                     
                    rs=oconexion.getResultado(vsql, parametros);

                    error_app= oconexion.getMensajeError();
                    UsuarioTableSpace=null;
                    
                    if (error_app!=null )
                        resultado=-2;
                     
                    if(resultado!=-2){
                        
                        while(rs.next()){
                              UsuarioTableSpace=rs.getString(1); 
                        }
                        
                        rs.close();
                        
                        if(UsuarioTableSpace!=null){
                            if(opcion==0){

                                if(UsuarioTableSpace.equalsIgnoreCase(conexionEsquemaSwt.getEsquema())==false){
                                    resultado=-2;
                                    error_app="Error en Base de Datos Engine: El TableSpace esta asociado a otro esquema" ;
                                }


                            }
                            else if (opcion==2){
                                if(UsuarioTableSpace.equalsIgnoreCase(conexionEsquemaLlaves.getEsquema())==false){
                                    resultado=-2;
                                    error_app="Error en Base de Datos Seguridad: El TableSpace esta asociado a otro esquema" ;
                                }
                            }  
                        }//Fin de if(UsuarioTableSpace!=null)
                        
                    }//Fin de  if(resultado!=-2){
                }//Fin de if(resultado!=-2 && resultado>0)
                
                if(resultado!=2 & resultado==0){
                    vsql=   " select count(1) from dba_roles  ";
                    vsql+=  "where ";
                    vsql+=  "      upper(role)=upper(?) ";   
                    
                    parametros.clear();
                    
                    if(opcion==0)
                        parametroDato= new parametrosDatos(1,conexionEsquemaSwt.getNombreRol(),tipoDatoValor.String);
                    else if(opcion==2)
                        parametroDato= new parametrosDatos(1,conexionEsquemaLlaves.getNombreRol(),tipoDatoValor.String);
                    
                    parametros.add(parametroDato);
                     
                    rs=oconexion.getResultado(vsql, parametros);
                    
                    error_app= oconexion.getMensajeError();
                  
                    if (error_app!=null )
                        resultado=-2;
                    
                    if(resultado!=-2){
                        while(rs.next()){
                            resultado=rs.getInt(1); 
                        }
                        
                        rs.close();          
                    }//Fin de if(resultado!=-2)
                    
                }//Fin de  if(resultado!=2 & resultado==0)
               
                if(resultado!=-2 && resultado==1){
                    vsql="select granted_role  ";
                    vsql+="from dba_role_privs ";
                    vsql+="where ";
                    vsql+=  "     upper(grantee) =upper(?) ";  
                    
                    parametros.clear();
                    
                    if(opcion==0)
                        parametroDato= new parametrosDatos(1,conexionEsquemaSwt.getEsquema(),tipoDatoValor.String);
                    else if(opcion==2)
                        parametroDato= new parametrosDatos(1,conexionEsquemaLlaves.getEsquema(),tipoDatoValor.String);
                     
                    parametros.add(parametroDato);
                     
                    rs=oconexion.getResultado(vsql, parametros);
                    
                    UsuarioTableSpace=null;
                    error_app= oconexion.getMensajeError();
                  
                    if (error_app!=null )
                        resultado=-2;
                    
                    if(resultado!=-2){
                        
                        while(rs.next()){
                            UsuarioTableSpace=rs.getString(1); 
                            
                            //Si hay coincidencia
                             if(opcion==0){
                                if(UsuarioTableSpace.equalsIgnoreCase(conexionEsquemaSwt.getNombreRol())==true)
                                   break ;
                            }
                            else if(opcion==2){
                                if(UsuarioTableSpace.equalsIgnoreCase(conexionEsquemaLlaves.getNombreRol())==true)
                                    break;
                                
                            }    
                        }
                        
                        rs.close();
                        
                        if(UsuarioTableSpace!=null){
                            
                            if(opcion==0){
                                if(UsuarioTableSpace.equalsIgnoreCase(conexionEsquemaSwt.getNombreRol())==false){
                                    resultado=-2;
                                    error_app="Error en Base de Datos Engine: El rol de usuario no esta asociado al esquema";
                                } 
                            }
                            else if(opcion==2){
                                if(UsuarioTableSpace.equalsIgnoreCase(conexionEsquemaLlaves.getNombreRol())==false){
                                    resultado=-2;
                                    error_app="Error en Base de Datos de Seguridad: El rol de usuario no esta asociado al esquema";
                                } 
                            }
                        }//Fin de if(UsuarioTableSpace!=null)
                        
                        
                    }//Fin de if(resultado!=-2)    
                }//Fin de if(resultado!=-2 && resultado==1)
                
            }//Fin de if(opcion==0 || opcion==2)
            
            if(opcion==4){ //Usuario de Base de Datos Admin Cliente
                vsql=   "select username,default_tablespace from dba_users ";
                vsql+=  "where ";
                vsql+=  "     username=upper(?) ";

                parametros=new ArrayList();

                parametroDato= new parametrosDatos(1,this.conexionEsquemaClientes.getEsquema(),tipoDatoValor.String);
                parametros.add(parametroDato);

                rs= oconexion.getResultado(vsql, parametros);
                
                error_app= oconexion.getMensajeError();

                if (error_app!=null && error_app.equals("")==false)
                    resultado=-2;  
                
                if (resultado!=-2){
                    
                    if (rs!=null){

                        while(rs.next()){
                            UsuarioTableSpace=rs.getString(2); 
                        }

                        if(UsuarioTableSpace!=null ){

                            resultado=1; 
                            
                           // if (opcion==0) //Esquema de Base de Datos de Engine
                            if( UsuarioTableSpace.equalsIgnoreCase(conexionEsquemaClientes.getNombreTableSpace())==false){
                                error_app="Error en Base de Datos Cliente: El nombre del TableSpace no es el correcto";
                                resultado=-2;
                            }
                        }
                        else if (UsuarioTableSpace==null)
                            resultado=0; //Esquema de Base de Datos Cliente no existe

                        rs.close();
                    }//Fin de if (rs!=null)

                }//Fin de if (resultado!=-2)
                
                
                if (resultado!=-2 && resultado==0){ //Si el Esquema no existe
                    vsql=   "select count(1) from dba_tablespaces ";
                    vsql+=  "where ";
                    vsql+=  "     upper(tablespace_name)=upper(?)";

                    parametros.clear();

                    parametroDato= new parametrosDatos(1,conexionEsquemaClientes.getNombreTableSpace(),tipoDatoValor.String);
                  
                    parametros.add(parametroDato);

                    rs=oconexion.getResultado(vsql, parametros);

                    error_app= oconexion.getMensajeError();

                    if (error_app!=null )
                        resultado=-2;
                    
                    if(resultado!=-2){
                    
                         while(rs.next()){
                            resultado=rs.getInt(1); 
                        }  
                         
                        if(resultado==0){
                            resultado=-2;
                            error_app="Error en Base de Datos Cliente: El tablespace ingresado no ha sido creado";
                        }
                        else 
                            resultado=0;

                        rs.close();
                    }//Fin de  if(resultado!=-2)      
                }//Fin de  if (resultado!=-2 && resultado==0)    
                
                
                if(resultado!=-2 && resultado==0){ //Verificae si el TableSpace esta asociado a otro usuario
                    vsql=   "select username,default_tablespace from dba_users ";
                    vsql+=  "where ";
                    vsql+=  "     default_tablespace=upper(?) ";   
                    
                    parametros.clear();
                     
                    parametroDato= new parametrosDatos(1,conexionEsquemaClientes.getNombreTableSpace(),tipoDatoValor.String);
                   
                    parametros.add(parametroDato);
                     
                    rs=oconexion.getResultado(vsql, parametros);

                    error_app= oconexion.getMensajeError();
                    
                    UsuarioTableSpace=null;
                    
                    if (error_app!=null )
                        resultado=-2;
                     
                    if(resultado!=-2){    
                        while(rs.next()){
                            UsuarioTableSpace=rs.getString(1); 
                        }
                        
                        rs.close();
                       
                        if(UsuarioTableSpace!=null){
                            if(UsuarioTableSpace.equalsIgnoreCase(conexionEsquemaClientes.getEsquema())==false){
                                resultado=-2;
                                error_app="Error en Base de Datos Cliente: El TableSpace esta asociado a otro esquema" ;
                            }
                        }
                        
                    }//Fin de  if(resultado!=-2){
                }//Fin de if(resultado!=-2 && resultado>0)
                
                
                if(resultado!=2 & resultado==0){
                    vsql=   " select count(1) from dba_roles  ";
                    vsql+=  "where ";
                    vsql+=  "      upper(role)=upper(?) ";   
                    
                    parametros.clear();
                    
                    parametroDato= new parametrosDatos(1,this.conexionEsquemaClientes.getNombreRol(),tipoDatoValor.String);
                    
                    parametros.add(parametroDato); 
                    rs=oconexion.getResultado(vsql, parametros);
                    
                    error_app= oconexion.getMensajeError();
                    
                    if (error_app!=null )
                        resultado=-2;
                    
                    if(resultado!=-2){
                        while(rs.next()){
                            resultado=rs.getInt(1); 
                        }
                        
                        rs.close();
                    }//Fin de if(resultado!=-2)    
                }//Fin de if(resultado!=2 & resultado==0)
                
                
                if(resultado!=-2 && resultado==1){
                    vsql="select granted_role  ";
                    vsql+="from dba_role_privs ";
                    vsql+="where ";
                    vsql+=  "     upper(grantee) =upper(?) ";  
                    
                    parametros.clear();
                   
                    parametroDato= new parametrosDatos(1,conexionEsquemaClientes.getEsquema(),tipoDatoValor.String);
                    parametros.add(parametroDato);
                     
                    rs=oconexion.getResultado(vsql, parametros);
                    
                    UsuarioTableSpace=null;
                    error_app= oconexion.getMensajeError();
                  
                    if (error_app!=null )
                        resultado=-2;
                    
                    if(resultado!=-2){
                        while(rs.next()){
                            UsuarioTableSpace=rs.getString(1); 
                            
                            //Si hay coincidencia
                            if (UsuarioTableSpace.equalsIgnoreCase(conexionEsquemaClientes.getNombreRol())==true)
                                break;
                            
                        }
                        
                        rs.close();
                        
                        if(UsuarioTableSpace!=null){
                            if(UsuarioTableSpace.equalsIgnoreCase(conexionEsquemaClientes.getNombreRol())==false){
                                resultado=-2;
                                error_app="Error en Base de Datos Cliente: El rol de usuario no esta asociado al esquema";
                            } 
                        }//Fin de if(UsuarioTableSpace!=null)
                    }//Fin de if(resultado!=-2)    
                }//Fin de if(resultado!=-2 && resultado==1)
                
            }//Fin de  if(opcion==4)
            
            if(opcion==6){ //Usuario de Base de Datos Vault
                vsql=   "select username,default_tablespace from dba_users ";
                vsql+=  "where ";
                vsql+=  "     username=upper(?) ";

                parametros=new ArrayList();

                parametroDato= new parametrosDatos(1,conexionEsquemaDatos.getEsquema(),tipoDatoValor.String);
                parametros.add(parametroDato);

                rs= oconexion.getResultado(vsql, parametros);
                
                error_app= oconexion.getMensajeError();
                
                if (error_app!=null && error_app.equals("")==false)
                    resultado=-2;  
                
                if (resultado!=-2){
                    
                    if (rs!=null){

                        while(rs.next()){
                            UsuarioTableSpace=rs.getString(2); 
                        }

                        if(UsuarioTableSpace!=null ){
                            resultado=1; 
                            
                           // if (opcion==0) //Esquema de Base de Datos de Engine
                            if( UsuarioTableSpace.equalsIgnoreCase( this.conexionEsquemaDatos.getNombreTableSpace())==false){
                                error_app="Error en Base de Datos Vault: El nombre del TableSpace no es el correcto";
                                resultado=-2;
                            }
                        }
                        else if (UsuarioTableSpace==null)
                            resultado=0; //Esquema de Base de Datos Cliente no existe

                        rs.close();
                    }//Fin de if (rs!=null)

                }//Fin de if (resultado!=-2)
                
                if (resultado!=-2 && resultado==0){ //Si el Esquema no existe
                    vsql=   "select count(1) from dba_tablespaces ";
                    vsql+=  "where ";
                    vsql+=  "     upper(tablespace_name)=upper(?)";

                    parametros.clear();

                    parametroDato= new parametrosDatos(1,this.conexionEsquemaDatos.getNombreTableSpace(),tipoDatoValor.String);
                  
                    parametros.add(parametroDato);

                    rs=oconexion.getResultado(vsql, parametros);

                    error_app= oconexion.getMensajeError();

                    if (error_app!=null && error_app.equals("")==false)
                        resultado=-2;
                    
                    if(resultado!=-2){
                    
                         while(rs.next()){
                            resultado=rs.getInt(1); 
                        }  
                         
                        if(resultado==0){
                            resultado=-2;
                            error_app="Error en Base de Datos Vault: El tablespace ingresado no ha sido creado";
                        }
                        else 
                            resultado=0;

                        rs.close();
                    }//Fin de  if(resultado!=-2)      
                }//Fin de  if (resultado!=-2 && resultado==0)    
                
                
                if(resultado!=-2 && resultado==0){ //Verificae si el TableSpace esta asociado a otro usuario
                    vsql=   "select username,default_tablespace from dba_users ";
                    vsql+=  "where ";
                    vsql+=  "     default_tablespace=upper(?) ";   
                    
                    parametros.clear();
                     
                    parametroDato= new parametrosDatos(1,this.conexionEsquemaDatos.getNombreTableSpace(),tipoDatoValor.String);
                   
                    parametros.add(parametroDato);
                     
                    rs=oconexion.getResultado(vsql, parametros);

                    error_app= oconexion.getMensajeError();
                    
                    UsuarioTableSpace=null;
                    
                    if (error_app!=null && error_app.equals("")==false)
                        resultado=-2;
                     
                    if(resultado!=-2){
                        
                        while(rs.next()){
                            UsuarioTableSpace=rs.getString(1); 
                        }
                        
                        rs.close();
                        
                        if(UsuarioTableSpace!=null && UsuarioTableSpace.equalsIgnoreCase(this.conexionEsquemaDatos.getEsquema())==false){
                            resultado=-2;
                            error_app="Error en Base de Datos Vault: El TableSpace esta asociado a otro esquema" ;
                        }
                    }//Fin de  if(resultado!=-2){
                }//Fin de if(resultado!=-2 && resultado>0)   
            
                
                if(resultado!=2 & resultado==0){
                    vsql=   " select count(1) from dba_roles  ";
                    vsql+=  "where ";
                    vsql+=  "      upper(role)=upper(?) ";   
                    
                    parametros.clear();
                    
                    parametroDato= new parametrosDatos(1,conexionEsquemaDatos.getNombreRol(),tipoDatoValor.String);
                    
                    parametros.add(parametroDato); 
                    rs=oconexion.getResultado(vsql, parametros);
                    
                    error_app= oconexion.getMensajeError();
                    
                    if (error_app!=null)
                        resultado=-2;
                    
                    if(resultado!=-2){
                        while(rs.next()){
                            resultado=rs.getInt(1); 
                        }
                        
                        rs.close();
                    }//Fin de if(resultado!=-2)    
                }//Fin de if(resultado!=2 & resultado==0)
                
                
                if(resultado!=-2 && resultado==1){
                    vsql="select granted_role  ";
                    vsql+="from dba_role_privs ";
                    vsql+="where ";
                    vsql+=  "     upper(grantee) =upper(?) ";  
                    
                    parametros.clear();
                   
                    parametroDato= new parametrosDatos(1,conexionEsquemaDatos.getEsquema(),tipoDatoValor.String);
                    parametros.add(parametroDato);
                     
                    rs=oconexion.getResultado(vsql, parametros);
                    
                    UsuarioTableSpace=null;
                    error_app= oconexion.getMensajeError();
                  
                    if (error_app!=null )
                        resultado=-2;
                    
                    if(resultado!=-2){
                        while(rs.next()){
                            UsuarioTableSpace=rs.getString(1); 
                            
                            if (UsuarioTableSpace.equalsIgnoreCase(conexionEsquemaDatos.getNombreRol())==true)
                                break;
                            
                        }
                        
                        rs.close();
                        
                        if(UsuarioTableSpace!=null){
                            if(UsuarioTableSpace.equalsIgnoreCase(conexionEsquemaDatos.getNombreRol())==false){
                                resultado=-2;
                                error_app="Error en Base de Datos Vault: El rol de usuario no esta asociado al esquema";
                            } 
                        }//Fin de if(UsuarioTableSpace!=null)
                    }//Fin de if(resultado!=-2)    
                }//Fin de if(resultado!=-2 && resultado==1)   
            }//Fin de if(opcion==6){ //Usuario de Base de Datos Vault
                
            if(oconexion!=null)
                oconexion.cerrar(); 
        }//Fin de if (resultado!=-2){
        
        return resultado;
    }
    
    private void waitFor (int iMillis) {
        try {
            Thread.sleep(iMillis);
              
        }
        catch (InterruptedException ex) {
            System.err.println(ex);
        }
    } // End of Method: waitFor()
    
    private int procesar(){
        int resultado=0;
        boolean flag_comando=false;
        String sqlPath=null;
        String sqlCmd = "sqlplus";
        String fileName=null;
        String cadenaConexion=null;
        String line=null;
        
        icount++;
          
        for(ArchivoComandos oarchivo:listaArchivoComandos ){    
            flag_comando=false;
            
            ConexionBD oconexionbd;
            
            oconexionbd= new ConexionBD(oarchivo.getParametrosConexion() );
            
            if (oarchivo.getEsquema().equalsIgnoreCase("ADMINBASESWITCH")==true || oarchivo.getEsquema().equalsIgnoreCase("ESQUEMABASESWITCH")==true)
                publish("Procesando scripts de instalación Base Engine " );
            
            if (oarchivo.getEsquema().equalsIgnoreCase("ADMINBASELLAVES")==true || oarchivo.getEsquema().equalsIgnoreCase("ESQUEMABASELLAVES")==true)
                publish("Procesando scripts de instalación Base Seguridad" );
             
            if (oarchivo.getEsquema().equalsIgnoreCase("ADMINBASECUENTA")==true || oarchivo.getEsquema().equalsIgnoreCase("ESQUEMABASECUENTA")==true)
                publish("Procesando scripts de instalación Base Cliente" );
            
            if(oarchivo.getEsquema().equalsIgnoreCase("ADMINBASEDATOS")==true || oarchivo.getEsquema().equalsIgnoreCase("ESQUEMABASEDATOS")==true)
                publish("Procesando scripts de instalación Base Vault" );
             
            if (oarchivo.getfilename().equalsIgnoreCase("script_transaccional_01")==true || oarchivo.getfilename().equalsIgnoreCase("script_sinonimos")==true || oarchivo.getfilename().equalsIgnoreCase("script_cuenta_01")==true )
                flag_comando=true;
           
            setProgress((icount * 100) /limite);
            
            if (flag_comando==false){ //Si no llama a ningun comando
            
                if(oconexionbd.isConectado()){
                    resultado=oconexionbd.ejecutarSql(oarchivo.getContenidoArchivo()); 
                    error_app= oconexionbd.getMensajeError();

                    if(resultado!=-2){
                       oconexionbd.confirmar();
                       error_app=  oconexionbd.getMensajeError();
                    }

                    oconexionbd.cerrar();

                    if(error_app!=null){
                        resultado=-2;
                        error_app= "Error en Base de Datos al procesar el archivo " + oarchivo.getfilename() + ": " + error_app;
                        break;
                    }     
                }
                else {
                    resultado =-2;
                    error_app="Error en Base de Datos al procesar el archivo " + oarchivo.getfilename() + ": " + oconexionbd.getMensajeError();
                    break;
                }//Fin de if(oconexionbd.isConectado()) 
            
            }
            else {//Llamada a Shell de comandos
                sqlPath=System.getProperties().getProperty("user.dir") + File.separator + "File" + File.separator;
                fileName=  oarchivo.getfilename() + ".sql";
                
                if(oconexionbd.isConectado()){
                    oconexionbd.cerrar();
                    
                    cadenaConexion="@"   +  "(DESCRIPTION=(ADDRESS=(PROTOCOL=" ;
                    
                    if (oarchivo.getParametrosConexion().getTipoConexion()==0)
                       cadenaConexion= cadenaConexion + "TCPS";
                    else
                        cadenaConexion= cadenaConexion + "TCP";
                    
                    cadenaConexion= cadenaConexion + ")(HOST=" + oarchivo.getParametrosConexion().getServidorBaseDatos() +
                    ")(PORT=" + oarchivo.getParametrosConexion().getPuertoBase() + (oarchivo.getParametrosConexion().getTipoConexion()==0 ? "))(CONNECT_DATA=(SERVICE_NAME=" : "))(CONNECT_DATA=(SID=")    + 
                    oarchivo.getParametrosConexion().getServiceName() + ")))";

                    ProcessBuilder pb=new ProcessBuilder(sqlCmd , oarchivo.getParametrosConexion().getEsquema() + "/" + oarchivo.getParametrosConexion().getClaveBaseDatos() + cadenaConexion,"@" + sqlPath + fileName );
                    
//                    Map<String, String> env = pb.environment();  
//                    env.put("VAR3", cadenaConexion);
//                    env.put("VAR4", "@" + sqlPath + fileName);
////                    
//                    pb.directory(new File(sqlPath));  
//                    
//                    pb.redirectErrorStream(true);  
                    
                    try{
                    
                        Process p = pb.start();  

                        BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));  
                        BufferedReader bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));  
  
                        while ((line = bri.readLine()) != null) {  
                            System.out.println(line);  
                        }              
                        
                        while ((line = bre.readLine()) != null) {  
                            System.out.println(line);  
                        }  
                        bre.close();  
                        
                        System.out.println("\n\n\n");
                        System.out.println("Done.");  
                        
                        bri.close();  
                    }
                    
                    catch(Exception err){
                        error_app= err.getMessage();
                        resultado=-2;
                        
                        break;
                    }    
                }
            }//Fin de if (flag_comando==false)
            
            icount++;
                 
            waitFor(250);
            
        }//Fin de for(ArchivoComandos oarchivo:listaArchivoComandos )
        
      
        return resultado;
    }
    
    private int validarProcesoSQL(){
        int resultado=0;
        boolean flagvalidar= false;
        
        try{
        
            icount++;
            setProgress((icount * 100) /limite);

            publish("Verifcando datos generales de entrada");

            if (listaArchivoComandos==null){
                resultado=-2;
                error_app="No existe ningún archivo a procesar";
            }

            //Verificar que el usuario Administrador y Esquema del Engine no sean iguales
            if(resultado!=-2){
                if (this.conexionAdminClientes!=null)
                    if (this.conexionAdminSwt.getEsquema().equalsIgnoreCase(this.conexionEsquemaSwt.getEsquema())==true){
                        resultado=-2;
                        error_app="El nombre de usuario de Base de Datos de Engine no puede ser el mismo usuario Administrador";
                    }
                
            }

             //Verificar que el usario Administrador y Esquema de Seguridad no sean iguales
            if(resultado!=-2){
                if (this.conexionAdminClientes!=null)
                    if (this.conexionAdminSwt.getEsquema().equalsIgnoreCase(this.conexionEsquemaLlaves.getEsquema())==true){
                        resultado=-2;
                        error_app="El nombre de usuario de Base de Datos de Seguridad no puede ser el mismo usuario Administrador";
                    }
            }

            if(resultado!=-2){
                if(conexionEsquemaSwt.getNombreBaseDatos().equalsIgnoreCase(this.conexionEsquemaLlaves.getNombreBaseDatos())){
                    resultado=-2;
                    error_app="El nombre de Base de Datos Engine no puede ser el mismo de la Base de Datos Seguridad";
                }
            }
            
            if(resultado!=-2){
                if(conexionEsquemaSwt.getNombreBaseDatos().equalsIgnoreCase(this.conexionAdminSwt.getNombreBaseDatos())){
                    resultado=-2;
                    error_app="El nombre de Base de Datos Engine no esta correcto. Verifique su ingreso";
                }
            }

            if (conexionEsquemaClientes!=null){

                if (resultado !=-2){
                    if(conexionEsquemaClientes.getEsquema().equalsIgnoreCase(this.conexionEsquemaSwt.getEsquema())==true){
                        resultado=-2;
                        error_app="El nombre de usuario de Base Cliente no puede ser igual al nombre de usuario de Base Engine";
                    }
                }
                
                if(resultado!=-2){
                    if(conexionEsquemaClientes.getNombreBaseDatos().equalsIgnoreCase(this.conexionAdminClientes.getNombreBaseDatos())){
                        resultado=-2;
                        error_app="El nombre de Base de Datos Cliente no esta correcto. Verifique su ingreso";
                    }
                }

                if (resultado!=-2){
                    if (conexionEsquemaClientes.getEsquema().equalsIgnoreCase(this.conexionAdminClientes.getEsquema())==true){
                        resultado=-2;
                        error_app="El nombre de usuario de Base de Cliente no puede ser el mismo del administrador de Base de Datos";
                    }
                }


                if(resultado!=-2){
                    if(conexionEsquemaClientes.getNombreBaseDatos().equalsIgnoreCase(conexionEsquemaSwt.getNombreBaseDatos())){
                        resultado=-2;
                        error_app="El nombre de Base de Datos de Cliente no puede ser el mismo de la Base de Datos Engine";
                    }
                }

                if(resultado!=-2){
                    if(conexionEsquemaClientes.getEsquema().equalsIgnoreCase(this.conexionEsquemaLlaves.getEsquema())){
                        resultado=-2;
                        error_app="El nombre de usuario de Base de Cliente no puede ser el mismo de la Base de Datos Seguridad";
                    }
                }

                if(resultado!=-2){
                    if(conexionEsquemaClientes.getNombreBaseDatos().equalsIgnoreCase(this.conexionEsquemaLlaves.getNombreBaseDatos())){
                        resultado=-2;
                        error_app="El nombre de Base de Datos Cliente no puede ser el mismo de la Base de Datos Seguridad";
                    }
                }
            }//Fin de if (this.conexionEsquemaClientes!=null)


            if(conexionEsquemaDatos!=null && conexionEsquemaClientes!=null ){

                if (resultado !=-2){
                    if(conexionEsquemaDatos.getEsquema().equalsIgnoreCase(this.conexionAdminDatos.getEsquema())==true){
                        resultado=-2;
                        error_app="El nombre de usuario de de Base de Datos Vault no puede ser igual al usuario Administrador";
                    }
                }

                if(resultado!=-2){
                    if(conexionEsquemaDatos.getEsquema().equalsIgnoreCase(this.conexionEsquemaClientes.getEsquema())){
                        resultado=-2;
                        error_app="El nombre de usuario Base de Vault no puede ser el mismo de la Base de Datos Cliente";
                    }
                }
                
                if(resultado!=-2){
                    if(conexionEsquemaDatos.getNombreBaseDatos().equalsIgnoreCase(this.conexionAdminDatos.getNombreBaseDatos())){
                        resultado=-2;
                        error_app="El nombre de Base de Datos Vault no es correcto. Verifique su ingreso";
                    }
                }

                if(resultado!=-2){
                    if(conexionEsquemaDatos.getNombreBaseDatos().equalsIgnoreCase(this.conexionEsquemaClientes.getNombreBaseDatos())){
                        resultado=-2;
                        error_app="El nombre de Base de Datos Vault no puede ser el mismo de la Base de Datos Cliente";
                    }
                }
                
            }//Fin de if(conexionEsquemaDatos!=null && conexionEsquemaClientes!=null )

            waitFor(250);

            if(resultado!=-2){
                icount++;
                setProgress((icount * 100) /limite);

                publish("Testeando existencia de Base de Datos Engine");

                resultado=validarSQL(0); //Validar conexion sa Base Switch
                
                if(conexionAdminSwt.getEsquema().equalsIgnoreCase(conexionEsquemaSwt.getEsquema()) )
                    flagvalidar=false;
                else
                  flagvalidar=true; 

                waitFor(250);
            }
            
            //Ejecutar script de Creación de Base de Datos Engine
            if(resultado!=-2 && resultado==0){
            
                publish("Creando Base de Datos Engine"); 
                
                resultado=crearBaseDatos(conexionEsquemaSwt.getNombreBaseDatos(),rutaBaseEngine,
                conexionAdminSwt);
                
                waitFor(250);     
            }
            
            icount++;
            setProgress((icount * 100) /limite);
            
            if(resultado!=-2 && resultado==1){
                
                publish("Validando autenticación de usuario de Base de Datos Engine");
                
                resultado=validarSQL(1); //Validar conexion sa Base Switch

                waitFor(250);   
  
            }
           
           if(resultado!=-2){
               
               publish("Verificando login de acceso Base de Datos Engine"); 
               
               resultado= crearLoginSql(conexionEsquemaSwt.getEsquema(),conexionEsquemaSwt.getNombreBaseDatos(),
               conexionAdminSwt);
               
               waitFor(250);   
            }
           
           
            if(resultado!=-2 && flagvalidar==true){
                publish("Verificando usuario de Base de Datos Engine");
                
                resultado=crearUserSql(conexionEsquemaSwt.getEsquema(),conexionEsquemaSwt.getNombreBaseDatos(),
                conexionAdminSwt);
                
                conexionAdminSwt.setNombreBaseDatos("master");
            }
            
            flagvalidar=false;
            
            if(resultado!=-2){
                icount++;
                setProgress((icount * 100) /limite);

                publish("Testeando existencia de Base de Seguridad");

                resultado=validarSQL(2); //Validar conexion sa Base Switch

                waitFor(250);
                
                if(conexionAdminLlaves.getEsquema().equalsIgnoreCase(conexionEsquemaLlaves.getEsquema()))
                    flagvalidar=false;
                else
                    flagvalidar=true;
            }
            
             //Ejecutar script de Creación de Base de Datos Engine
            if(resultado!=-2 && resultado==0){
            
                publish("Creando Base de Datos de Segurad"); 
                
                resultado=crearBaseDatos(conexionEsquemaLlaves.getNombreBaseDatos(),this.rutaBaseLlaves,
                conexionAdminLlaves);
               
                waitFor(250);
            }
            
            icount++;
            setProgress((icount * 100) /limite);
            
            if(resultado!=-2 && resultado==1){
                publish("Validando autenticacion Base de Datos de Segurad"); 
                 
                resultado=validarSQL(3); //Validar conexion sa Base Seguridad

                waitFor(250);
            }
            
           
            
            if(resultado!=-2){
               
               publish("Verificando login de acceso Base de Datos de Seguridad"); 
               
               resultado= crearLoginSql(conexionEsquemaLlaves.getEsquema(),conexionEsquemaLlaves.getNombreBaseDatos(),
               conexionAdminLlaves);
               
               waitFor(250);   
            }
            
            if(resultado!=-2 && flagvalidar==true){
                publish("Verificando usuario de Base de Datos de Seguridad");
                
                resultado=crearUserSql(conexionEsquemaLlaves.getEsquema(),conexionEsquemaLlaves.getNombreBaseDatos(),
                conexionAdminLlaves);
                
                conexionAdminLlaves.setNombreBaseDatos("master");
            }
            
           
            if(resultado!=-2){
                
                if(conexionEsquemaClientes!=null){
            
                    icount++;
                    setProgress((icount * 100) /limite);

                    publish("Testeando existenvia de Base de Datos Cliente");
                    resultado=validarSQL(4); //Validar conexion datos de esquema de cliente
                    waitFor(250);
                   
               
                    //Ejecutar script de Creación de Base de Datos Cliente
                    if(resultado!=-2 && resultado==0){
                        publish("Creando Base de Datos de Cliente"); 
                        resultado=crearBaseDatos(conexionEsquemaClientes.getNombreBaseDatos(),rutaBaseEngine,
                        conexionAdminClientes);
                        waitFor(250);
                    }
                    
                    
                    icount++;
                    setProgress((icount * 100) /limite);
                    
                    if(resultado!=-2 && resultado==1){
                        publish("Validando autenticacion Base de Datos de Clientes"); 
                 
                        resultado=validarSQL(5); //Validar conexion sa Base Clientes

                        waitFor(250);
                    }
                    
                    
                    if(resultado!=-2){
                        publish("Verificando login de acceso Base de Datos de Clientes"); 
                        resultado= crearLoginSql(conexionEsquemaClientes.getEsquema(),conexionEsquemaClientes.getNombreBaseDatos(),
                        conexionAdminClientes);
               
                        waitFor(250);   
                    }
                    
                    if(resultado!=-2 /*&& flagvalidar==true*/){
                        publish("Verificando usuario de Base de Datos de Datos de Clientes");
                
                        resultado=crearUserSql(conexionEsquemaClientes.getEsquema(),conexionEsquemaClientes.getNombreBaseDatos(),
                        conexionAdminClientes);
                
                        conexionAdminClientes.setNombreBaseDatos("master");
                        waitFor(250);   
                    }
                    
                    if(resultado!=-2){
                        publish("Creando usuario enginecuenta en Base de Datos Engine");
                        resultado=crearUserSqlRol("enginecuenta",null,conexionEsquemaClientes.getEsquema() ,0,
                        conexionEsquemaSwt);
                        waitFor(250);   
                    }
                    
                    if(resultado!=-2){
                        publish("Creando rol control_cuenta en Base de Datos Engine");
                        resultado=crearUserSqlRol("enginecuenta","control_cuenta",null ,1,conexionEsquemaSwt);
                        waitFor(250);   
                    }
                    
                }//Fin de  if(conexionEsquemaClientes!=null)
                
            }//Fin de if(resultado!=-2)
            
            
            if(resultado!=-2){
                if(conexionEsquemaClientes!=null && conexionAdminDatos!=null){
                  
                    icount++;
                    setProgress((icount * 100) /limite);

                    publish("Testeando existenvia de Base de Datos Vault");
                    resultado=validarSQL(6); //Validar conexion datos de esquema de cliente
                    waitFor(250);
                    
                     //Ejecutar script de Creación de Base de Datos Vault
                    if(resultado!=-2 && resultado==0){
                        publish("Creando Base de Datos de Vault"); 
                        resultado=crearBaseDatos(conexionEsquemaDatos.getNombreBaseDatos(),rutaBaseEngine,
                        conexionAdminDatos);
                        waitFor(250);
                    }
                    
                    icount++;
                    setProgress((icount * 100) /limite);
                    
                    if(resultado!=-2 && resultado==1){
                        publish("Validando autenticacion Base de Datos Vault"); 
                 
                        resultado=validarSQL(7); //Validar conexion de Base Vault

                        waitFor(250);
                        
                    }
                    
                    if(resultado!=-2){
                        publish("Verificando login de acceso Base de Datos Vault"); 
                        resultado= crearLoginSql(conexionEsquemaDatos.getEsquema(),conexionEsquemaDatos.getNombreBaseDatos(),
                        conexionAdminDatos);
               
                        waitFor(250);   
                    }
                    
                    if(resultado!=-2 /*&& flagvalidar==true*/){
                        publish("Verificando usuario de Base de Datos de Datos Vault");
                
                        resultado=crearUserSql(conexionEsquemaDatos.getEsquema(),conexionEsquemaDatos.getNombreBaseDatos(),
                        conexionAdminDatos);
                
                        conexionAdminDatos.setNombreBaseDatos("master");
                    }  
                    
                    
                    if(resultado!=-2){
                        publish("Creando usuario userengine_datos en Base de Datos Engine");
                        resultado=crearUserSqlRol("userengine_datos",null,conexionEsquemaDatos.getEsquema() ,0,
                        conexionEsquemaSwt);
                        waitFor(250);   
                    }
                    
                    if(resultado!=-2){
                        publish("Creando rol control_datos en Base de Datos Engine");
                        resultado=crearUserSqlRol("userengine_datos","control_datos",null ,1,conexionEsquemaSwt);
                        waitFor(250);   
                    }
                    
                                
                    if(resultado!=-2){
                        publish("Creando usuario userdatos en Base de Datos Clientes");
                        resultado=crearUserSqlRol("userdatos",null,conexionEsquemaDatos.getEsquema() ,0,
                        conexionEsquemaClientes);
                        waitFor(250);   
                    }
                    
                    if(resultado!=-2){
                        publish("Creando rol control_datos en Base de Datos Clientes");
                        resultado=crearUserSqlRol("userdatos","control_datos",null ,1,conexionEsquemaClientes);
                        waitFor(250);   
                    }
                    
                    if(resultado!=-2){
                        publish("Creando usuario userswitch en Base de Datos Clientes");
                        resultado=crearUserSqlRol("userswitch",null,conexionEsquemaSwt.getEsquema() ,0,
                        conexionEsquemaClientes);
                        waitFor(250);   
                    }
                    
                    if(resultado!=-2){
                        publish("Creando rol control_switch en Base de Datos Clientes");
                        resultado=crearUserSqlRol("userswitch","control_switch",null ,1,conexionEsquemaClientes);
                        waitFor(250);   
                    }
                    
                    waitFor(250); 
                }//Fin de if(conexionEsquemaCliente!=null && conexionAdminDatos!=null)  
                
            }//Fin de if(resultado!=-2)     
        }
        catch(SQLException ex){
             error_app="Ocurrio el siguiente error en la Base de Datos" +  ex.getMessage();
            resultado=-2;
        }
        
       return resultado;
    }
    
    private int validarProcesoOracle(){
        int resultado=0;
        
        try{
            
            icount++;
            setProgress((icount * 100) /limite);
            
            publish("Verifcando datos generales de entrada");
            
            if (listaArchivoComandos==null){
                resultado=-2;
                error_app="No existe ningún archivo a procesar";
            }
            
          
            //Verificar que el usuario Administrador y Esquema del Engine no sean iguales
            if(resultado!=-2)
                if (this.conexionAdminSwt.getEsquema().equalsIgnoreCase(this.conexionEsquemaSwt.getEsquema())==true){
                    resultado=-2;
                    error_app="El nombre del esquema de Base de Datos de Engine no puede ser el mismo usuario Administrador";
                }
            
            //Verificar que el usarui Administrador y Esquema de Seguridad no sean iguales
            if(resultado!=-2){
                if (this.conexionAdminSwt.getEsquema().equalsIgnoreCase(this.conexionEsquemaLlaves.getEsquema())==true){
                    resultado=-2;
                    error_app="El nombre del esquema de Base de Datos de Seguridad no puede ser el mismo usuario Administrador";
                }
            }
            
            if(resultado!=-2){
                if(conexionEsquemaSwt.getNombreRol().equalsIgnoreCase(this.conexionEsquemaLlaves.getNombreRol())==true){
                    resultado=-2;
                    error_app="El nombre del Rol de Usuario del Esquema del Engine debe ser diferente al del Esqumea de Seguridad";
                }
            }
            
            if(resultado!=-2){
                if(conexionEsquemaSwt.getNombreTableSpace().equalsIgnoreCase(conexionEsquemaLlaves.getNombreTableSpace())==true){
                    resultado=-2;
                    error_app="El nombre del Tablespace de Base de Datos Engine no puede ser el mismo de la Base de Datos Seguridad";
                }
            }
            
            if (this.conexionEsquemaClientes!=null){
                
                if (resultado !=-2){
                    if(conexionEsquemaClientes.getEsquema().equalsIgnoreCase(this.conexionEsquemaSwt.getEsquema())==true){
                        resultado=-2;
                        error_app="El nombre del esquema de Base Cliente no puede ser igual al nombre de esquema de Base Engine";
                    }
                }
                
                if (resultado!=-2){
                    if (conexionEsquemaClientes.getEsquema().equalsIgnoreCase(this.conexionAdminClientes.getEsquema())==true){
                        resultado=-2;
                        error_app="El nombre del esquema Base de Cliente no puede ser el mismo del administrador de Base de Datos";
                    }
                }
                
                
                if(resultado!=-2){
                    if(conexionEsquemaClientes.getNombreTableSpace().equalsIgnoreCase(conexionEsquemaSwt.getNombreTableSpace())){
                        resultado=-2;
                        error_app="El nombre del tableSpace Base Cliente no puede ser el mismo de la Base de Datos Engine";
                    }
                }
                
                if(resultado!=-2){
                    if(conexionEsquemaClientes.getEsquema().equalsIgnoreCase(this.conexionEsquemaLlaves.getEsquema())){
                        resultado=-2;
                        error_app="El nombre del esquema Base de Cliente no puede ser el mismo de la Base de Datos Seguridad";
                    }
                }
                
                if(resultado!=-2){
                    if(conexionEsquemaClientes.getNombreTableSpace().equalsIgnoreCase(this.conexionEsquemaLlaves.getNombreTableSpace())){
                        resultado=-2;
                        error_app="El nombre del tableSpace Base de Datos Cliente no puede ser el mismo de la Base de Datos Seguridad";
                    }
                }
                
                if(resultado!=-2){
                    if(conexionEsquemaClientes.getNombreRol().equalsIgnoreCase(this.conexionEsquemaSwt.getNombreRol())==true){
                        resultado=-2;
                        error_app="El nombre del rol de usuario del Esquema de Clientes debe ser diferente del Esquema del Engine";
                    }
                }
                
                if(resultado!=-2){
                     if(conexionEsquemaClientes.getNombreRol().equalsIgnoreCase(conexionEsquemaLlaves.getNombreRol())==true){
                        resultado=-2;
                        error_app="El nombre del rol de usuario del Esquema  Cliente debe ser diferente del Esquema de Seguridad";
                    }
                }
                
            }//Fin de if (this.conexionEsquemaClientes!=null)
          
            if(conexionEsquemaDatos!=null){
                
                if (resultado !=-2){
                    if(conexionEsquemaDatos.getEsquema().equalsIgnoreCase(this.conexionEsquemaSwt.getEsquema())==true){
                        resultado=-2;
                        error_app="El nombre del esquema de Base Vault no puede ser igual al nombre de esquema de Base Engine";
                    }
                }
                
                if (resultado!=-2){
                    if (conexionEsquemaDatos.getEsquema().equalsIgnoreCase(this.conexionAdminClientes.getEsquema())==true){
                        resultado=-2;
                        error_app="El nombre del esquema Base de Vaul no puede ser el mismo del administrador de Base de Datos";
                    }
                }
                
                if(resultado!=-2){
                    if(conexionEsquemaDatos.getNombreTableSpace().equalsIgnoreCase(conexionEsquemaSwt.getNombreTableSpace())){
                        resultado=-2;
                        error_app="El nombre del tableSpace Base Vault no puede ser el mismo de la Base de Datos Engine";
                    }
                }
                
                if(resultado!=-2){
                    if(conexionEsquemaDatos.getEsquema().equalsIgnoreCase(this.conexionEsquemaLlaves.getEsquema())){
                        resultado=-2;
                        error_app="El nombre del esquema Base de Vaul no puede ser el mismo de la Base de Datos Seguridad";
                    }
                }
                
                if(resultado!=-2){
                    if(conexionEsquemaDatos.getNombreTableSpace().equalsIgnoreCase(this.conexionEsquemaLlaves.getNombreTableSpace())){
                        resultado=-2;
                        error_app="El nombre del tableSpace Base de Datos Vaul no puede ser el mismo de la Base de Datos Seguridad";
                    }
                }
            }//Fin de if(conexionEsquemaDatos!=null)
            
            if(conexionEsquemaDatos!=null && conexionEsquemaClientes!=null ){
                
                if (resultado !=-2){
                    if(conexionEsquemaDatos.getEsquema().equalsIgnoreCase(this.conexionAdminDatos.getEsquema())==true){
                        resultado=-2;
                        error_app="El nombre del esquema de Base de Datos Vaul no puede ser igual al usuario Administrador";
                    }
                }
                
                if(resultado!=-2){
                    if(conexionEsquemaDatos.getEsquema().equalsIgnoreCase(this.conexionEsquemaClientes.getEsquema())){
                        resultado=-2;
                        error_app="El nombre del esquema Base de Vaul no puede ser el mismo de la Base de Datos Cliente";
                    }
                }
                
                if(resultado!=-2){
                    if(conexionEsquemaDatos.getNombreTableSpace().equalsIgnoreCase(this.conexionEsquemaClientes.getNombreTableSpace())){
                        resultado=-2;
                        error_app="El nombre del tableSpace Base de Datos Vaul no puede ser el mismo de la Base de Datos Cliente";
                    }
                }
                
                if(resultado!=-2){
                     if(conexionEsquemaClientes.getNombreRol().equalsIgnoreCase(conexionEsquemaDatos.getNombreRol())==true){
                        resultado=-2;
                        error_app="El nombre del rol de usuario del Esquema  Cliente debe ser diferente del Esquema Vault";
                    }
                }
                
                if(resultado!=-2){
                     if(conexionEsquemaSwt.getNombreRol().equalsIgnoreCase(conexionEsquemaDatos.getNombreRol())==true){
                        resultado=-2;
                        error_app="El nombre del rol de usuario del Esquema Engine debe ser diferente del Esquema Vault";
                    }
                }
                
                 if(resultado!=-2){
                     if(conexionEsquemaLlaves.getNombreRol().equalsIgnoreCase(conexionEsquemaDatos.getNombreRol())==true){
                        resultado=-2;
                        error_app="El nombre del rol de usuario del Esquema de Seguridad debe ser diferente del Esquema Vault";
                    }
                }        
            }//Fin de if(conexionEsquemaDatos!=null && conexionEsquemaClientes!=null )
            
          
            waitFor(250);
            

            if(resultado!=-2){
                icount++;
                setProgress((icount * 100) /limite);
                
                publish("Testeando conexion usuario Base Engine");
              
                resultado=validarOracle(0); //Validar conexion system Base Switch
                 
                waitFor(250);
            }
            
            
            
            icount++;
            setProgress((icount * 100) /limite);
            
            if (resultado!=-2 && resultado==1){
                
                publish("Testenado conexion de esquema Base Engine"); 
                resultado=validarOracle(1); //Validar conexion esquema Base Switch
               
            }
            
            waitFor(250);

            if (resultado!=-2 ){ 
                icount++;
                setProgress((icount * 100) /limite);
                
                publish("Testeando conexion de usuario Base Seguridad");
                    
                resultado=validarOracle(2); //Validar conexion system Base Llaves
                waitFor(250);
            }

            icount++;
            setProgress((icount * 100) /limite);
           
            if (resultado!=-2 && resultado==1){ 
                publish("Testeando conexion de esquema Base Seguridad");
                
                resultado=validarOracle(3); //Validar conexion esquema Base Llaves de Seguridad
            }
            
            waitFor(250);
            
            if(conexionEsquemaClientes!=null){
            
                if(resultado!=-2){
                    icount++;
                    setProgress((icount * 100) /limite);
                    
                    publish("Testeando conexión de usuario Base Cliente");
                    resultado=validarOracle(4); //Validar conexion datos de esquema de cliente
                    waitFor(250);
                }
                
                icount++;
                setProgress((icount * 100) /limite);
                
                if(resultado!=-2 &&  resultado==1){
                    publish("Testeando conexión de esquema de Base Cliente");
                    resultado=validarOracle(5); //Validar conexion autenticacion esquema cliente
                }
                
                waitFor(250);
            }//Fin de  if(conexionEsquemaClientes!=null)
           
            if(conexionEsquemaDatos!=null && conexionAdminDatos!=null){
                
                if(resultado!=-2){
                
                    icount++;
                    setProgress((icount * 100) /limite);
                    
                    publish("Testeando conexión de usuario Base Vault");
                    resultado=validarOracle(6); //Validar conexion autenticación Usuario Base Vault
                    waitFor(250);
                }
                
                icount++;
                setProgress((icount * 100) /limite);
                
                if(resultado!=-2 &&  resultado==1){
                    publish("Testeando conexión de esquema de Base Vault");
                    resultado=validarOracle(7); //Validar conexion autenticacion esquema cliente
                }
                
                waitFor(250);
            }
            
        }
        catch(SQLException ex){
            resultado=-2;
            error_app="Ocurrio el siguiente error en la Base de Datos" +  ex.getMessage();
        }
        
        return resultado;
    }
    
    private int crearBaseOracle(){
        int resultado=0;
        
        resultado=validarProcesoOracle();
        
        if (resultado!=-2){ //Procesar los archivos de Base de Datos
            resultado=procesar();
        }//Fin de if (resultado!=-2){
            
        return resultado;
    }//Fin de metodo  public int crearBase()
      
    protected int crearBaseSQL(){
        int resultado=0;
        
        resultado=validarProcesoSQL();
        
        if (resultado!=-2){ //Procesar los archivos de Base de Datos
            resultado=procesar();
        }//Fin de if (resultado!=-2){
        
        return resultado;
    }
    
}
