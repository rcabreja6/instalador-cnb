
/******************************************************************************/
//                       Clase deploymentLic Versión 20.
//Objetivo:        Porporcionar información de datos de licencia para generar Base de Datos
//Fecha Creación:  2019-03-02
//Responsable:     Sistemas
//Empresa:         Hiper S.A
/******************************************************************************/

package soft.deployment;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.File;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Properties;
import seg.Encripcion;

public class deploymentLic {
    
    private String mBaseDatos;
    private String mModulo;
    private String mErrorApp;
   
    private ArrayList <ArchivoComandos> listafiles;
    
    public ArrayList <ArchivoComandos> getListaFiles(){
        return listafiles;
    }
   
    public String MensajeError(){
        return mErrorApp;
    }
    
    public String getMotorBase(){
        return mBaseDatos;
    }
    
    public String getModulo(){
        return mModulo;
    }
    
    
    
    private String obtenerContenidoArchivo(String nombreArchivo){
        String contenidoArchivo="";
        File archivo;
        
        archivo= new File(System.getProperties().getProperty("user.dir") + File.separator +  "File" + File.separator + nombreArchivo);
         
        try{
            StringBuffer buffer;
            String contenidoTemporal;
            RandomAccessFile fichero ;
             
            //Verificar el archivo ConfigSec
            if (archivo.exists()){
                  
                 fichero= new RandomAccessFile(archivo,"r");
                 buffer=new StringBuffer();
                 
                while((contenidoTemporal=fichero.readLine())!=null){    
                    buffer.append(contenidoTemporal );
                }
                 
                fichero.close();      
                contenidoArchivo= buffer.toString();
                
                contenidoArchivo= Encripcion.desencriptar(contenidoArchivo).trim();
                
            }
            else {
                mErrorApp="Error al cargar información del sistema:  El archivo " + nombreArchivo + " no se encuentra disponible"; 
            }
        }
         catch(IOException ex){
            mErrorApp="Error al cargar información del sistema: " + ex.getLocalizedMessage();    
        }
        
        return contenidoArchivo;
    }
    
    private Properties obtenerConfig(String contenidoArchivo) throws IOException{
        Properties oconfig= null;
        
        if(contenidoArchivo!=null && contenidoArchivo.equalsIgnoreCase("")==false){
                
            //Paso de una Cadena a un BufferReader para lectura por linea
            InputStream is = new ByteArrayInputStream(contenidoArchivo.getBytes("UTF-8"));

            try (BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
                oconfig= new Properties();

                while((contenidoArchivo = reader.readLine()) != null)
                    oconfig.put( contenidoArchivo.split("=")[0] , contenidoArchivo.split("=")[1]);
            }
        }
        
        return oconfig;
    }
    
    private boolean obtener_infoConfigSec(){
        boolean resultado=true;
        String contenidoArchivo;    
        Properties configSec;
        Properties ConfigSql;
              
        try{   
            ArchivoComandos oarchivos;
             
            contenidoArchivo=obtenerContenidoArchivo("ConfigSec");
            configSec=obtenerConfig(contenidoArchivo);
            
            contenidoArchivo=obtenerContenidoArchivo("ConfigSql");
            ConfigSql=obtenerConfig(contenidoArchivo);  
            
            if(configSec!=null && ConfigSql!=null && mErrorApp==null){
                Enumeration<Object> keys = configSec.keys();
                
                listafiles= new ArrayList();
                
                while (keys.hasMoreElements()){
                    Object key = keys.nextElement();

                    oarchivos= new ArchivoComandos();
                    oarchivos.setSecuencia(Integer.parseInt(key.toString()));
                    oarchivos.setFileName(configSec.getProperty(key.toString())); 
                    oarchivos.setEsquema(ConfigSql.getProperty(configSec.getProperty(key.toString())));

                    if(oarchivos.getEsquema().equalsIgnoreCase("ESQUEMABASEDATOS") ||oarchivos.getEsquema().equalsIgnoreCase("ADMINBASEDATOS")){
                         
                        if(mModulo.equalsIgnoreCase("WALLET")) //Si tiene el Archivo Licencia se procesa
                            listafiles.add(oarchivos);  
                         else{
                            resultado=false;
            
                            mErrorApp="Error al cargar información del sistema: Información de licencia no válida" ;
                            break;
                         }
                    }
                    
                  
                    if(oarchivos.getEsquema().equalsIgnoreCase("ADMINBASECUENTA") || oarchivos.getEsquema().equalsIgnoreCase("ESQUEMABASECUENTA") ){

                        if(mModulo.equalsIgnoreCase("POSMOVIL")==true || mModulo.equalsIgnoreCase("WALLET") || mModulo.equalsIgnoreCase("CNB")==true) //Si tiene el Archivo Licencia se procesa
                            listafiles.add(oarchivos);
                        else {
                            resultado=false;
            
                            mErrorApp="Error al cargar información del sistema: Información de licencia no válida" ;
                            break;
                        }
                    }
                    
                    if(oarchivos.getEsquema().equalsIgnoreCase("ADMINBASESWITCH") || oarchivos.getEsquema().equalsIgnoreCase("ESQUEMABASESWITCH") ){
                        listafiles.add(oarchivos);  
                    }
                    
                    if(oarchivos.getEsquema().equalsIgnoreCase("ADMINBASELLAVES") || oarchivos.getEsquema().equalsIgnoreCase("ESQUEMABASELLAVES") ){
                        listafiles.add(oarchivos);  
                    }
                        
                }
                
                
                if (mModulo.equalsIgnoreCase("POSMOVIL")==true || mModulo.equalsIgnoreCase("WALLET")==true )
                    agregarArchivos("script_objetos_cuenta","ESQUEMABASECUENTA");

                listafiles.sort(new Comparator<ArchivoComandos>(){                   
                        @Override
                        public int compare(ArchivoComandos o1, ArchivoComandos o2) {

                            if (o1.getidsec()>o2.getidsec())
                                return 1;
                            else if (o1.getidsec()< o2.getidsec())
                                return -1;
                            else
                                return 0;
                        }
                    }
                );    
            
            }//Fin de if(configSec!=null && ConfigSql!=null && mErrorApp==null)
            
            
        }
        
        catch(IOException ex){
            resultado=false;
            
            mErrorApp="Error al cargar información del sistema: " + ex.getLocalizedMessage();    
        }
        
        
        
        return resultado;
    }
    
    
    private  void agregarArchivos(String archivo,String esquemaBase){
        File archivoadic;
        ArchivoComandos oarchivos;
        
        archivoadic= new File(System.getProperties().getProperty("user.dir") + File.separator + "File" + File.separator +  archivo);
          
         if (archivoadic.exists()){
            oarchivos= new ArchivoComandos();
            oarchivos.setSecuencia(listafiles.size()+1);
            oarchivos.setFileName(archivo);
            oarchivos.setEsquema(esquemaBase);
            listafiles.add(oarchivos);
        }
    }
    
    private boolean obtener_infolicencia(){
       
        boolean resultado=true;
     
        mErrorApp=null;
          
        try{
            String contenidoArchivo;
            contenidoArchivo=obtenerContenidoArchivo("Licencia");
            
            if(mErrorApp==null ){

                //Formato de Contenido de Archivo: "BASE=ORACLE@MODULO=POSMOVIL";
                String [] arreglo;
                arreglo=contenidoArchivo.split("@");
                mBaseDatos=arreglo[0].split("=")[1];
                mModulo= arreglo[1].split("=")[1] ;     
            }
            else
                 resultado=false;
        }
        
        catch(Exception ex){
            resultado=false;
            mErrorApp="Error al cargar información del sistema: " + ex.getLocalizedMessage();
        }
        
        return resultado;
    }
    
    public deploymentLic(){
        if(obtener_infolicencia()){
            obtener_infoConfigSec();
        }
    }
    
}
