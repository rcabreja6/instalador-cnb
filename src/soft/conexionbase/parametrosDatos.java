
package soft.conexionbase;



public class parametrosDatos {
    private int idParametro;
    private Object valorParametro;
    
    public enum tipoDatoValor{
        Entero,
        String,
        Double,
        Date,
        Float
    };
    
    tipoDatoValor tipoDato;
    
    public parametrosDatos(int idparammetro, Object valorParametro,tipoDatoValor tipo){
        this.idParametro=idparammetro;
        this.valorParametro=valorParametro;
        tipoDato=tipo;
    }
    
    public tipoDatoValor getTipoDato(){
        return tipoDato;
    }
    
    public int getidParametro(){
        return idParametro;
    }
    
    public Object getValorParametro(){
        return valorParametro;
    }
    
}
