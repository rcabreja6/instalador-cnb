
/******************************************************************************/
//                       Clase ConexionBD Versión 20.
//Objetivo:        Clase para interacturar con las Bases de Datos según Licencia de uso
//Fecha Creación:  2019-03-02
//Responsable:     Sistemas
//Empresa:         Hiper S.A
/******************************************************************************/

package soft.conexionbase;

import java.security.Security;
import soft.deployment.parametrosConexion;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.Properties;
import oracle.jdbc.OracleTypes;
import soft.conexionbase.parametrosDatos.tipoDatoValor;


public final class ConexionBD {
    private String error_app;
    
    private parametrosConexion parametrosBd;
    private boolean flagConectar=false;
    Connection oconexionbd=null;
    
    public String getMensajeError(){
        return error_app;
    }
    
    private void preparConexionOracle(parametrosConexion parametros){
        Properties props = new Properties();
        parametrosBd=parametros;
        String vcadenaConexion=null;
        
        
        if(parametros.getTipoConexion()==1){
            vcadenaConexion = "jdbc:oracle:thin:@" + parametros.getServidorBaseDatos() + 
            ":" + parametros.getPuertoBase() + ":" + parametros.getServiceName();
            
         }
         else if(parametros.getTipoConexion()==0){ //SSL
             vcadenaConexion = "jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS=(PROTOCOL=tcps)"
             + "(HOST=" + parametros.getServidorBaseDatos() + ")(PORT = " + parametros.getPuertoBase() + "))"
             + "(CONNECT_DATA = (SERVICE_NAME = " + parametros.getServiceName() + ")))";
             
            Security.addProvider(new oracle.security.pki.OraclePKIProvider());
              
            props.setProperty("oracle.net.wallet_location",
            "(SOURCE = (METHOD = FILE)(METHOD_DATA = (DIRECTORY = " + parametros.getRutaWallet() + ")))");
            
            props.setProperty("oracle.net.authentication_services", "(TCPS)");
            props.setProperty("javax.net.ssl.trustStoreType", "SSO");
         }

        props.setProperty("user", parametros.getEsquema());
        props.setProperty("password", parametros.getClaveBaseDatos());
        
        try{
            Class.forName("oracle.jdbc.OracleDriver");
             
            oconexionbd = DriverManager.getConnection(vcadenaConexion, props);
            oconexionbd.setAutoCommit(false);

            flagConectar=true;       
        }
        catch(Exception ex){
            error_app= ex.getMessage();
        }    
    }
    
    private void prepararConexionSqlServer(parametrosConexion parametros){
        Properties props = new Properties();
        parametrosBd=parametros;
        String vcadenaConexion;
        
        vcadenaConexion = "jdbc:sqlserver://" + parametros.getServidorBaseDatos() + ":" + parametros.getPuertoBase() + ";DatabaseName=" + parametros.getNombreBaseDatos();
        
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
             
            props.setProperty("user", parametros.getEsquema());
            props.setProperty("password", parametros.getClaveBaseDatos());
            
            if(parametros.getTipoConexion()==0){ //SSL
                props.setProperty("EncryptionMethod", "SSL");
                props.setProperty("HostNameInCertificate", parametros.getHostNameCertificado() );
                props.setProperty("TrustStore", parametros.getRutaWallet());
                props.setProperty("TrustStorePassword", parametros.getClaveCertificado());
                props.setProperty("ValidateServerCertificate", "true");
                props.setProperty("CryptoProtocolVersion", "TLSv1.2");
                props.setProperty("encrypt", "true");
                props.setProperty("trustServerCertificate", "false"); 
            }
            
            oconexionbd = DriverManager.getConnection(vcadenaConexion, props);
            oconexionbd.setAutoCommit(false);
            flagConectar=true;      
         
        }
        catch(ClassNotFoundException | SQLException ex){
            error_app= ex.getMessage();
        }    
    }
    
    public ConexionBD(parametrosConexion parametros){
        
       if(parametros.getMotorBaseDatos().equalsIgnoreCase("ORACLE"))
           preparConexionOracle(parametros);
       
       if(parametros.getMotorBaseDatos().equalsIgnoreCase("SQLSERVER"))
           prepararConexionSqlServer(parametros);
       
    }//Fin de método ConexionBD
    
    public boolean isConectado(){
        return flagConectar;
    }
    
    public int ejecutarSql(String sql){
        int resultado=0;
        PreparedStatement stmt=null;
       
        try{
             stmt= oconexionbd.prepareStatement(sql);
             
             if(stmt.execute(sql, 1))
                 resultado=1;
            
             stmt.close();
        }
        catch(Exception e){
            resultado=-2;
            error_app=e.getMessage();
        }
       
        return resultado;
    }
    
    public ResultSet getResultado(String sql, ArrayList<parametrosDatos> parametros ){
        ResultSet rs=null;
        PreparedStatement stmt=null;
        
        parametros.sort(new Comparator<parametrosDatos>(){
                              
                @Override
                public int compare(parametrosDatos o1, parametrosDatos o2) {

                    if (o1.getidParametro()>o2.getidParametro())
                        return 1;
                    else if (o1.getidParametro()< o2.getidParametro())
                        return -1;
                    else

                        return 0;
                }
            }
        );
        
        try{
            stmt= oconexionbd.prepareCall(sql);
            
            if (parametros!=null){
                
                for(parametrosDatos parametro : parametros ){

                    switch (parametro.getTipoDato()) {
 
                        case Entero:
                            if (parametro.getValorParametro()!=null)
                                stmt.setInt(parametro.getidParametro(), (int) parametro.getValorParametro());
                            else
                                stmt.setNull(parametro.getidParametro(),java.sql.Types.INTEGER);
                            break;

                        case Double:

                            if (parametro.getValorParametro()!=null)
                                stmt.setDouble(parametro.getidParametro(),(double) parametro.getValorParametro());
                            else
                                stmt.setNull(parametro.getidParametro(),java.sql.Types.DOUBLE);
                            break;

                        case String:
                            if(parametro.getValorParametro()!=null)
                                stmt.setString(parametro.getidParametro(),String.valueOf( parametro.getValorParametro()));
                            else 
                                stmt.setString(parametro.getidParametro(), null);
                            break;
                            
                        case Float:
                            if (parametro.getValorParametro()!=null)
                                stmt.setFloat(parametro.getidParametro(),(float) parametro.getValorParametro());
                            else
                                stmt.setNull(parametro.getidParametro(),java.sql.Types.FLOAT);
                            break;

                        default:
                            break;
                    } //Fin de switch (parametro.getTipoDato())     
                }//Fin de switch (parametro.getTipoDato())
            }//Fin de  if (parametros!=null)
            
            rs=stmt.executeQuery();
        }
        catch(SQLException ex){
            error_app= ex.getMessage();
        }
        return rs;
    }
    
    public void confirmar(){
       
        try{
            oconexionbd.commit();
        }
        catch(SQLException ex){
            
            error_app=ex.getMessage();
        }
    }
    
    public void cerrar() {  
        
        try{
            oconexionbd.close(); 
        }
        catch(SQLException ex){
            
            error_app=ex.getMessage();
        }
    }   


}
